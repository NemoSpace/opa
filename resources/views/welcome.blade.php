<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <title>Online Parking Reservation</title>
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <link href="{{url('/public/asset/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{url('/public/asset/css/now-ui-kit.css?v=1.1.0')}}" rel="stylesheet" />
        <link href="{{url('/public/asset/css/demo.css')}}" rel="stylesheet" />
    </head>
    <body class="landing-page sidebar-collapse">
        <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
            <div class="container-fluid">
                <div class="navbar-wrapper"><a class="navbar-brand" href="{{url('/')}}">Online Parking Reservation</a></div>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="../public/assets/img/blurred-image-1.jpg">
                    <ul class="navbar-nav">
                        <li class="nav-item"><a class="nav-link" href="{{ url('/motorist/register') }}">Motorist Register</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('/motorist/login') }}">Motorist Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('/owner/register') }}">Owner Register</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('/owner/login') }}">Owner Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="wrapper">
            <div class="page-header page-header-small">
                <div class="page-header-image" data-parallax="true" style="background-image: url('/public/img/slider/bg2.jpg');"></div>
            </div>
            <div class="section section-about-us">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title">About</h2>
                            <h5 class="description"><b>Information technology nowadays is one of the basic needs of the society. Today, Different varieties of trends in technology are being introduced. Information technology provides unlimited possibilities to have better future for our society. Smartphone devices are mostly used in today’s generation. Some developers include their system to the smartphone because it’s widely used, and most people prepare to buy a smartphone than a PC’s or Laptop because it is more accessible and affordable.
                            A system that helps the motorist easily reserve a parking slot. A system that will helps motorist to find a vacant parking slot. A system that will also help the owner to make money out of their parking lot and to manage also their parking lot.</b></h5>
                            <a href=https://opa.tuptaguig.com/public/apk/opa.apk> download the app </a>
                        </div>
                    </div>
                    <div class="separator separator-primary"></div>
                    <div class="section-story-overview">
                        <div class="row">
                            <div class="col-md-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3862.5604813485807!2d121.0350851!3d14.5099025!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397cf2870dcefcf%3A0xa07beacda1e3d3b7!2sTechnological+University+of+the+Philippines!5e0!3m2!1sen!2sph!4v1516072940100" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-5">
                                <!-- First image on the right side, above the article -->
                                <div class="image-container image-right" style="background-image: url('/public/img/slider/bg3.jpg')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                  <h4 class="title"> Please Evaluate this system</h4>
                  <a href=https://goo.gl/forms/XizgbOS079ppMm3l2>Evaluate </a>
                </div>
                <div class="section section-team text-center">
                    <div class="container">
                        <h2 class="title">Here is our team</h2>
                        <div class="team">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="team-player">
                                        <img src="{{url('/public/img/slider/justfer.jpg')}}" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                        <h4 class="title">Justfer Himbing</h4>
                                        <p class="category text-primary">Programmer</p>
                                        <a href="https://www.facebook.com/jstfr" class="btn btn-primary btn-icon btn-round"><i class="fa fa-facebook-square"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="team-player">
                                        <img src="{{url('/public/img/slider/kharl.jpg')}}" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                        <h4 class="title">Kharl Kenneth Bagatchalon</h4>
                                        <p class="category text-primary">Programmer</p>
                                        <a href="https://www.facebook.com/kharl.ken13" class="btn btn-primary btn-icon btn-round"><i class="fa fa-facebook-square"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="section section-contact-us text-center">
                <div class="container">
                    <h2 class="title">Want to work with us?</h2>
                    <p class="description">Your project is very important to us.</p>
                    <div class="row">
                        <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
                            <div class="input-group input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="First Name...">
                            </div>
                            <div class="input-group input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Email...">
                            </div>
                            <div class="textarea-container">
                                <textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
                            </div>
                            <div class="send-button">
                                <a href="#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <footer class="footer footer-default">
                <div class="container">
                    <div class="copyright">
                        &copy;
                        <script>
                        document.write(new Date().getFullYear())
                        </script>
                        <a>Online Parking Reservation</a>.
                    </div>
                </div>
            </footer>
        </div>
        <script src="{{url('/public/asset/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
        <script src="{{url('/public/asset/js/core/popper.min.js')}}" type="text/javascript"></script>
        <script src="{{url('/public/asset/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{url('/public/asset/js/plugins/bootstrap-switch.js')}}"></script>
        <script src="{{url('/public/asset/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
        <script src="{{url('/public/asset/js/plugins/bootstrap-datepicker.j')}}s" type="text/javascript"></script>
        <script src="{{url('/public/asset/js/now-ui-kit.js?v=1.1.0')}}" type="text/javascript"></script>
    </body>
</html>
