@extends('owner.layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <form method="GET" style ='float: left; padding: 5px;' action="{{ route('owner.violator.pdf')}}">
                    <input type="hidden" name="start" id="start" >
                    <input type="hidden" name="end" id="end" >
                    <input type="hidden" name="download" value="pdf">
                    <strong class="card-title">Violation</strong>
                    <button name="submit" class="btn btn-success btn-lg"><i class="ti-download"></i>&nbsp; PDF</button>
                  </form>
                  <form method="GET" style ='float: left; padding: 5px;' action="{{ route('owner.violator.excel')}}">
                    <input type="hidden" name="start" id="starts" >
                    <input type="hidden" name="end" id="ends" >
                    <button type="submit" class="btn btn-info btn-lg"><i class="ti-download"></i>&nbsp; Excel</button>
                    <select name="type" id="type" style="width: 68% !important;">
                      <option value="csv" disabled selected>-- Choose save as --</option>
                      <option value="xls">xls</option>
                      <option value="xlsx">xlsx</option>
                      <option value="csv">csv</option>
                    </select>
                  </form>
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                  </div>
                </div>
                <div class="card-body">
                    <table id="violationTable" class="table table-bordered">
                        <thead class="text-primary">
                            <th>Plate Number</th>
                            <th>Lot Location</th>
                            <th>Lot Address</th>
                            <th>Strike</th>
                            <th>Date</th>
                        </thead>
                        <tbody id="violationBody">
                            @foreach($violations as $violation)
                                <tr>
                                    <td title="{{$violation->complaint}}">{{$violation->plate_number}}</td>
                                    <td title="{{$violation->complaint}}">{{$violation->location}}</td>
                                    <td title="{{$violation->complaint}}">{{$violation->address}}</td>
                                    <td title="{{$violation->complaint}}">{{$violation->strike}}</td>
                                    <td title="{{$violation->complaint}}">{{$violation->updates}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
	@include('owner.violation.complaint')
@stop
@section('script')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
  $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();
    function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      passDate(start.format('YY-MM-DD 00:00:00'),end.format('YY-MM-DD 00:00:00'));
      $('#start').val(start.format('YY-MM-DD 00:00:00'));
      $('#end').val(end.format('YY-MM-DD 00:00:00'));
      $('#starts').val(start.format('YY-MM-DD 00:00:00'));
      $('#ends').val(end.format('YY-MM-DD 00:00:00'));
    }
    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'This Year': [moment().startOf('year'), moment().endOf('year')]
    }
    }, cb);
    cb(start, end);
  });
  function passDate(startDate,endDate) {
    console.log(startDate);
    console.log(endDate);
    $('#violationTable').DataTable().clear().draw();
    $('#violationTable').DataTable().destroy();
    $.ajax({
      type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url: '/owner/violator/filterdate', // the url where we want to POST
      data:{
        '_token': $('input[name=_token]').val(),
        'start':startDate,
        'end':endDate
      },success:function(data){
        $('#violationBody').empty();
        console.log(data);
        $.each(data, function(index, listObj) {
          $('#violationBody').append(
            '<tr>'+
              '<td title="'+listObj.complaint+'">'+listObj.plate_number+'</td>'+
              '<td title="'+listObj.complaint+'">'+listObj.location+'</td>'+
              '<td title="'+listObj.complaint+'">'+listObj.address+'</td>'+
              '<td title="'+listObj.complaint+'">'+listObj.strike+'</td>'+
              '<td title="'+listObj.complaint+'">'+listObj.updates+'</td>'+
            '</tr>'
          );
        });
        $('#violationTable').DataTable();
      },error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
      }
    });
  }
  $(document).on('click', '.btnComplaint', function() {
     $('#complaints').val($(this).data('complaint')),
     $('#complaintModal').modal('show');
  });
</script>
@stop
