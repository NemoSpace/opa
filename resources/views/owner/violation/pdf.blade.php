<!DOCTYPE html>
<html>
<head>
	<title>Violation List - PDF</title>
  <style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
  </style>
</head>
<body>
<div class="container">
  <h3 class="text-center"> Violation List - PDF</h3>
  <table class="table table-bordered">
	<tr>
      <th>Plate Number</th>
      <th>Lot Location</th>
      <th>Lot Address</th>
      <th>Strike</th>
      <th>Date</th>
  </tr>
		  @foreach($violations as $violation)
			<tr>
        <td>{{$violation->plate_number}}</td>
        <td>{{$violation->location}}</td>
        <td>{{$violation->address}}</td>
        <td>{{$violation->strike}}</td>
        <td>{{$violation->updates}}</td>
			</tr>
			@endforeach
	</table>
</div>
</body>
</html>
