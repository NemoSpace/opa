@extends('owner.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="red">
                    <h4 class="title">Complaints</h4>
                </div>
                <div class="card-body">
                    <div class="form-group is-empty">
                        {!! Form::open(['method' => 'GET', 'url'=>'/owner/complaint','class'=>'search-form','role'=>'search']) !!}
                        <div class="form-group has-feedback">
                            <label for="search" class="sr-only">Search Parking Lot</label>
                            <input type="text" class="form-control" name="search" id="search" placeholder="search">
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div id="contentLot">
                        <hr>
                        @forelse($complaints as $complaint)
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Slot Code:</strong> {{$complaint->code}}</p>
                                <p><strong>Location:</strong> {{$complaint->location}}</p>
                                <p><strong>Address:</strong> {{$complaint->address}}</p>
                                <p><strong>Plate Number:</strong> {{$complaint->plate_number}}</p>
                                <p><strong>Complaints:</strong> {{$complaint->complaint}}</p>
                                @if($complaint->status == 'PENDING')
                                    <div style="margin-top: 30px !important;">
                                        <button class="reserve-modal btn btn-success approve" data-id="{{$complaint->id}}" data-plate="{{$complaint->plate_number}}" data-lot="{{$complaint->lots}}">Approve</button>
                                        <button class="reserve-modal btn btn-danger deny" data-id="{{$complaint->id}}">Deny</button>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr>
                        @empty
                        <p>No Parking Lot.</p>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('modal')
    @include('owner.complaint.confirm')
@stop
@section('script')
    <script>
        $(document).on('click', '.approve', function() {
            $('#cstatus').html('Approve');
            $('#cid').val($(this).data('id'));
            $('#lots').val($(this).data('lot'));
            $('#platenumber').val($(this).data('plate'));
            $('#fstatus').val('APPROVED');
            $('#divPercent').show();
            $('#confirmModal').modal('show');
        });
        $(document).on('click', '.deny', function() {
            $('#cstatus').html('Deny');
            $('#cid').val($(this).data('id'));
            $('#divPercent').hide();
            $('#fstatus').val('DENIED');
            $('#confirmModal').modal('show');
        });
    </script>

@stop