<div id="confirmModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" role="form"  method="POST" action="{{route('owner.complaint.update')}}">
				<div class="modal-header"><h4 class="modal-title">Confirmation</h4></div>
				<div class="modal-body">
					<input type="hidden" name="fstatus" id="fstatus">
					<input type="hidden" name="cid" id="cid">
					<input type="hidden" name="lots" id="lots">
					<input type="hidden" name="platenumber" id="platenumber">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<h6>Are you sure you want to <span id="cstatus"></span>?</h6>
					<br>
					<div class="form-group" id="divPercent">
						<label for="percent">Percent:</label>
						<input class="form-control" type="text" name="percent" id="percent" placeholder="Percent">
					</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-success" name="submit" value="Confirm">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>
