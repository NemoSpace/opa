@extends('owner.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="title">Daily Slot Status</h3>
        </div>
        <div  class="card-body table-responsive">
          <div class="row">
            <div class="col-md-12 col-md-12">
              <div class="pad">
                <table id="slotTable" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>location</th>
                      <th>address</th>
                      <th>slot</th>
                      <th>plate number</th>
                      <th>type</th>
                      <th>status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($slots as $slot)
                    <tr>
                      <td>{{$slot->location}}</td>
                      <td>{{$slot->address}}</td>
                      <td>{{$slot->code}}</td>
                      <td>{{$slot->plate_number}}</td>
                      <td>{{$slot->name}}</td>
                      @if(($slot->status) && ($slot->status != 1))
                        <td>{{$slot->status}}</td>
                      @else
                        <td>VACANT</td>
                      @endif
                      <td>
                        <button class="btn btn-danger viewslot"  data-status="{{$slot->status}}" data-id="{{$slot->id}}">View</button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop()
@section('modal')
  @include('owner.reserve.arrive')
	@include('owner.reserve.reserve')
	@include('owner.reserve.occupied')
@stop()
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#slotTable').DataTable()
    })

    $(document).on('click', '.viewslot', function() {
      if($(this).data('status') == 1){
        $('#slot_id').val($(this).data('id'));
				@isset($rfidtables)
					$('#rfid_reserve').val('{{$rfidtables->rfid}}');
				@endisset
        $('#reserveModal').modal('show');
      }else{
        console.log('view');
				console.log($(this).data('id'));
        $.ajax({
          url: '/owner/payment/view/'+ $(this).data('id'),
          type: "GET",
          dataType: "json",
          success:function(data) {
            console.log(data);
            $('.modal-title').text('information');
            if(data.status == 'RESERVED'){
              $('#rfid_arr').val(data.rfid);
              $('#reservation_id_arr').val(data.id);
              $('#motorist_id_arr').val(data.motorist_id);
              $('#price_arr').val(data.price);
              $('#balance_arr').val(data.balance);
              $('#plateinfo_arr').val(data.plate_number);
              $('#balanceinfo_arr').val(data.balance);
              $('#costinfo_arr').val(data.price);
              $('#reservinfo_arr').val(data.reserved);
              $('#arrivalinfo_arr').val('Not yet Arrived');
              $('#departinfo_arr').val('Not yet Departed');
              $('#arriveModal').modal('show');
            }else{
              $('#rfid_occ').val(data.rfid);
              $('#reservation_id_occ').val(data.id);
              $('#motorist_id_occ').val(data.motorist_id);
              $('#price_occ').val(data.price);
              $('#balance_occ').val(data.balance);
              $('#plateinfo_occ').val(data.plate_number);
              $('#balanceinfo_occ').val(data.balance);
              $('#costinfo_occ').val(data.price);
              $('#reservinfo_occ').val(data.reserved);
              $('#arrivalinfo_occ').val(data.arrival);
              $('#departinfo_occ').val('Not yet Departed');
              $('#occupiedModal').modal('show');
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
          },timeout: 3000
        });
      }
    });
  </script>
@stop()
