@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    ul{
      list-style: none !important;
    }
    .myimage{
      width: 200px !important;
      height : 200px !important;
    }
  </style>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@stop
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h3 class="title">Slot Category</h3>
          <a href="{{route('owner.slotcategory.add',$lots->id)}}" class="btn btn-sm btn-primary pull-right">New Slot Category</a>
        </div>
        <div class="card-body">
          <table id="scatTable" class="table table-hover">
            <thead>
              <tr>
                <th>Type</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($scategories as $scategory)
              <tr>
                <td>{{$scategory->name}}</td>
                <td>{{$scategory->price}}</td>
                <td>
                  <a class="btn btn-success btn-sm btn-round" href="{{route('owner.slotcategory.edit', $scategory->id)}}">Edit</a>
                  <a class="btn btn-danger btn-sm btn-round" href="{{route('owner.scategory.destroy', $scategory->id)}}">Remove</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <h3 class="title">Slot</h3>
          @if($scategories->count() > 0)
          <a href="{{route('owner.slot.add', $lots->id)}}" class="btn btn-sm btn-primary pull-right">New Slot</a>
          @endif
        </div>
        <div class="card-body">
          @if(session()->has('message'))
          <div class="alert alert-success">
            {{ session()->get('message') }}
          </div>
          @endif
          <table id="slotTable" class="table table-bordered">
            <thead>
              <tr>
                <th>Image</th>
                <th>Code</th>
                <th>Type</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($slots as $slot)
              <tr>
                <td><img class="img myimage" src="{{ asset('/public/'.$slot->image)}}"/></td>
                <td>{{$slot->code}}</td>
                <td>{{$slot->name}}</td>
                <td>
                  <a class="btn btn-success pull-left" href="{{route('owner.slot.edit', $slot->id)}}">Edit</a>
                  <form action="{{ route('owner.slot.destroy', $slot->id) }}" method="POST" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="Delete">
                    <button class="btn btn-danger pull-left" ><i class="fa fa-remove"></i>Delete</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>Image</th>
              <th>Code</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#slotTable').DataTable()
    })
  </script>
@stop
