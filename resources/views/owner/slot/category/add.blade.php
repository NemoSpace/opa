@extends('owner.layouts.app')
@section('title')
  <title>{{ config('app.name', 'OPA')}}-Slot Category</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#">Slot Category </a>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 offset-md-4">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="card">
          <div class="card-header" data-background-color ="blue">
            <h3 class="title">Add Slot Category</h3>
          </div>

          <form method="POST" action="{{ route('owner.slotcategory.save', $lots->id)}}" enctype="multipart/form-data"
           onsubmit="document.getElementById('submit').disabled=true;
            document.getElementById('submit').value='Submitting, please wait...';">
            <div class="card-body">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="POST">
              <div class="form-group">
               <select name="type" class="form-control" id="type" class="form-control">
                  <option value="" selected="selected">--Vehicle Type --</option>
                  @foreach($vcategories as $vcategory)
                    <option value="{{$vcategory->id}}">{{$vcategory->name}}</option>  
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <input name="price" type="text" class="form-control" onkeypress="return isNumberKey(event);" placeholder="Price" required>
              </div>
            </div>
            <div class="card-footer text-center">
              <input class="btn btn-success" type="submit" id="submit" name="submit" value="submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript">
    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      return ((charCode == 46 || charCode == 8) || (charCode >= 48 && charCode <= 57));
    }

  </script>
@stop