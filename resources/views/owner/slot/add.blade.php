@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 100% !important;
      height : 500px !important;
    }
  </style>
@stop
@section('title')
  <title>{{ config('app.name', 'OPA')}}-Parking Slot</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#">Parking Slot </a>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="card">
          <div class="card-header" data-background-color ="blue">
            <h3 class="title">Add Parking Slot</h3>
          </div>
          <form method="POST" action="{{route('owner.slot.save', $lots->id)}}" enctype="multipart/form-data" 
            onsubmit="document.getElementById('submit').disabled=true;
            document.getElementById('submit').value='Submitting, please wait...';">
            <div class="card-body">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="POST">
              <div class="form-group">
                <input type="file" name="file" id="file" accept="image/*" onchange="AlertFilesize(this)" required>
              </div>
              <div class="form-group">
              	<img class="img" id="myimage" src="#"/>
              </div>
              <div class="form-group">
              	<input name="code" type="text" class="form-control" placeholder="Code" style="text-transform:uppercase" required>
              </div>
              <div class="form-group">
               <select name="type" class="form-control" id="type" class="form-control">
                  <option value="" selected="selected">--Vehicle Type --</option>
                  @foreach($scategories as $scategory)
                    <option value="{{$scategory->id}}">{{$scategory->name}}</option>  
                  @endforeach
                </select>
              </div>
            </div>
            <div class="card-footer text-center">
              <input class="btn btn-success" type="submit" id="submit" name="submit" value="submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
	<script type="text/javascript">
		function AlertFilesize(input){
  		var reader = new FileReader();
  		reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
  		reader.readAsDataURL(input.files[0]);
  	}
    $(function() {
      $('input').focusout(function() {
        // Uppercase-ize contents
        this.value = this.value.toLocaleUpperCase();
      });
    }); 
	</script>
@stop