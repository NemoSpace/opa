@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    #myimage, #parkinglayout{
      width: 500px !important;
      height: 500px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="col-md-12">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form method="POST" action="{{ route('owner.lot.save')}}" enctype="multipart/form-data" onsubmit="document.getElementById('submit').disabled=true;
            document.getElementById('submit').value='Submitting, please wait...';">
        <div class="card">
          <div class="card-header">
            <h3 class="title"> Add Parking Lot</h3>
          </div>
          <div class="card-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">
            <div class="col-md-6">
              <h6>Parking Lot Image</h6>
              <input type="file" name="file" id="file" accept="image/*" onchange="AlertFilesize(this)" required>
              <div  class="form-group">
                <img id="myimage" src="#"/>
              </div>
            </div>
            <div class="col-md-6">
              <h6>Parking Lot Layout</h6>
              <input type="file" name="image" id="image" accept="image/*" onchange="AlertFilesizeLot(this)" required>
              <div class="form-group">
                <img id="parkinglayout" src="#" />
              </div>
            </div>
            <div class="col-md-12 form-group">
              <h6 class="text-center">Parking Lot Information</h6>
            </div>
            <div class="col-md-6 form-group">
              <input name="location" type="text" class="form-control" placeholder="Location" required>
            </div>
            <div class="col-md-6 form-group">
              <input name="addrLot" type="text" class="form-control" placeholder="Blk & Lot">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrStreet" type="text" class="form-control" placeholder="Street">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrSubd" type="text" class="form-control" placeholder="Subdivision">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrBrgy" type="text" class="form-control" placeholder="Barangay" required>
            </div>
            <div class="col-md-6 form-group">
              <input name="addrCity" type="text" class="form-control" placeholder="City" required>
            </div>
            <div class="col-md-6 form-group">
              <input name="addrProv" type="text" class="form-control" placeholder="Province" required>
            </div>
            <div class="col-md-6 form-group">
              <input name="addrPostal" type="text" class="form-control" placeholder="Postal Code" onkeypress="return isNumberKey(event);" required minlength="4">
            </div>
          </div>
          <div class="card-footer">
            <div class="text-center form-group">
              <input class="btn btn-success" type="submit" id="submit" name="submit" value="submit">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
@section('script')
  <script>
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
    
    function AlertFilesizeLot(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#parkinglayout').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }

    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }
  </script>
@stop()