@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    #myimage, #layoutimage{
      width: 500px !important;
      height: 500px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="col-md-12">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form method="POST" action="{{route('owner.lot.update', $lots->id)}}" enctype="multipart/form-data" >
        <div class="card">
          <div class="card-header">
            <h3 class="title"> Edit Parking Lot</h3>
          </div>
          <div class="card-body">
            <div class="col-md-6">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <h6>Parking Lot Image</h6>
              <input type="file" name="file" id="file" onchange="AlertFilesize(this)">
              <div class="form-group">
                @if($lots->image == 'EMPTY')
                  <img id="myimage" src="#"/>
                @else
                  <img id="myimage" src="{{asset('/public/'.$lots->image)}}" />
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <h6>Parking Layout</h6>
              <input type="file" name="image" id="image" onchange="AlertFilesizeLot(this)">
              <div class="form-group">
                @if($lots->parkinglayout == 'EMPTY')
                  <img id="layoutimage" src="#"/>
                @else
                  <img id="layoutimage" src="{{asset('/public/'.$lots->map)}}" />
                @endif
              </div>
            </div>
            <div class="col-md-12 form-group">
              <h6 class="text-center">Parking Lot Information</h6>
            </div>
            <br>
            <div class="col-md-6 form-group">
              <input name="location" type="text" class="form-control" placeholder="Location" value="{{$lots->location}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrLot" type="text" class="form-control" placeholder="Blk & Lot" value="{{$lots->addrLot}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrStreet" type="text" class="form-control" placeholder="Street" value="{{$lots->addrStreet}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrSubd" type="text" class="form-control" placeholder="Subdivision" value="{{$lots->addrSubd}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrBrgy" type="text" class="form-control" placeholder="Barangay" value="{{$lots->addrBrgy}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrCity" type="text" class="form-control" placeholder="City" value="{{$lots->addrCity}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrProv" type="text" class="form-control" placeholder="Province" value="{{$lots->addrProv}}">
            </div>
            <div class="col-md-6 form-group">
              <input name="addrPostal" type="text" class="form-control" placeholder="Postal Code" onkeypress="return isNumberKey(event);" value="{{$lots->addrPostal}}">
            </div>
          </div>
          <div class="card-footer text-center">
            <input class="btn btn-success" type="submit" name="submit" value="submit">
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
@section('script')
  <script>
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
    function AlertFilesizeLot(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#layoutimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }

    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }
  </script>
@stop()
