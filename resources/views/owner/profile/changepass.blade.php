@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 300px !important;
      height : 300px !important;
    }
  </style>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Profile </a>
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-4 offset-md-4">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
         <div class="card">
            <div class="card-header">
              <h3 class="title"><i class="fa fa-key"></i>  Change Password</h3>
            </div>
            <div class="card-body">
              <form method="post" action="{{ route('owner.profile.savepass',Auth::id())}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="PUT">
                  <div class="row">
                    <div class="col-md-12 pr-1">
                    <div class="form-group">
                      <label for="oldpass">Old Password:</label>
                      <input type="password" class="form-control" placeholder="Old Password" name="oldpass" value="" required>
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                          <label for="newpass">New Password:</label>
                          <input type="password" class="form-control" placeholder="New Password" name="password" value="" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label for="confirmpass">Confirm New Password:</label>
                        <input type="password" class="form-control" placeholder="Confirm New Password" name="confirm" value="" required>
                      </div>
                    </div>
                  </div>
                  <div class="text-center">
                    <button class="btn btn-success" type="submit">Change</button>
                  </div>
              </form>
            </div>
         </div>
      </div>
    </div>
  </div>
@stop
@section('script')
@stop()
