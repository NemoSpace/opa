@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 500px !important;
      height : 500px !important;
    }
  </style>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Profile </a>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header" data-background-color="blue">
            <h4 class="title">Edit Profile</h4>
            <p class="category">Complete your profile</p>
          </div>
          <form method="POST" action="{{ route('owner.profile.save')}}"  enctype="multipart/form-data">
            <div class="card-body">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <input type="file" name="file" id="file" accept="image/*" onchange="AlertFilesize(this)">
              <div class="form-group" >
                @if($owners->image == 'EMPTY')
                  <img class="avatar border-gray" id="myimage" src="#"/>
                @else
                  <img class="avatar border-gray" id="myimage" src="{{asset('/public/'.$owners->image)}}"  />
                @endif
              </div>
              <div class="form-group">
                <input name="name" type="text" class="form-control" placeholder="Full name" value="{{$owners->name}}">
              </div>
              <div class="form-group">
                <select name="sex" id="sex" class="form-control">
                  <option value="" >-- Gender --</option>
                  <option value="female">Female</option>
                  <option value="male">Male</option>
                </select>
              </div>
              <div class="form-group">
                <input name="email" type="email" class="form-control" placeholder="Email" value="{{$owners->email}}">
              </div>
              <div class="form-group">
                <input name="number" id="number" type="text" class="form-control" placeholder="Contact Number" onkeypress="return isNumberKey(event);" value="{{$owners->number}}">
              </div>
              <div class="form-group">
                <input name="address" type="text" class="form-control" placeholder="Address" value="{{$owners->address}}">
              </div>
            </div>
            <div class="card-footer text-center">
              <button type="submit" name="submit" class="btn btn-success">Update Profile</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#sex').val('{{$owners->sex}}');
    });

    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }

    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }
  </script>
@stop()
