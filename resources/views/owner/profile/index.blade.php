@extends('owner.layouts.app')
@section('css')
  <style type="text/css">
    .parking-image{
    width: 100% !important;
    height: 200px !important;
    }
  </style>
  @stop
@section('content')
<div class="content">
  <div class="row">
  <div class="col-md-3 text-sm-center">
    <aside class="profile-nav alt">
      <section class="card">
        <div class="card-header user-header alt bg-dark">
          <div class="media">
            <a href="">
              <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('/public/'.$owners->image)}}">
            </a>
            <div class="media-body">
              <h4 class="text-light display-6">{{$owners->name}}</h4>
              <p  class="text-light">Owner</p>
            </div>
          </div>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <a> <i class="fa fa-envelope-o"></i> {{$owners->email}}</a>
          </li>
          <li class="list-group-item">
            <a> <i class="fa fa-map-marker"></i> {{$owners->address}}</a>
          </li>
          <li class="list-group-item">
            <a> <i class="fa fa-mobile "></i> {{$owners->number}}</a>
          </li>
          <li class="list-group-item">
            <a> <i class="fa fa-venus-mars"></i> {{$owners->sex}}</a>
          </li>
          <li class="list-group-item">
            <a href="{{url('owner/profile/edit')}}" class="btn btn-success btn-round form-group">Edit</a>
            <a href="{{route('owner.profile.changepass')}}" class="btn btn-info btn-round form-group">Change Password</a>
          </li>
        </ul>
      </section>
    </aside>

  </div>
<div class="col-md-9">
  <div class="card">
     @if(session()->has('message'))
          <div class="alert alert-success">
            {{ session()->get('message') }}
          </div>
          @endif
    <div class="card-header" data-background-color="orange">
      <h3 class="title">Own Parking Lot</h3>
      <a href="{{route('owner.lot.add')}}" class="btn btn-sm btn-primary pull-right btn-round">New Lot</a>
    </div>
    <div class="card-body">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="search" id="search" placeholder="search">
      </div>
      <div id="ParkingLot">
        @forelse($lots as $lot)
        <div class="row">
          <div class="col-md-3">
            <img class="parking-image" src="{{asset('/public/'.$lot->image)}}" alt="Lot Image">
          </div>
          <div class="col-md-9">
            <p><strong>Location:</strong> {{$lot->location}}</p>
            <p><strong>Address:</strong> {{$lot->address}}</p>
            <p><strong>Total Slot:</strong> {{$lot->slot}}</p>
            <div style="margin-top: 55px !important;">
              <div class="row">
              <a class="btn btn-info btn-round" href="{{route('owner.slot.view',$lot->id)}}">Slot</a>
              <a class="btn btn-success btn-round" href="{{route('owner.lot.edit',$lot->id)}}">Edit</a>
               <form action="{{ route('owner.lot.destroy', $lot->id) }}" method="POST" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="Delete">
              <button class="btn btn-danger btn-round"><i class="fa fa-remove"></i></button>
              </form>
            </div>
            </div>
          </div>
        </div>
        <hr>
        @empty
          <p>No Parking Lot.</p>
        @endforelse
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection
@section('script')
  <script type="text/javascript">
    $('#search').on('keypress',function(){
      $.ajax({
        type : 'GET',
        url : '/owner/lot',
        data:{
          'search':$(this).val()
        },success:function(data){

          $("#ParkingLot").empty();
          $.each(data, function(index, listObj) {
            console.log(listObj.image);
            $('#ParkingLot').append(
              '<div class="row">' +
                '<div class="col-md-3">' +
                  '<img class="parking-image" src="https://opa.tuptaguig.com/public/'+listObj.image+'" alt="Lot Image">' +
                '</div>'+
                '<div class="col-md-9">'+
                  '<p><strong>Location:</strong>'+listObj.location+'</p>' +
                  '<p><strong>Address:</strong> '+listObj.address+'</p>' +
                  '<p><strong>Total Slot:</strong> '+listObj.slot+'</p>' +
                  '<div style="margin-top: 55px !important;">' +
                    '<div class="row">'+
                      '<a class="btn btn-info btn-round" href="/owner/lot/'+listObj.id+'">Slot</a>'+
                      '<a class="btn btn-success btn-round" href="/owner/lot/edit/,'+listObj.id+'">Edit</a>'+
                      '<form action="/owner/lot/delete/'+listObj.id+'" method="POST" >'+
                        '<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
                        '<input type="hidden" name="_method" value="Delete">'+
                        '<button class="btn btn-danger btn-round"><i class="fa fa-remove"></i></button>'+
                      '</form>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'
            );
          });
        },error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(data);
        }
      });
    });
  </script>
@stop
