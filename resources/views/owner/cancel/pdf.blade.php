<!DOCTYPE html>
<html>
	<head>
		<title>Cancel Reservation List - PDF</title>
	  <style type="text/css">
	    table {
	        font-family: arial, sans-serif;
	        border-collapse: collapse;
	        width: 100%;
	    }
	    td, th {
	        border: 1px solid #dddddd;
	        text-align: left;
	        padding: 8px;
	    }
	    tr:nth-child(even) {
	        background-color: #dddddd;
	    }
	  </style>
	</head>
	<body>
		<div class="container">
	  	<h3 class="text-center"> Cancel Reservation List - PDF</h3>
	  	<table class="table table-bordered">
				<tr>
			    <th>Location</th>
			    <th>Slot</th>
			    <th>Price</th>
			    <th>Date</th>
	  		</tr>
				@php($total = 0);
		  	@foreach($reservations as $reservation)
			  	<tr>
				    <td>{{$reservation->location}}</td>
				    <td>{{$reservation->code}}</td>
				    <td>{{$reservation->price}}</td>
				    <td>{{$reservation->updates}}</td>
			    	@php($total += $reservation->price)
			  	</tr>
				@endforeach
			</table>
			<div class="text-center">
				<h3>Total Amount :{{$total}}</h3>
			</div>
		</div>
	</body>
</html>
