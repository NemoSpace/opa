<!DOCTYPE html>
<html>
<head>
	<title>Discount List - PDF</title>
  <style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
  </style>
</head>
<body>
<div class="container">
  <h3 class="text-center"> Discount List - PDF</h3>
  <table class="table table-bordered">
	<tr>
      <th>Percentage</th>
      <th>Plate Number</th>
      <th>Deducted</th>
      <th>Plate Number</th>
      <th>Gained</th>
  </tr>
    @foreach($discounts as $discount)
        <tr>
            <td>{{$discount->percent}}%</td>
            <td>{{$discount->dnumber}}</td>
            <td>-{{$discount->dprice}}</td>
            <td>{{$discount->pnumber}}</td>
            <td>+{{$discount->pprice}}</td>
        </tr>
    @endforeach
	</table>
</div>
</body>
</html>
