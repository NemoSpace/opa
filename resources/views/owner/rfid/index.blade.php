@extends('owner.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h3 class="title">RFID <button class="btn btn-info pull-right addRfid">Add</button></h3>
        </div>
        <div  class="card-body table-responsive">
          <div class="row">
            <div class="col-md-12 col-md-12">
              <div class="pad">
                <table id="rfidTable" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>RFID</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($rfids as $rfid)
                      <tr>
                        <td>{{$rfid->rfid}}</td>
                        <td>
                          <button class="btn btn-success editRfid" data-id="{{$rfid->id}}" data-rfid="{{$rfid->rfid}}">Edit</button>
                          <button class="btn btn-danger deleteRfid" data-id="{{$rfid->id}}" data-rfid="{{$rfid->rfid}}">Remove</button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop()
@section('modal')
	@include('owner.rfid.insert')
	@include('owner.rfid.update')
	@include('owner.rfid.delete')
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#rfidTable').DataTable()
    })
    $(document).on('click', '.addRfid', function() {
      $('.modal-title').text('Add RFID');
		  @isset($unreg)
					$('#rfid').val('{{$unreg->rfid}}');
			@endisset
      $('#insertrfidModal').modal('show');
    });

    $(document).on('click', '.editRfid', function() {
      $('.modal-title').text('Edit RFID');
      $('#rfid_id').val($(this).data('id'));
      $('#rfid_edit').val($(this).data('rfid'));
      $('#updaterfidModal').modal('show');
    });

    $(document).on('click', '.deleteRfid', function() {
      $('.modal-title').text('Remove RFID');
      $('#rfdel_id').val($(this).data('id'));
      $('#rfid_delete').val($(this).data('rfid'));
      $('#deleterfidModal').modal('show');
    });

  </script>
@stop()
