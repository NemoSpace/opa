<div id="deleterfidModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{route('owner.rfid.delete')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" id="rfdel_id">
					<div class="modal-body">
						<div class="form-group">
              <p>Are you sure you want to remove <span id="rfid_delete"></span>?</p>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-info recieve" name="submit">Confirm</button>
						<button class="btn btn-danger" data-dismiss="modal"> Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
