<div id="updaterfidModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{route('owner.rfid.edit')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" id="rfid_id">
					<div class="modal-body">
						<div class="form-group">
							<label for="plateinfo">RFID:</label>
							<input type="text" class="form-control" name="rfid" id="rfid_edit">
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-info recieve" name="submit">Submit</button>
						<button class="btn btn-danger" data-dismiss="modal"> Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
