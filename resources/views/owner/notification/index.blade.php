@extends('owner.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="animated fadeIn">
        <div class="card">
          <div class="card-header">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <strong class="card-title">Notification</strong>
          </div>
          <div class="card-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table id="notificationTable" class="table table-bordered">
              <thead class="text-primary">
                <th>Message</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($notification as $notif)
                <tr>
                  <td>{{$notif->message}}</td>
                  <td>{{$notif->updated_at}}</td>
                  <td>{{$notif->status}}</td>
                  <td><button class="btn btn-danger change" type="button" data-id="{{$notif->id}}" data-status="{{$notif->status}}">Change</button></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('modal')
  @include('owner.notification.edit')
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function() {
      $('#notificationTable').DataTable();
    });

    $(document).on('click', '.change', function() {
			$('.modal-title').text('Change Status');
			$('#notif_id').val($(this).data('id'));
			$('#status').val($(this).data('status'));
			$('#notificationModal').modal('show');
		});
  </script>
@stop
