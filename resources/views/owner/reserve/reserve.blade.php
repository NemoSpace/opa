<div id="reserveModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{route('owner.arrive.nonuser')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="slot_id" id="slot_id">
					<div class="modal-body">
						<div class="form-group">
							<label for="plateinfo">RFID:</label>
							<input type="text" class="form-control" name="rfid" id="rfid_reserve">
						</div>
						<div class="form-group">
							<label for="plateinfo">Plate Number:</label>
							<input type="text" class="form-control" name="plate_number" id="plate_number">
						</div>
						<div class="form-group">
							<label for="balanceinfo">Hours:</label>
							<input type="text" class="form-control" name="hours" id="hours">
						</div>
						<div class="form-group">
							<label for="costinfo">Total Cost:</label>
							<input type="text" class="form-control" name="price" id="price">
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-info recieve" type="submit" name="submit">Park</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
