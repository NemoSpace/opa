<div id="arriveModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{route('owner.arrive.user')}}">
					<div class="modal-body">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="motorist_id" id="motorist_id_arr">
						<input type="hidden" name="reservation_id" id="reservation_id_arr">
						<input type="hidden" name="balance" id="balance_arr">
						<input type="hidden" name="price" id="price_arr">
						<div class="form-group">
							<label for="plateinfo">RFID:</label>
							<input type="text" class="form-control" name="rfid" id="rfid_arr">
						</div>
						<div class="form-group">
							<label for="plateinfo">Plate Number:</label>
							<input type="text" class="form-control" id="plateinfo_arr" disabled>
						</div>
						<div class="form-group">
							<label for="balanceinfo">Balanced:</label>
							<input type="text" class="form-control" id="balanceinfo_arr" disabled>
						</div>
						<div class="form-group">
							<label for="costinfo">Total Cost:</label>
							<input type="text" class="form-control" id="costinfo_arr" disabled>
						</div>
						<div class="form-group">
							<label for="reservinfo">Reservation:</label>
							<input type="text" class="form-control" id="reservinfo_arr" disabled>
						</div>
						<div class="form-group">
							<label for="arrivalinfo">Arrival:</label>
							<input type="text" class="form-control" id="arrivalinfo_arr" disabled>
						</div>
						<div class="form-group">
							<label for="departinfo">Departure:</label>
							<input type="text" class="form-control" id="departinfo_arr" disabled>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" name="submit">Arrive</button>
						<button class="btn btn-danger" data-dismiss="modal"> Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
