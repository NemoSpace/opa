@extends('owner.layouts.app')
@section('css')
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="animated fadeIn">
        <div class="card">
          <div class="card-header">
            <form method="GET" style ='float: left; padding: 5px;' action="{{ route('owner.payment.pdf')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="start" id="start" >
              <input type="hidden" name="end" id="end" >
              <input type="hidden" name="download" value="pdf">
              <strong class="card-title">Payment</strong>
              <button name="submit" class="btn btn-success btn-xs"><i class="ti-download"></i>&nbsp; PDF</button>
            </form>
            <form method="GET" style ='float: left; padding: 5px;' action="{{ route('owner.payment.excel')}}">
              <input type="hidden" name="start" id="starts" >
              <input type="hidden" name="end" id="ends" >
              <button type="submit" class="btn btn-info btn-xs"><i class="ti-download"></i>&nbsp; Excel</button>
              <select name="type" id="type" style="width: 65% !important;">
                <option value " " disabled selected>-- Choose save as --</option>
                <option value="xls">xls</option>
                <option value="xlsx">xlsx</option>
                <option value="csv">csv</option>
              </select>
            </form>
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
              <span></span> <b class="caret"></b>
            </div>
          </div>
          <div class="card-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table id="paymentTable" class="table table-bordered">
              <thead class="text-primary">
                <th>Motorist</th>
                <th>Plate Number</th>
                <th>Price</th>
                <th>Status</th>
                <th>Date</th>
              </thead>
              <tbody id="paymentBody">
                @foreach($payments as $payment)
                <tr>
                  <td>{{$payment->name}}</td>
                  <td>{{$payment->plate_number}}</td>
                  <td>{{$payment->price}}</td>
                  <td>{{$payment->status}}</td>
                  <td>{{$payment->updates}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="text-center" >
              <h3 id="lblTotal"></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function() {
      var start = moment().subtract(29, 'days');
      var end = moment();
      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        passDate(start.format('YY-MM-DD 00:00:00'),end.format('YY-MM-DD 23:59:59'));
        $('#start').val(start.format('YY-MM-DD 00:00:00'));
        $('#end').val(end.format('YY-MM-DD 23:59:59'));
        $('#starts').val(start.format('YY-MM-DD 00:00:00'));
        $('#ends').val(end.format('YY-MM-DD 23:59:59'));
      }
      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'This Year': [moment().startOf('year'), moment().endOf('year')]
      }
      }, cb);
      cb(start, end);
    });
    function passDate(startDate,endDate) {
      console.log(startDate);
      console.log(endDate);
      $('#paymentTable').DataTable().clear().draw();
      $('#paymentTable').DataTable().destroy();
      $.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '/owner/payment/filterdate', // the url where we want to POST
        data:{
          '_token': $('input[name=_token]').val(),
          'start':startDate,
          'end':endDate
        },success:function(data){
          $('#paymentBody').empty();
          $.each(data, function(index, listObj) {
            $('#paymentBody').append(
              '<tr>'+
                '<td>'+listObj.name+'</td>'+
                '<td>'+listObj.plate_number+'</td>'+
                '<td>'+listObj.price+'</td>'+
                '<td>'+listObj.status+'</td>'+
                '<td>'+listObj.updates+'</td>'+
              '</tr>'
            );
          });
          $('#paymentTable').DataTable();
        },error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
        }
      });

      $.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '/owner/payment/totalpayments', // the url where we want to POST
        data:{
          '_token': $('input[name=_token]').val(),
          'start':startDate,
          'end':endDate
        },success:function(data){
          console.log(data);
          $('#lblTotal').text("Total Amount :"+data.total);
        },error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
        }
      });
    }
  </script>
@stop
