<!DOCTYPE html>
<html>
	<head>
		<title>Payment List - PDF</title>
		<style type="text/css">
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}

			tr:nth-child(even) {
				background-color: #dddddd;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<h3 class="text-center"> Payment List - PDF</h3>
			<table class="table table-bordered">
				<tr>
					<th>Motorist</th>
					<th>Plate Number</th>
					<th>Price</th>
					<th>Status</th>
					<th>Date</th>
				</tr>
				@php($total = 0);
				@foreach($payments as $payment)
					<tr>
						<td>{{$payment->name}}</td>
						<td>{{$payment->plate_number}}</td>
						<td>{{$payment->price}}</td>
						<td>{{$payment->status}}</td>
						<td>{{$payment->updates}}</td>
						@php($total += $payment->price)
					</tr>
				@endforeach
			</table>
			<div class="text-center">
				<h3>Total Amount :{{$total}}</h3>
			</div>
		</div>
	</body>
</html>
