@extends('owner.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card-body bg-flat-color-1">
				<div class="h1 text-muted text-right mb-4">
					<i class="ti-location-arrow text-light"></i>
				</div>
				<div class="h4 mb-0 text-light">
					<span class="count">{{$slots}}</span>
				</div>
				<small class="text-uppercase font-weight-bold text-light">Slot</small>
				<div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card-body bg-flat-color-2">
				<div class="h1 text-muted text-right mb-4">
					<i class="ti-alert text-light"></i>
				</div>
				<div class="h4 mb-0 text-light">
					<span class="count">0</span>
				</div>
				<small class="text-uppercase font-weight-bold text-light">Violations</small>
				<div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card-body bg-flat-color-3">
				<div class="h1 text-muted text-right mb-4">
					<i class="fa fa-qrcode text-light"></i>
				</div>
				<div class="h4 mb-0 text-light">
					<span class="count">{{$rfids}}</span>
				</div>
				<small class="text-uppercase font-weight-bold text-light">RFID</small>
				<div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card-body bg-flat-color-4">
				<div class="h1 text-muted text-right mb-4">
					<i class="ti-money text-light"></i>
				</div>
				<div class="h4 mb-0 text-light">
					<span class="count">{{$payments}}</span>
				</div>
				<small class="text-uppercase font-weight-bold text-light">Transaction</small>
				<div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-3">
		  <div class="card">
		    <div class="card-body">
					  <h4 class="mb-3">Ratings Chart </h4>
		      	<canvas id="pieRating"></canvas>
		    </div>
		  </div>
		</div>
		<div class="col-md-3">
		  <div class="card">
		    <div class="card-body">
					  <h4 class="mb-3">Most Occupied Slot</h4>
		      	<canvas id="pieSlotOccupied"></canvas>
		    </div>
		  </div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
						<h4 class="mb-3">Most Canceled Slot</h4>
						<canvas id="pieSlotCanceled"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
						<h4 class="mb-3">Most Frequent Driver</h4>
						<canvas id="pieSlotMotorist"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
					<div class="card-body">
							<h4 class="mb-3">Monthly Payments</h4>
							<canvas id="linePayment"></canvas>
					</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
					<div class="card-body">
							<h4 class="mb-3">Monthly Cancel Reservation</h4>
							<canvas id="lineCancel"></canvas>
					</div>
			</div>
		</div>
		<div class="col-md-4">
				<div class="card">
						<div class="card-body">
								<h4 class="mb-3">Monthly Reservation</h4>
								<canvas id="ReservationChart"></canvas>
						</div>
				</div>
		</div>
		<div class="col-lg-4">
				<div class="card">
						<div class="card-body">
								<h4 class="mb-3">Monthly Complaints</h4>
								<canvas id="ComplaintChart"></canvas>
						</div>
				</div>
		</div>
		<div class="col-lg-4">
				<div class="card">
						<div class="card-body">
								<h4 class="mb-3">Monthly Violation</h4>
								<canvas id="ViolationChart"></canvas>
						</div>
				</div>
		</div>
	</div>
</div>
@stop()
@section('script')
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {

			// Ratings Chart
			$.ajax({
		      url: '/owner/chart/ratings',
		      type: "GET",
		      success: function(data) {
		        console.log(data);
		        var rate = [];
		        var rates = [];
		        var coloR = [];
		        var dynamicColors = function() {
		           var r = Math.floor(Math.random() * 255);
		           var g = Math.floor(Math.random() * 255);
		           var b = Math.floor(Math.random() * 255);
		           return "rgb(" + r + "," + g + "," + b + ")";
		        };
		        $.each(data, function(index, listObj) {
		          rate.push(listObj.rate + " Star");
		          rates.push(listObj.rates);
		          coloR.push(dynamicColors());
		        });
		        var chartData = {
		            labels: rate,
		            datasets: [{
		               label: 'Ratings',
		               backgroundColor: coloR,
		               borderColor: 'rgba(200, 200, 200, 0.75)',
		               hoverBorderColor: 'rgba(200, 200, 200, 1)',
		               data: rates
		            }]
		         };
		         var ctx = $("#pieRating");
		         var barGraph = new Chart(ctx, {
		            type: 'pie',
		            data: chartData
		         })
		      },
		      error: function (xhr, ajaxOptions, thrownError) {
		        alert(xhr.status);
		      },
		   	});

			// Cancel Chart
			$.ajax({
	      		url: '/owner/chart/cancel',
 		      	type: "GET",
 		      	success: function(data) {
 		        	console.log(data);
 		        	var mnth = [];
 		        	var pays = [];
					$.each(data, function(index, listObj) {
		          		mnth.push(listObj.monthName.name);
		          		pays.push(listObj.pay);
		        	});

					var chartData = {
		            	labels: mnth,
		            	datasets: [{
			               	label: 	'Monthly Cancel Reservation',
									backgroundColor: 'rgba(0,103,255,.15)',
									borderColor: 'rgba(0,103,255,0.5)',
									borderWidth: 3.5,
									pointStyle: 'circle',
									pointRadius: 5,
									pointBorderColor: 'transparent',
									pointBackgroundColor: 'rgba(0,103,255,0.5)',
		               		data: pays
		            	}]
	         		};

				 	var ctx = $("#lineCancel");
		         	var barGraph = new Chart(ctx, {
    					type: 	'line',
        				data: 	chartData,
						options: {
		            		responsive: true,
		            			tooltips: {
		                			mode: 'index',
		                			titleFontSize: 12,
		                			titleFontColor: '#000',
		                			bodyFontColor: '#000',
	                				backgroundColor: '#fff',
		                			titleFontFamily: 'Montserrat',
		                			bodyFontFamily: 'Montserrat',
		                			cornerRadius: 3,
	                				intersect: false,
		            			},
			            		legend: {
			                		display: false,
			                		position: 'top',
			                		labels: {
			                    	usePointStyle: true,
			                    	fontFamily: 'Montserrat',
			                	},
		            		},
		            		scales: {
		                		xAxes: [ {
		                    		display: true,
	                    			gridLines: {
	                        			display: false,
	                        			drawBorder: false
	                    			},
	                    			scaleLabel: {
	                        			display: false,
	                        			labelString: 'Month'
	                    			}
	                        	} ],
			                	yAxes: [ {
			                    	display: true,
			                    	gridLines: {
			                        	display: false,
			                        	drawBorder: false
			                    	},
			                    	scaleLabel: {
			                        	display: true,
			                        	labelString: 'Value'
			                    	}
		                        } ]
		            		},
		            		title: {
		                		display: false,
		            		}
		        		}
		        	})
		 		},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				},
		 	});

			// Payment Chart
			$.ajax({
	      		url: '/owner/chart/payment',
 		      	type: "GET",
 		      	success: function(data) {
 		        	console.log(data);
 		        	var mnth = [];
 		        	var pays = [];
					$.each(data, function(index, listObj) {
		          		mnth.push(listObj.monthName.name);
		          		pays.push(listObj.pay);
		        	});

					var chartData = {
		            	labels: mnth,
		            	datasets: [{
			               	label: 	'Monthly Payments',
									backgroundColor: 'rgba(0,103,255,.15)',
									borderColor: 'rgba(0,103,255,0.5)',
									borderWidth: 3.5,
									pointStyle: 'circle',
									pointRadius: 5,
									pointBorderColor: 'transparent',
									pointBackgroundColor: 'rgba(0,103,255,0.5)',
		               		data: pays
		            	}]
	         		};

				 	var ctx = $("#linePayment");
		         	var barGraph = new Chart(ctx, {
    					type: 	'line',
        				data: 	chartData,
						options: {
		            		responsive: true,
		            			tooltips: {
		                			mode: 'index',
		                			titleFontSize: 12,
		                			titleFontColor: '#000',
		                			bodyFontColor: '#000',
	                				backgroundColor: '#fff',
		                			titleFontFamily: 'Montserrat',
		                			bodyFontFamily: 'Montserrat',
		                			cornerRadius: 3,
	                				intersect: false,
		            			},
			            		legend: {
			                		display: false,
			                		position: 'top',
			                		labels: {
			                    	usePointStyle: true,
			                    	fontFamily: 'Montserrat',
			                	},
		            		},
		            		scales: {
		                		xAxes: [ {
		                    		display: true,
	                    			gridLines: {
	                        			display: false,
	                        			drawBorder: false
	                    			},
	                    			scaleLabel: {
	                        			display: false,
	                        			labelString: 'Month'
	                    			}
	                        	} ],
			                	yAxes: [ {
			                    	display: true,
			                    	gridLines: {
			                        	display: false,
			                        	drawBorder: false
			                    	},
			                    	scaleLabel: {
			                        	display: true,
			                        	labelString: 'Value'
			                    	}
		                        } ]
		            		},
		            		title: {
		                		display: false,
		            		}
		        		}
		        	})
		 		},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				},
		 	});

			// Reservation Chart
		 	$.ajax({
				url: '/owner/chart/reservation',
				type: "GET",
				success: function(data) {
					console.log(data);
					var chartData = {
						labels: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.","Aug.","Sept.","Oct.","Nov.","Dec." ],
					 	datasets: data
					};
					var ctx = $("#ReservationChart");
					var barGraph = new Chart(ctx, {
						type: 'bar',
						data: chartData,
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero: true
									}
								}]
							}
						}
					})
				},error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
		 		}
			});

		 	//Occupied Slot Chart
		 	$.ajax({
			 	url: '/owner/chart/slot/occupied',
			 	type: "GET",
			 	success: function(data) {
					console.log(data);
				 	var count = [];
				 	var code = [];
				 	var coloR = [];
				 	var dynamicColors = function() {
				 		var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ")";
					};
				 	$.each(data, function(index, listObj) {
				 		count.push(listObj.lote);
					 	code.push(listObj.code);
					 	coloR.push(dynamicColors());
				 	});
				 	var chartData = {
				 		labels: code,
				 		datasets: [{
							label: 'Top Slot',
							backgroundColor: coloR,
							borderColor: 'rgba(200, 200, 200, 0.75)',
							hoverBorderColor: 'rgba(200, 200, 200, 1)',
							data: count
						}]
					};
					var ctx = $("#pieSlotOccupied");
					var barGraph = new Chart(ctx, {
					 	type: 'pie',
					 	data: chartData
					})
			 	},
			 	error: function (xhr, ajaxOptions, thrownError) {
				 	alert(xhr.status);
			 	},
		 	});

			//Motorist Slot Chart
			$.ajax({
				url: '/owner/chart/slot/motorist',
				type: "GET",
				success: function(data) {
					console.log(data);
					var count = [];
					var code = [];
					var coloR = [];
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ")";
					};
					$.each(data, function(index, listObj) {
						count.push(listObj.plates);
						code.push(listObj.plate_number);
						coloR.push(dynamicColors());
					});
					var chartData = {
						labels: code,
						datasets: [{
							label: 'Top Driver',
							backgroundColor: coloR,
							borderColor: 'rgba(200, 200, 200, 0.75)',
							hoverBorderColor: 'rgba(200, 200, 200, 1)',
							data: count
						}]
					};
					var ctx = $("#pieSlotMotorist");
					var barGraph = new Chart(ctx, {
						type: 'pie',
						data: chartData
					})
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				},
			});

			//Canceled Slot Chart
			$.ajax({
				url: '/owner/chart/slot/canceled',
				type: "GET",
				success: function(data) {
					console.log(data);
					var count = [];
					var code = [];
					var coloR = [];
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ")";
					};
					$.each(data, function(index, listObj) {
						count.push(listObj.lote);
						code.push(listObj.code);
						coloR.push(dynamicColors());
					});
					var chartData = {
						labels: code,
						datasets: [{
							label: 'Top Slot',
							backgroundColor: coloR,
							borderColor: 'rgba(200, 200, 200, 0.75)',
							hoverBorderColor: 'rgba(200, 200, 200, 1)',
							data: count
						}]
					};
					var ctx = $("#pieSlotCanceled");
					var barGraph = new Chart(ctx, {
						type: 'pie',
						data: chartData
					})
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				},
			});


			// Complaint Chart
			$.ajax({
				url: '/owner/chart/complaint',
				type: "GET",
			 	success: function(data) {
					console.log(data);
				 	var chartData = {
					 	labels: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.","Aug.","Sept.","Oct.","Nov.","Dec." ],
					 	datasets: data
					};
					var ctx = $("#ComplaintChart");
					var barGraph = new Chart(ctx, {
						type: 'bar',
						data: chartData,
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero: true
									}
								}]
							}
						}
					})
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				}
			});

			// Complaint Chart
			$.ajax({
			 	url: '/owner/chart/violation',
				type: "GET",
			 	success: function(data) {
					console.log(data);
				 	var chartData = {
						labels: [ "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.","Aug.","Sept.","Oct.","Nov.","Dec." ],
						datasets: data
					};
					var ctx = $("#ViolationChart");
					var barGraph = new Chart(ctx, {
						type: 'bar',
						data: chartData,
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero: true
									}
								}]
							}
						}
					})
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
				}
			});
		});
	</script>
@stop()
