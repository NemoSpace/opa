@extends('motorist.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="content">
  <div class="col-md-8">
    <div class="card">
      <form method="POST" action="{{route('motorist.complain.complaint')}}" onsubmit="document.getElementById('submit').disabled=true; document.getElementById('submit').value='Submitting, please wait...';">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="card-header">
          <h4 class="title">Complain</h4>
        </div>
        <div class="card-body">
          <div class="form-group">
            <input class="form-control" type="text" name="plate_number" id="plate_number"  placeholder="Plate Number" required minlength="6" maxlength="7">
          </div>
          <div class="form-group">
            <select class="form-control" id="complaintTemplate">
              <option value='0'>-- Choose Complaint Template --</option>
              <option value='1'>Report Plate No. XYZ1234 for taking my parking slot.</option>
              <option value='2'>Plate No. XYZ1234 took my parking slot.</option>
              <option value='3'>Plate No. XYZ1234 occupied my reserved slot.</option>
            </select>
          </div>
          <div class="form-group">
            <textarea name="complaint" id="complaint" rows="9" placeholder="Complaint" class="form-control" required minlength="5"></textarea>
          </div>
          <div class="form-group">
            <input class="btn btn-success" type="submit" name="submit" id="submit" value="submit">
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-header">
        <h4 class="title">Complaint List</h4>
      </div>
      <div class="card-body">
        <table class="table table-bordered" id="complaintTable">
          <thead>
            <tr>
              <th>Plate Number</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach($complaints as $complaint)
            <tr>
              <td>{{$complaint->plate_number}}</td>
              <td>{{$complaint->status}}</td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
          <tr>
            <th>Plate Number</th>
            <th>Status</th>
          </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#complaintTable').DataTable()
    })

    $('#complaintTemplate').on('change', function() {

      if(this.value == 1){
        $('#complaint').val('Report Plate No.'+$('#plate_number').val()+' for taking my parking');
      }else if(this.value == '2'){
        $('#complaint').val('Plate No. '+$('#plate_number').val()+'  took my parking slot.');
      }else if(this.value == '3'){
        $('#complaint').val('Plate No. '+$('#plate_number').val()+'  occupied my reserved slot.');
      }else{
        $('#complaint').val('');
      }
})
</script>
@stop
