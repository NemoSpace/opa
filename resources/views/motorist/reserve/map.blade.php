<div id="mapModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Parking Slot Map</h4>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					<img id="slotMap" src="" width="300" height="300" alt="Lot Image">
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
