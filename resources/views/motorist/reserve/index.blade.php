@extends('motorist.layouts.app')
@section('css')
  <style type="text/css">
    .parking-image{
      width: 100% !important;
      height: 200px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header" data-background-color="green">
            <h4 class="title">Information</h4>
            <p class="category">Fill in the needed information.</p>
          </div>
          <div class="card-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <input type="text" name="name" class="form-control" readonly value="{{$motorists->name}}">
            </div>
            <div class="form-group">
              <select name="vehicle" class="form-control" id="vehicle" class="form-control">
                <option value="" >-- Choose your vehicle --</option>
                @foreach($vehicles as $vehicle)
                  <option value="{{$vehicle->id}}">{{$vehicle->plate_number}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <input type="text" name="hour" id="hour" class="form-control" onkeypress="return isNumberKey(event);" placeholder="No. of Hours">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card">
          <div class="card-header" data-background-color="red">
            <h4 class="title">Location</h4>
            <p class="category">Choose where to park</p>
          </div>
          <div class="card-body">
            <div class="form-group is-empty">
                <div class="form-group has-feedback">
                  <label for="search" class="sr-only">Search Parking Lot</label>
                  <input type="text" id="search" class="form-control" name="search" id="search" placeholder="search">
                </div>
            </div>
            <div id="contentLot">
              @forelse($lots as $lot)
                <div class="row">
                  <div class="col-md-3">
                    <img class="parking-image" src="{{ asset('/public/'.$lot->image)}}" alt="Lot Image">
                  </div>
                  <div class="col-md-9">
                    <p><strong>Location:</strong> {{$lot->location}}</p>
                    <p><strong>Address:</strong> {{$lot->address}}</p>
                    <p><strong>Available Slot:</strong> {{$lot->available}}</p>
                    <p><strong>Total Slot:</strong> {{$lot->slot}}</p>
                    @if($reservations == 0)
                      @if($reserves < 1)
                        @if($lot->available > 0)
                          <div style="margin-top: 20px !important;">
                            <button class="reserve-modal btn btn-success" data-id="{{$lot->id}}">Reserve</button>
                          </div>
                        @endif
                      @endif
                    @endif
                  </div>
                </div>
                <hr>
              @empty
                <p>No Parking Lot.</p>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('modal')
  @include('motorist.reserve.lot')
  @include('motorist.error')
@stop
@section('script')
  <script>
    $('#search').on('keypress',function(){
      $.ajax({
        type : 'GET',
        url : '/motorist/reserve/search',
        data:{
          'search':$(this).val()
        },success:function(data){
          $("#contentLot").empty();
          $.each(data, function(index, listObj) {
            btx = '';
            if(listObj.available > 0){
              btx = '<div style="margin-top: 20px !important;">'+
                      '<button class="reserve-modal btn btn-success" data-id= '+listObj.id+'>Reserve</button>'+
                    '</div>';
            }
            $('#contentLot').append(
              '<div class="row">'+
                '<div class="col-md-3">'+
                  '<img class="parking-image" src="https://opa.tuptaguig.com/public/'+listObj.image+'" alt="Lot Image">'+
                '</div>'+
                '<div class="col-md-9">'+
                  '<p><strong>Location:</strong>'+listObj.location+'</p>'+
                  '<p><strong>Address:</strong> '+listObj.address+'</p>'+
                  '<p><strong>Available Slot:</strong> '+listObj.available+'</p>'+
                  '<p><strong>Total Slot:</strong> '+listObj.slot+'</p>'+
                  ''+btx+''+
                '</div>'+
              '</div>'+
              '<hr>'
            );
          });
        },error: function (xhr, ajaxOptions, thrownError) {
          $('#txtError').text("Something went wrong.");
          $('#errorModal').modal('show');
        }
      });
    });

    $(document).on('click', '.reserve-modal', function() {
      console.log($("#hour").val());
      console.log($(this).data('id'));
      $.ajax({
        url: '/motorist/reserve/cost',
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val(),
          'id':  $(this).data('id'),
          'vehicle': $('#vehicle').val(),
          'hour': $('#hour').val()
        },
        dataType: "json",
        success:function(data) {
          console.log('success');
          console.log(data);
          $('.modal-title').text('Verification');
          $('#slotID').val(data.id);
          $('#slotCode').val(data.code);
          $('#slotPrice').val(data.cost);
          $('#slotVehicle').val($('#vehicle  option:selected').text());
          $('#slotHour').val($('#hour').val());
          $('#slot_info').val(data.code);
          $('#cost_info').val(data.cost);
          $('#infoModal').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
          $('#txtError').text("Something went wrong. Make sure you choose a vehicle and enter your reservation hour(s). ");
          $('#errorModal').modal('show');
        },timeout: 3000
      });
    });

    function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }
  </script>
@stop
