<div id="infoModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{ route('motorist.reserve.confirm')}}">
					<div class="modal-body">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden"  id="slotID" name="slotID">
						<input type="hidden"  id="slotCode" name="slotCode">
						<input type="hidden"  id="slotPrice" name="slotPrice">
						<input type="hidden"  id="slotVehicle" name="slotVehicle">
						<input type="hidden"  id="slotHour" name="slotHour">
						<div class="form-group">
							<label for="slot_info">Slot:</label>
							<input type="text" class="form-control" id="slot_info" readonly>
						</div>
						<div class="form-group">
							<label for="cost_info">Cost:</label>
							<input type="text" class="form-control" id="cost_info" readonly>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit" name="submit">Confirm</button> 
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>