<div id="notificationModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title"></h4></div>
				<form class="form-horizontal" role="form"  method="POST" action="{{route('motorist.notification.update')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" id="notif_id">
					<div class="modal-body">
						<div class="form-group">
							<label for="plateinfo">Mark as:</label>
              <select class="form-control" name="status" id="status">
                <option value="read">read</option>
                <option value="unread">unread</option>
              </select>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-info" name="submit">Submit</button>
						<button class="btn btn-danger" data-dismiss="modal"> Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
