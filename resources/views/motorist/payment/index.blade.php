@extends('motorist.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
@stop
@section('content')
  	<div class="container-fluid">
    	<div class="col-md-6">
    		<div class="card">
     			<div class="card-header">
        			<h4 class="title">Payer</h4>
        			<p class="category">Payer Parking Information.</p>
      			</div>
      			<div class="card-content">
  					<div class="col-md-6">
  						{!! QrCode::encoding('UTF-8')->size(300)->generate('Name:'.$reservations->name.
  							', Plate:'.$reservations->plate_number.
  							', Balanced:' .$payments.
  						 	', Price:'.$reservations->price.
  						 	', Reserved:'.$reservations->reserved.
  						 	', Arrived:'.$reservations->arrival.
  						 	', Departure:'.$dateToday
  						 ); !!}
						</div>
  					<div class="col-md-6">
  						<p><span style="font-weight: bold;" >Name:</span> {{ $reservations->name}}</p>
							<p><span style="font-weight: bold;"">Plate Number:</span> {{ $reservations->plate_number}}</p>
							<p><span style="font-weight: bold;"">Price:</span> {{ $reservations->price}}</p>
							<p><span style="font-weight: bold;"">Balanced:</span> {{$payments}}</p>
							<p><span style="font-weight: bold;"">Reserved:</span> {{ $reservations->reserved}}</p>
							<p><span style="font-weight: bold;"">Arrived:</span> {{ $reservations->arrival}}</p>
							<p><span style="font-weight: bold;"">Departure:</span> {{ $dateToday}}</p>
						<div id="rateYo"></div>
						<br>
						<div class="form-group">
						<form  method="POST" action="{{route('motorist.reserve.rate')}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="slot" value="{{$reservations->slot_id}}">
							<input type="hidden" name="rate" id="rate">
							<input class="btn btn-success form-control" type="submit" name="submit" value="Rate">
						</form>
						</div>
  					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
	<script type="text/javascript">
		$(function () {
  			$("#rateYo").rateYo({
    			spacing   : "5px",
    			starWidth: "20px",
    			multiColor: {
      				"startColor": "#FF0000", //RED
      				"endColor"  : "#00FF00"  //GREEN
    			},
    			onSet: function (rating, rateYoInstance) {
    				$('#rate').val(rating);
    			}
  			});
		});
	</script>
@stop
