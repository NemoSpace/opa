<div id="reminderModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title textHead"></h4>
			</div>
			<div class="modal-body">
    	   <h6 class="text-danger textBody"></h6>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					<span class='glyphicon glyphicon-remove'></span> Close
				</button>
			</div>
		</div>
	</div>
</div>
