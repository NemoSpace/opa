<div id="timesModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Expired</h4>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					<input id="resID" name="resID" type="hidden">
					<h6 class="text-danger">You didn't make it your reservation is now over but you have to pay.</h6>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="submit" name="submit">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
