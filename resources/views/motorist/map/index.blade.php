@extends('motorist.layouts.app')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <iframe id="gmap" style="height: 800px !important; width: 100% !important;" frameborder="0" scrolling="yes" margintheight="0" marginwidth="0" src="https://maps.google.com.ph?output=embed"></iframe>
        </div>
      </div>
      <div class="col-md-4">
        @isset($reservations)
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="title">Verification</h4>
                <p class="category">Temporary</p>
              </div>
              <div class="card-content">
                @if($reservations->status == "RESERVED")
                  <br>
                  <form method="POST" action="{{route('motorist.reserve.cancel', $reservations->id)}}">
                    <div class="col-md-12 form-group">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <p>Cancel Reservation?<input class="btn btn-danger pull-right" type="submit" name="yes" value="Yes"></p>
                    </div>
                  </form>
                  <br>
                  <div class="col-md-12 form-group"><strong>Slot<button type="button" class="btn btn-info pull-right slotbtn">View</button></strong></div>
                @endif
                @if($reservations->status != "RESERVED")
                  <br>
                  <div class="col-md-12 form-group"><strong>Slot<button type="button" class="btn btn-info pull-right slotbtn">View</button></strong></div>
                  <br>
                  <div class="col-md-12 form-group">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <strong>Session
                    <a class="btn btn-primary pull-right" href="{{route('motorist.reserve.extends',$reservations->id)}}">Extends</a>
                    <a class="btn btn-success pull-right" href="{{route('motorist.reserve.pay', $reservations->id)}}" id="pay">Pay</a>
                  </strong>
                  </div>
                @endif
              </div>
            </div>
          </div>
        @endisset
      </div>
    </div>
  </div>
@stop()
@section('script')
  <script>
    $(function(){
      @isset($reservations)
        var resID = '{{$reservations->id}}';
        var countDownDate = new Date('{{$reservations->departure}}').getTime();
        var countReserveDate = new Date('{{$reservations->arrival}}').getTime();
        var countDate = new Date('{{$reservations->dates}}').getTime();
        var countStatus = '{{$reservations->status}}';
        var x = setInterval(function() {
          var now = new Date().getTime();
          if(countStatus == 'OCCUPIED'){
            if(countDownDate > now){
                console.log('departure')
                var distance = countDownDate - now;
            }else{
              console.log('dates')
                var distance = countDate - now;
            }
          }else if(countStatus == 'RESERVED'){
            var distance = countReserveDate - now;
          }
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          if (days == 0){ document.getElementById("timer").innerHTML = hours + "h "+ minutes + "m " + seconds + "s ";}
          if (days > 0){ document.getElementById("timer").innerHTML = days + "d "+ hours + "h "+ minutes + "m " + seconds + "s ";}
          if ((hours == 0) && (days==0)){ document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";}
          if ((minutes == 0) && (hours == 0) && (days==0)){document.getElementById("timer").innerHTML = seconds + "s ";}
          if ((hours == 0) && (minutes == 15) && (seconds == 0)){
            if(countStatus == 'OCCUPIED'){
              $('.textHead').text('Reminder');
              $('.textBody').text('You only have 15 minutes left before your parking session ends.');
              $('#reminderModal').modal('show');
            }
          }
          if ((hours == 0) && (minutes == 0) && (seconds == 0)){
            if(countStatus == 'RESERVED'){
              console.log('start');
              $.ajax({
                url:'/motorist/reserve/expire',
                type: 'POST',
                data:{
                  '_token': $('input[name=_token]').val(),
                  'id':resID
                },success:function(data){
                  $('#timer').html("EXPIRED");
                  $('#timesModal').modal('show');
                }
              });
            }
            if(countStatus == 'OCCUPIED'){
                $('.textHead').text('Alert');
                $('.textBody').text('You only have 5 minutes left in order for you to pay your parking session.');
                $('#reminderModal').modal('show');
            }
          }
          if (distance < 0) {
            clearInterval(x);
          }
        }, 1000);
        var destination = "{{$reservations->location}}";
        var pos, destID;
        var latLngDest = "{{$reservations->latitude}}"+", "+"{{$reservations->longitude}}";


        var latLng = function(pos){
          var accuracy  = pos.coords.accuracy,
              lat       = pos.coords.latitude,
              lng       = pos.coords.longitude,
              coords    = lat + ', '+ lng;
              console.log(coords);
              console.log(latLngDest);
              $('#gmap').attr('src','https://www.google.com/maps/embed/v1/directions?key=AIzaSyBPLs7IVLikaskFS2ljpSwf3Z3UAI7zzSE&origin='+coords+'&destination='+latLngDest+'&mode=driving');
          }
        var err = function(error){
          if(error.code === 1){
            alert('Unable to get Location');
          }
        }
        navigator.geolocation.watchPosition(latLng, err,{
          enableHighAccuracy: true,
          maximumAge: 3000,
          timeout:30000
        });
      @else
        var latLng = function(pos){
          var accuracy  = pos.coords.accuracy,
              lat       = pos.coords.latitude,
              lng       = pos.coords.longitude,
              coords    = lat + ', '+ lng;
              $('#gmap').attr('src','https://maps.google.com.ph?q='+coords+'&z=13&output=embed');
          }
        var err = function(error){
          if(error.code === 1){
            alert('Unable to get Location');
          }
        }
        navigator.geolocation.getCurrentPosition(latLng, err);
      @endisset
    })
  </script>
@stop
