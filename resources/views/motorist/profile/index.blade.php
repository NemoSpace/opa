@extends('motorist.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="content">
    <div class="col-md-3 text-sm-center">
      <aside class="profile-nav alt">
        <section class="card">
          <div class="card-header user-header alt bg-dark">
            <div class="media">
              <a href="">
                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('/public/'.$motorists->image)}}">
              </a>
              <div class="media-body">
                <h2 class="text-light display-6">{{$motorists->name}}</h2>
                <p class="text-light">Motorist</p>
              </div>
            </div>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item"><a><i class="fa fa-envelope-o"></i> {{$motorists->email}}</a></li>
            <li class="list-group-item"><a><i class="fa fa-map-marker"></i> {{$motorists->address}}</a></li>
            <li class="list-group-item"><a><i class="fa fa-mobile "></i> {{$motorists->number}}</a></li>
            <li class="list-group-item"><a><i class="fa fa-venus-mars"></i> {{$motorists->sex}}</a></li>
            <li class="list-group-item">
              <a href="{{url('motorist/profile/edit')}}" class="btn btn-success btn-round form-group">Edit</a>
              <a href="{{route('motorist.profile.changepass')}}" class="btn btn-info btn-round form-group">Change Password</a>
            </li>
          </ul>
        </section>
      </aside>
    </div>
    <div class="col-md-9">
      <div class="card">
        <div class="card-header">
            @if(session()->has('message'))
            <div class="alert alert-success">
              {{ session()->get('message') }}
            </div>
            @endif
          <h4 class="title">Vehicle</h4>
          <p class="category">Motorist list of vehicles</p>
          <a href="{{route('motorist.vehicles.create')}}" class="btn btn-sm btn-info pull-right">New Vehicle</a>
        </div>
        <div class="card-body">
          <table class="table table-bordered" id="vehicleTable">
            <thead>
              <tr>
                <th>Model</th>
                <th>Plate Number</th>
                <th>Type</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vehicles as $vehicle)
                <tr>
                  <td>{{$vehicle->model }}</td>
                  <td>{{$vehicle->plate_number }}</td>
                  <td>{{$vehicle->type }}</td>
                  <td>
                    <div class="btn-group">
                      <a class="btn btn-success btn-sm" href="{{ route('motorist.vehicles.update', $vehicle->id) }}"><i class="fa fa-edit"></i> Edit</a>
                      <form action="{{ route('motorist.vehicles.destroy', $vehicle->id) }}" method="POST" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="Delete">
                        <button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i>Delete</button>
                      </form>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#vehicleTable').DataTable()
    })
  </script>
@stop
