@extends('motorist.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card-body bg-flat-color-5">
          <div class="h1 text-muted text-right mb-4">
            <i class="ti-bell text-light"></i>
          </div>
          <div class="h4 mb-0 text-light">
            <span class="title" id="timer">No Reservation</span>
          </div>
          <small class="text-uppercase font-weight-bold text-light">Reservation</small>
          <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card-body bg-flat-color-4">
          <div class="h1 text-muted text-right mb-4">
            <i class="ti-car text-light"></i>
          </div>
          <div class="h4 mb-0 text-light">
            <span class="title">{{$vehicle}}</span>
          </div>
          <small class="text-uppercase font-weight-bold text-light">Vehicles</small>
          <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card-body bg-flat-color-3">
          <div class="h1 text-muted text-right mb-4">
            <i class="ti-alert text-light"></i>
          </div>
          <div class="h4 mb-0 text-light">
            <span class="title">{{$unpaid}}</span>
          </div>
          <small class="text-uppercase font-weight-bold text-light">Unpaid</small>
          <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card-body bg-flat-color-2">
          <div class="h1 text-muted text-right mb-4">
            <i class="ti-money text-light"></i>
          </div>
          <div class="h4 mb-0 text-light">
            <span class="title">{{$paid}}</span>
          </div>
          <small class="text-uppercase font-weight-bold text-light">Paid</small>
          <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <h4 class="mb-3">Most Reserved Lot </h4>
            <canvas id="pieLot"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <h4 class="mb-3">Most Reserved Slot </h4>
            <canvas id="pieSlot"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <h4 class="mb-3">Most Canceled Slot </h4>
            <canvas id="pieCanceled"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <h4 class="mb-3">Monthly Payments</h4>
            <canvas id="linePayment"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop()
@section('modal')
  @include('motorist.payment.remind')
  @include('motorist.payment.expire')
  @include('motorist.reserve.map')
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      @isset($reservations)
        var resID = '{{$reservations->id}}';
        var countDownDate = new Date('{{$reservations->departure}}').getTime();
        var countReserveDate = new Date('{{$reservations->arrival}}').getTime();
        var countDate = new Date('{{$reservations->dates}}').getTime();
        var countStatus = '{{$reservations->status}}';
        var x = setInterval(function() {
          var now = new Date().getTime();
          if(countStatus == 'OCCUPIED'){
            if(countDownDate > now){
                console.log('departure')
                var distance = countDownDate - now;
            }else{
              console.log('dates')
                var distance = countDate - now;
            }
          }else if(countStatus == 'RESERVED'){
            var distance = countReserveDate - now;
          }
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          if (days == 0){ document.getElementById("timer").innerHTML = hours + "h "+ minutes + "m " + seconds + "s ";}
          if (days > 0){ document.getElementById("timer").innerHTML = days + "d "+ hours + "h "+ minutes + "m " + seconds + "s ";}
          if ((hours == 0) && (days==0)){ document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";}
          if ((minutes == 0) && (hours == 0) && (days==0)){document.getElementById("timer").innerHTML = seconds + "s ";}
          if ((hours == 0) && (minutes == 15) && (seconds == 0)){
            if(countStatus == 'OCCUPIED'){
              $('.textHead').text('Reminder');
              $('.textBody').text('You only have 15 minutes left before your parking session ends.');
              $('#reminderModal').modal('show');
            }
          }
          if ((hours == 0) && (minutes == 0) && (seconds == 0)){
            if(countStatus == 'RESERVED'){
              console.log('start');
              $.ajax({
                url:'/motorist/reserve/expire',
                type: 'POST',
                data:{
                  '_token': $('input[name=_token]').val(),
                  'id':resID
                },success:function(data){
                  $('#timer').html("EXPIRED");
                  $('#timesModal').modal('show');
                }
              });
            }
            if(countStatus == 'OCCUPIED'){
                $('.textHead').text('Alert');
                $('.textBody').text('You only have 5 minutes left in order for you to pay your parking session.');
                $('#reminderModal').modal('show');
            }
          }
          if (distance < 0) {
            clearInterval(x);
          }
        }, 1000);
      @else
      @endisset

      // Lot Chart
      $.ajax({
        url: '/motorist/chart/lot',
        type: "GET",
        success: function(data) {
          console.log(data);
          var location = [];
          var lots = [];
          var coloR = [];
          var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
          };
          $.each(data, function(index, listObj) {
            location.push(listObj.location);
            lots.push(listObj.lote);
            coloR.push(dynamicColors());
          });
          var chartData = {
            labels: location,
            datasets: [{
              label: 'Ratings',
              backgroundColor: coloR,
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: lots
            }]
          };
          var ctx = $("#pieLot");
          var barGraph = new Chart(ctx, {
            type: 'pie',
            data: chartData
          })
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status);
        },
      });

      //Slot Charts
      $.ajax({
        url: '/motorist/chart/slot',
        type: "GET",
        success: function(data) {
          console.log(data);
          var code = [];
          var slots = [];
          var coloR = [];
          var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
          };
          $.each(data, function(index, listObj) {
            code.push(listObj.code);
            slots.push(listObj.slote);
            coloR.push(dynamicColors());
          });
          var chartData = {
            labels: code,
            datasets: [{
              label: 'Ratings',
              backgroundColor: coloR,
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: slots
            }]
          };
          var ctx = $("#pieSlot");
          var barGraph = new Chart(ctx, {
            type: 'pie',
            data: chartData
          })
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status);
        },
      });

      //Cancel Charts
      $.ajax({
        url: '/motorist/chart/cancel',
        type: "GET",
        success: function(data) {
          console.log(data);
          var code = [];
          var slots = [];
          var coloR = [];
          var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
          };
          $.each(data, function(index, listObj) {
            code.push(listObj.code);
            slots.push(listObj.clote);
            coloR.push(dynamicColors());
          });
          var chartData = {
            labels: code,
            datasets: [{
              label: 'Ratings',
              backgroundColor: coloR,
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: slots
            }]
          };
          var ctx = $("#pieCanceled");
          var barGraph = new Chart(ctx, {
            type: 'pie',
            data: chartData
          })
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status);
        },
      });


      // Payment Chart
      $.ajax({
        url: '/motorist/chart/payment',
        type: "GET",
        success: function(data) {
          console.log(data);
          var mnth = [];
          var pays = [];
          $.each(data, function(index, listObj) {
            mnth.push(listObj.monthName.name);
            pays.push(listObj.pay);
          });
          var chartData = {
            labels: mnth,
            datasets: [{
              label: 'Monthly Payments',
              backgroundColor: 'rgba(0,103,255,.15)',
              borderColor: 'rgba(0,103,255,0.5)',
              borderWidth: 3.5,
              pointStyle: 'circle',
              pointRadius: 5,
              pointBorderColor: 'transparent',
              pointBackgroundColor: 'rgba(0,103,255,0.5)',
              data: pays
            }]
          };
          var ctx = $("#linePayment");
          var barGraph = new Chart(ctx, {
            type: 'line',
            data: chartData,
            options: {
              responsive: true,
              tooltips: {
                mode: 'index',
                titleFontSize: 12,
                titleFontColor: '#000',
                bodyFontColor: '#000',
                backgroundColor: '#fff',
                titleFontFamily: 'Montserrat',
                bodyFontFamily: 'Montserrat',
                cornerRadius: 3,
                intersect: false,
              },
              legend: {
                display: false,
                position: 'top',
                labels: {
                  usePointStyle: true,
                  fontFamily: 'Montserrat',
                },
              },
              scales: {
                xAxes: [ {
                  display: true,
                  gridLines: {
                    display: false,
                    drawBorder: false
                  },
                  scaleLabel: {
                    display: false,
                    labelString: 'Month'
                  }
                } ],
                yAxes: [ {
                  display: true,
                  gridLines: {
                    display: false,
                    drawBorder: false
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'Value'
                  }
                } ]
              },
              title: {
                display: false,
              }
            }
          })
        },
        error: function (xhr, ajaxOptions, thrownError) {
          //alert(xhr.status);
        },
      });
    })
  </script>
@stop
