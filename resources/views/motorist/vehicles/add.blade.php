@extends('motorist.layouts.app')
@section('title')
  <title>{{ config('app.name', 'OPA')}}-Vehicle</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Vehicle</a>
@stop
@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 offset-md-4">
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
        		<div class="card">
	          		<div class="card-header" data-background-color="blue">
	            		<h4 class="title">Add Vehicle</h4>
	            		<p class="category">Register your vehicle here</p>
	          		</div>
	          		<form method="POST" action="{{ route('motorist.vehicles.insert')}}">
	          			<div class="card-body">
	          			
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="plate_number">Plate Number</label>
								<input class="form-control" id="plate_number" name="plate_number" style="text-transform:uppercase" required minlength="6" maxlength="7">
							</div>
							<div class="form-group">
								<label for="model">Model</label>
								<input class="form-control" id="model" name="model" style="text-transform:uppercase"  required>
							</div>
							<div class="form-group">
								<label for="type">Type</label>
								<select name="type" class="form-control" id="type" class="form-control">
									<option value="" selected="selected">-- Vehicle Type --</option>
									@foreach($vcategories as $vcategory)
										<option value="{{$vcategory->id}}">{{$vcategory->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="card-footer text-center">
							<input class="btn btn-success" type="submit" name="submit" value="submit">
						</div>
					</form>
	      		</div>
	  		</div>
		</div>
	</div>
@stop
@section('script')
	<script type="text/javascript">
		$(function() {
    		$('input').focusout(function() {
        		// Uppercase-ize contents
        		this.value = this.value.toLocaleUpperCase();
    		});
		});	
	</script>
@stop
