<div id="errorModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title">Error</h4></div>
			<div class="modal-body">
				<div class="tim-typo">
					<p class="text-danger" id="txtError"></p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">
					<span class='glyphicon glyphicon-remove'></span> Close
				</button>
			</div>
		</div>
	</div>
</div>
