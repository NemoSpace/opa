@extends('layouts.auth')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
      <div class="card-header" data-background-color="red">
          <h3 class="title">Terms and Conditions</h4>
      </div>
      <div class="card-body">
          <h4><b>• Items Left in Vehicle.</b> Online Parking Reservation or the parking lot shall not be responsible for damage or loss to possessions or items left in your vehicle.</h4>
          <h4><b>• Damage to Vehicle.</b> Online Parking Reservation or the parking lot shall not be responsible for damage to your vehicle, whether or not such damage is caused by other vehicle(s) or person(s) in the parking lot and surrounding area.</h4>
          <h4><b>• Receipts.</b>  Online Parking Reservation or the parking lot agrees to provide a receipt for each payment received.  Such receipt shall show the amount paid and number of the leased parking space.</h4>
          <h4><b>• Parking Lot Attendants. </b>  the parking lot shall, shall not, may provide parking attendants.  In the event that the parking lot provides such attendants, any use of such attendant to park or drive your vehicle shall be at your request, direction and sole risk of any resulting loss and you shall indemnify the parking lot facility for any loss resulting from such use.</h4>
          <h4><b>• Damage to Premises.</b>  Should you cause any damages beyond normal wear and tear to the building or facility where the parking space is located, you will be held responsible for replacement   or loss of any stolen, damaged, or misplaced property.</h4>
          <h4><b>• Accounts.</b> You agree that you will not create any false account with Online Parking Reservation or use your account with Online Parking Reservation for any immoral or illegal activity or purpose including (without limit) malicious or fraudulent bookings or money laundering..</h4>
          <h4><b>• Service Agreement. </b> As a Lot Owner you agree to use Online Parking Reservation as your exclusive agent for the purposes of making and accepting bookings from Motorist</h4>
          <h4><b>• Termination. </b> Either party may terminate this Agreement at any time. From the date of termination, we will not confirm or accept any new bookings for the Parking Space until this Parking Space passes a quality check by Online parking reservation, and Online parking reservation will relocate outstanding bookings to the nearest possible site, and or issue a discount.</h4>
          <h4><b>• Violation.</b> In an instance where you reserve a parking slot and decide not to park in your designated parking slot, a fine will be issue to you. </h4>
          <h4><b>• Time. </b> Upon Reservation you have an allowance of 30 minutes exclusive to your parking time, within the said time you can cancel your reservation without a fee but if the time exceeds and you didn't arrive at the designated location you will be charge and the reservatoin will stop.In your next transaction the unpaid balance will be added to your current payment price. The system will notifity you 15 minutes before the time runs out. After the expiration of the parking session a 5 minutes will be given to you to pay and leave the premises.</h4>
      </div>
       <div class="card-footer">
        <center>
          <button type="button" class="btn btn-success"><a href="{{ url()->previous() }}">Back</a></button>
        </center>
      </div>
    </div>
  </div>
</div>

@stop()
