<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>

        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="one page, business template, single page, onepage, responsive, parallax, creative, business, html5, css3, css3 animation">
        <meta charset="utf-8">
        <title>Online Parking Reservation</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('/public/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/jquery.fancybox.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/app.css') }}" >
    </head>
    <body>

        @yield('content')

        @include('includes.footer')
        <script src="{{ asset('/public/js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <script src="{{ asset('/public/js/vendor/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('/public/js/bootstrap.min.j') }}s"></script>
        <script src="{{ asset('/public/js/jquery.nav.js') }}"></script>
        <script src="{{ asset('/public/js/jquery.mixitup.min.js') }}"></script>
        <script src="{{ asset('/public/js/jquery.fancybox.pack.js') }}"></script>
        <script src="{{ asset('/public/js/jquery.parallax-1.1.3.js') }}"></script>
        <script src="{{ asset('/public/js/jquery.appear.js') }}"></script>
        <script src="{{ asset('/public/js/jquery-countTo.js') }}') }}"></script>
        <script src="{{ asset('/public/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('/public/js/wow.min.js') }}"></script>
        <script src="{{ asset('/public/js/main.js') }}"></script>
        @yield('script')
    </body>
</html>
