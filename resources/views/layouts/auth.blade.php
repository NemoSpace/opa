<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{url('/asset/img/apple-icon.png')}}">
        <link rel="icon" type="image/png" href="{{url('/assets/img/favicon.png')}}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <link href="{{url('/public/asset/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{url('/public/asset/css/now-ui-kit.css?v=1.1.0')}}" rel="stylesheet" />
        <link href="{{url('/public/asset/css/demo.css')}}" rel="stylesheet" />
    </head>
    <body class="login-page sidebar-collapse">
        <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
            <div class="container">
                <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="../public/assets/img/blurred-image-1.jpg">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/')}}">Back to Homepage</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
        @yield('script')
    </body>
    <script src="{{url('/public/asset/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/public/asset/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/public/asset/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/public/asset/js/plugins/bootstrap-switch.js')}}"></script>
    <script src="{{url('/public/asset/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/public/asset/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{url('/public/asset/js/now-ui-kit.js?v=1.1.0')}}" type="text/javascript"></script>
</html>
