@extends('layouts.auth_layout')
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><b>MoRPS</b> version 1.0</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form class="form-horizontal" method="POST" action="{{ route('success') }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <select name="usertype" class="form-control" id="user_id-field" name="user_id" class="form-control">
                        <option value="" selected="selected">-- User Type --</option>
                        <option value="motorist">Motorist</option>
                        <option value="owner">Parking Lot Owner</option>
                    </select>
                </div>
                <div class="form-group has-feedback">
                    <input name="email" type="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input name="password" type="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </form>
            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
            </div> -->
   
            <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>
        </div>
    </div>
@stop()
@section('script')
    <script src="{{asset('bootstrap/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{asset('bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{asset('bootstrap/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        });
    </script>
@stop()
