@extends('layouts.auth_layout')
@section('content')
  <div class="register-box">
    <div class="register-logo">
      <a href="{{ url('/') }}"><b>MoRPS</b> version 1.0</a>
    </div>
    <div class="register-box-body">
      <p class="login-box-msg">Register a new membership</p>
        <form class="form-horizontal" method="POST" action="{{route('create_register')}}">
          {{ csrf_field() }}
         <div class="form-group has-feedback">
            <select name="usertype" class="form-control" class="form-control">
                <option value="" selected="selected">-- User Type --</option>
                <option value="motorist">Motorist</option>
                <option value="owner">Parking Lot Owner</option>
            </select>
          </div>
          <div class="form-group has-feedback">
            <input name="name" type="text" class="form-control" placeholder="Full name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <select name="sex" class="form-control" class="form-control">
              <option value="" selected="selected">-- Gender --</option>
              <option value="female">Female</option>
              <option value="male">Male</option>
            </select>
          </div>
          <div class="form-group has-feedback">
            <input name="email" type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="number" id="number" type="text" class="form-control" placeholder="Contact Number">
            <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="address" type="text" class="form-control" placeholder="Address">
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="username" type="text" class="form-control" placeholder="User name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password" id="password" type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="confirm" id="confirm" type="password" class="form-control" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label><input type="checkbox"> I agree to the <a href="#">terms</a></label>
              </div>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
          </div>
      </form>
      <!-- <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      </div> -->
      <a href="{{ url('login') }}" class="text-center">I already have a membership</a>
    </div>
  </div>
@stop() 
@section('script')
  <script src="{{asset('bootstrap/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('bootstrap/plugins/iCheck/icheck.min.js')}}"></script>
  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
      $("#number").keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
      });
    });
  </script>
@stop()