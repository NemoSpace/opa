@extends('layouts.auth')
@section('content')
	<div class="page-header" filter-color="orange">
	    <div class="page-header-image" style="background-image:url({{url('/public/asset/img/bg7.jpg')}}"></div>
	    <div class="container">
	        <div class="col-md-4 content-center">
	            <div class="card card-login card-plain">
	                @if (Session::has('message'))
	                	<div class="alert alert-info">{{ Session::get('message') }}</div>
	                @endif
	          	</div>
	         </div>
	    </div>
	</div>
@endsection
