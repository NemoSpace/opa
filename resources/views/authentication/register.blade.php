@extends('layouts.auth')
@section('content')
    @if (Session('user') == 'admin')
        <div class="section section-signup" style="background-image: url({{url('/public/asset/img/bg11.jpg')}}); background-size: cover; background-position: top center; min-height: 700px;" filter-color="orange">
            <div class="container">
                <div class="row">
                    <div class="card card-signup" data-background-color="orange">
                        <br><br><br><br>
                        <form class="form-horizontal" method="POST" action="{{url('/register')}}"
                            onsubmit="document.getElementById('submit').disabled=true;
                            document.getElementById('submit').value='Submitting, please wait...';">
                            {{ csrf_field() }}
                            <div class="header text-center">
                                <h1 class="title title-up">ADMIN</h1>
                                <h4 class="title title-up">Sign Up</h4>
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="input-group form-group-no-border">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons users_circle-08"></i>
                                    </span>
                                    <input name="name" type="text" class="form-control" placeholder="Full Name">
                                </div>
                                <div class="input-group form-group-no-border">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons ui-1_email-85"></i>
                                    </span>
                                    <input name="email" type="text" class="form-control" placeholder="Email">
                                </div>
                                <div class="input-group form-group-no-border">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons location_world"></i>
                                    </span>
                                    <input name="address" type="text" class="form-control" placeholder="Address">
                                </div>
                                <div class="input-group form-group-no-border">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                    </span>
                                    <input name="password" id="password" type="password"  placeholder="Password" class="form-control">
                                </div>
                                <div class="input-group form-group-no-border">
                                    <span class="input-group-addon">
                                        <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                    </span>
                                    <input name="confirm" id="confirm" type="password"  placeholder=" Confirm Password" class="form-control">
                                </div>
                                <div class="checkbox">
                                    <input id="checkboxSignup" type="checkbox" name="checkboxSignup">
                                    <label for="checkboxSignup"> I agree to the <a href="{{route('terms')}}">terms</a></label>
                                </div>
                            </div>
                            <div class="footer text-center">
                                <button type="submit" id="submit" class="btn btn-neutral btn-round btn-lg">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="section section-signup" style="background-image: url({{url('/public/asset/img/bg11.jpg')}}); background-size: cover; background-position: top center; min-height: 700px;" filter-color="orange">
          <div class="container">
            <div class="row">
              <div class="card card-signup" data-background-color="orange">
                <form class="form-horizontal" method="POST" action="{{url('/register')}}"
                  onsubmit="document.getElementById('submit').disabled=true;
                  document.getElementById('submit').value='Submitting, please wait...';">
                  {{ csrf_field() }}
                  <div class="header text-center">
                    <h4 class="title title-up">Sign Up</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                        </div>
                    @endif
                  </div>
                  <div class="card-body">
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons users_circle-08"></i>
                      </span>
                      <input name="name" type="text" class="form-control" placeholder="Full Name" required autofocus>
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons users_single-02"></i>
                      </span>
                      <select name="sex" class="form-control">
                        <option data-background-color="orange" value="" selected="selected">-- Gender --</option>
                        <option data-background-color="orange" value="female">Female</option>
                        <option data-background-color="orange" value="male">Male</option>
                      </select>
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons ui-1_email-85"></i>
                      </span>
                      <input name="email" type="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons travel_info"></i>
                      </span>
                      <input name="number" type="text" class="form-control" onkeypress="return isNumberKey(event);" placeholder="Contact Number" required minlength="11" maxlength="11">
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons location_world"></i>
                      </span>
                      <input name="address" type="text" class="form-control" placeholder="Address" required minlength="10">
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons ui-1_lock-circle-open"></i>
                      </span>
                      <input name="password" id="password" type="password"  placeholder="Password" class="form-control" required minlength="7">
                    </div>
                    <div class="input-group form-group-no-border">
                      <span class="input-group-addon">
                        <i class="now-ui-icons ui-1_lock-circle-open"></i>
                      </span>
                      <input name="confirm" id="confirm" type="password"  placeholder=" Confirm Password" class="form-control" required minlength="7">
                    </div>
                    <div class="checkbox">
                      <input id="checkboxSignup" type="checkbox" name="checkboxSignup" required>
                      <label for="checkboxSignup"> <a href="{{route('terms')}}" data-toggle="tooltip" data-placement="bottom" title="Read Terms And Conditions"> I agree to the terms</a></label>
                    </div>
                  </div>
                  <div class="footer text-center">
                    <!-- <a href="{{ url('/social/login/facebook')}}" class="btn btn-warning btn-round btn-lg"><i class="fa fa-facebook-square"></i> Sign in using Facebook</a> -->
                    <button type="submit" id="submit" class="btn btn-neutral btn-round btn-lg">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    @endif
@stop()
@section('script')
    <script type="text/javascript">
      function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
      }
    </script>
@stop
