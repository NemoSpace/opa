@extends('layouts.auth')
@section('content')
<div class="page-header" filter-color="orange">
    <div class="page-header-image" style="background-image:url({{url('/public/asset/img/login.jpg')}}"></div>
    <div class="container">
        <div class="col-md-4 content-center">
            <div class="card card-login card-plain">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <div class="content">
                        <div class="input-group form-group-no-border input-lg {{ $errors->has('email') ? 'has-error' : '' }}">
                            <span class="input-group-addon"><i class="now-ui-icons ui-1_email-85"></i></span>
                            <input name="email" type="Email" class="form-control" placeholder="Email Address" required>
                        </div>
                        <div class="input-group form-group-no-border input-lg {{ $errors->has('password') ? 'has-error' : '' }}">
                            <span class="input-group-addon"><i class="now-ui-icons ui-1_lock-circle-open"></i></span>
                            <input name="password" type="password"  placeholder="Password" class="form-control" required minlength="6">
                        </div>
                    </div>
                    <div class="footer text-center">
                        <!-- @if (Session('user') != 'admin')
                        <a href="{{ url('/social/login/facebook')}}" class="btn btn-warning btn-round btn-lg btn-block"><i class="fa fa-facebook-square"></i> Sign in using Facebook</a>
                        @endif -->
                        <button type="submit" class="btn btn-info btn-round btn-lg btn-block">Get Started</button>
                    </div>
                    <div class="pull-left">
                        <h6>
                        @if (Session('user') == 'admin')
                        <a href="{{ url('admin/register') }}" class="link">Create Account</a>
                        @elseif(Session('user') == 'motorist')
                        <a href="{{ url('/motorist/register')}}" class="link">Create Account</a>
                        @elseif(Session('user') == 'owner')
                        <a href="{{ url('/owner/register')}}" class="link">Create Account</a>
                        @endif
                        </h6>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop()
