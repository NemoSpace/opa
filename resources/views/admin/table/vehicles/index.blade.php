@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Vehicle List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="vehicleTable">
              <thead class="text-danger">
                <th>Motorist</th>
                <th>Plate Number</th>
                <th>Model</th>
                <th>Type</th>
                <th>Status</th>
              </thead>
              <tbody>
                @foreach($vehicles as $vehicle)
                <tr>
                  @if($vehicle->name)
                  <td>{{$vehicle->name}}</td>
                  @else
                  <td>Not Registered</td>
                  @endif
                  <td>{{$vehicle->plate_number}}</td>
                  <td>{{$vehicle->model}}</td>
                  <td>{{$vehicle->type}}</td>
                  @if($vehicle->status == 1)
                  <td>active</td>
                  @else
                  <td>inactive</td>
                  @endif
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#vehicleTable').DataTable()
    })
  </script>
@stop
