@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Ratings List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="ratingsTable">
              <thead class="text-danger">
                <th>Name</th>
                <th>Rate</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($ratings as $rating)
                <tr>
                  <td>{{$rating->name}}</td>
                  <td>{{$rating->rate}}</td>
                  <td>{{$rating->date}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#ratingsTable').DataTable()
    })
  </script>
@stop
