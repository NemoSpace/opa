@extends('admin.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Payment List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="paymentTable">
              <thead class="text-danger">
                <th>Motorist</th>
                <th>Owner</th>
                <th>Slot Code</th>
                <th>Price</th>
                <th>Status</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($payments as $payment)
                <tr>
                  @if($payment->motorist)
                  <td>{{$payment->motorist}}</td>
                  @else
                  <td>Not Registered</td>
                  @endif
                  <td>{{$payment->owner}}</td>
                  <td>{{$payment->code}}</td>
                  <td>{{$payment->price}}</td>
                  <td>{{$payment->status}}</td>
                  <td>{{$payment->updated_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#paymentTable').DataTable()
    })
  </script>
@stop
