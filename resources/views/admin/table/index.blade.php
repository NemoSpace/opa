@extends('admin.layouts.app')
@section('title')
    <title>{{ config('app.name', 'OPA')}}-Tables</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Tables </a>
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header" data-background-color="red">
                        <h4 class="title">Table List</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table" id="tables">
                            <thead class="text-danger">
                                <th>Table Name</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @forelse($tables as $table)
                                    @if(($table->Tables_in_u830136546_opa != 'motorists') && ($table->Tables_in_u830136546_opa != 'admins') && ($table->Tables_in_u830136546_opa != 'migrations') && ($table->Tables_in_u830136546_opa != 'owners') && ($table->Tables_in_u830136546_opa != 'users'))
                                        @if($table->Tables_in_u830136546_opa == 'vcategories')
                                            <tr>
                                                <td>vehicle categories</td>
                                                <td><a class="btn btn-primary" href="{{route('admin.table.view',$table->Tables_in_u830136546_opa)}}">View</a></td>
                                            </tr>
                                        @elseif($table->Tables_in_u830136546_opa == 'scategories')
                                            <tr>
                                                <td>slot categories</td>
                                                <td><a class="btn btn-primary" href="{{route('admin.table.view',$table->Tables_in_u830136546_opa)}}">View</a></td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td>{{$table->Tables_in_u830136546_opa}}</td>
                                                <td><a class="btn btn-primary" href="{{route('admin.table.view',$table->Tables_in_u830136546_opa)}}">View</a></td>
                                            </tr>
                                        @endif

                                    @endif
                                @empty
                                    You have Empty Table update your Database
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
