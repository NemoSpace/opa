@extends('admin.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <style type="text/css">
    .myimage{
      width: 200px !important;
      height : 200px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header" data-background-color="red">
            <h4 class="title">Slot List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="slotTable">
              <thead class="text-danger">
                <th>Image</th>
                <th>Location</th>
                <th>Address</th>
                <th>Type</th>
                <th>Code</th>
                <th>Price</th>
                <th>Status</th>
              </thead>
              <tbody>
                @foreach($slots as $slot)
                <tr>
                  <td><img class="img myimage" src="{{ asset('/public/'.$slot->image)}}"/></td>
                  <td>{{$slot->location}}</td>
                  <td>{{$slot->address}}</td>
                  <td>{{$slot->name}}</td>
                  <td>{{$slot->code}}</td>
                  <td>{{$slot->price}}</td>
                  <td>{{$slot->status}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(function(){
$('#slotTable').DataTable()
})
</script>
@stop
