@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Reservation List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="reservationTable">
              <thead class="text-danger">
                <th>Vehicle</th>
                <th>Slot Code</th>
                <th>Price</th>
                <th>Reserved</th>
                <th>Arrived</th>
                <th>Departed</th>
                <th>Status</th>
              </thead>
              <tbody>
                @foreach($reservations as $reservation)
                <tr>
                  <td>{{$reservation->plate_number}}</td>
                  <td>{{$reservation->code}}</td>
                  <td>{{$reservation->price}}</td>
                  <td>{{$reservation->reserved}}</td>
                  <td>{{$reservation->arrival}}</td>
                  <td>{{$reservation->departure}}</td>
                  <td>{{$reservation->status}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#reservationTable').DataTable()
    })
  </script>
@stop
