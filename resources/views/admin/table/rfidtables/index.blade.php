@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Dialy RFIDs List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="rfidTableTable">
              <thead class="text-danger">
                <th>RFID</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($rfidtables as $rfidtable)
                <tr>
                  <td>{{$rfidtable->rfid_id}}</td>
                  <td>{{$rfidtable->created_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#rfidTableTable').DataTable()
    })
  </script>
@stop
