@extends('admin.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <style type="text/css">
    .myimage{
      width: 200px !important;
      height : 200px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Lot List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="lotTable">
              <thead class="text-danger">
                <th>Image</th>
                <th>Owner</th>
                <th>Location</th>
                <th>Address</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($lots as $lot)
                <tr>
                  <td><img class="img myimage" src="{{ asset('/public/'.$lot->image)}}"/></td>
                  <td>{{$lot->name}}</td>
                  <td>{{$lot->location}}</td>
                  <td>{{$lot->address}}</td>
                  <td>{{$lot->latitude}}</td>
                  <td>{{$lot->longitude}}</td>
                  <td>{{$lot->updated_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#lotTable').DataTable()
    })
  </script>
@stop
