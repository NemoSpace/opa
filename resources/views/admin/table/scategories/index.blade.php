@extends('admin.layouts.app')
@section('css')
  <style type="text/css">
    .myimage{
      width: 200px !important;
      height : 200px !important;
    }
  </style>
@stop
@section('title')
    <title>{{ config('app.name', 'OPA')}}-Slot Category Tables</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Slot Category Tables </a>
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="red">
                        <h4 class="title">Slot Category List</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table" id="scategoryTable">
                            <thead class="text-danger">
                                <th>Location</th>
                                <th>Address</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach($scategories as $scategory)
                                    <tr>
                                        <td>{{$scategory->location}}</td>
                                        <td>{{$scategory->address}}</td>
                                        <td>{{$scategory->name}}</td>
                                        <td>{{$scategory->price}}</td>
                                        <td>{{$scategory->status}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th>Location</th>
                                <th>Address</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Status</th>
                            </tfoot>                     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
  <script src="{{ asset('bootstrap/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bootstrap/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script>
    $(function(){
      $('#scategoryTable').DataTable()
    })
  </script>
@stop


