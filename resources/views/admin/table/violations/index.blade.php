@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Violation List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="violationTable">
              <thead class="text-danger">
                <th>Name</th>
                <th>Location</th>
                <th>Plate Number</th>
                <th>Strike</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($violations as $violation)
                <tr>
                  <td>{{$violation->name}}</td>
                  <td>{{$violation->location}}</td>
                  <td>{{$violation->plate_number}}</td>
                  <td>{{$violation->strike}}</td>
                  <td>{{$violation->updated_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#violationTable').DataTable()
    })
  </script>
@stop
