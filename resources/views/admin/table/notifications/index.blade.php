@extends('admin.layouts.app')
@section('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Notification List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="notifTable">
              <thead class="text-danger">
                <th>Name</th>
                <th>Type</th>
                <th>Message</th>
                <th>Status</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($notifications as $notification)
                <tr>
                  <td>{{$notification->user_name}}</td>
                  <td>{{$notification->user_type}}</td>
                  <td>{{$notification->message}}</td>
                  <td>{{$notification->status}}</td>
                  <td>{{$notification->updated_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#notifTable').DataTable()
    })
  </script>
@stop
