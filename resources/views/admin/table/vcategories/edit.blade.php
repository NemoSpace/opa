@extends('motorist.layouts.app')
@section('title')
    <title>{{ config('app.name', 'OPA')}}-Edit Vehicle Category</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#">Edit Vehicle Category</a>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="card">
          <div class="card-header" data-background-color="blue">
            <h3 class="title"> Edit Vehicle Category</h3>
          </div>
          <form method="post" action="{{route('admin.table.vcategory.update', $vcategories->id)}}">
            <div class="card-content">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">              
              <input type="hidden" name="_method" value="PUT">
              <div class="form-group">
                <label for="name">Type:</label>
                <input type="text" class="form-control" placeholder="Type" name="name" value="{{$vcategories->name}}" required>
              </div> 
              <div class="form-group">
                <label for="status">Status:</label>
                <select name="status" class="form-control" id="status" class="form-control">
                    <option value="" selected="selected">-- Status --</option>
                    <option value="1">active</option>
                    <option value="2">inactive</option>
                </select>
              </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-success" type="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#status').val('{{$vcategories->status}}');
        });
    </script>
@stop
