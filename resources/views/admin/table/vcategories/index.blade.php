@extends('admin.layouts.app')
@section('css')
  <style type="text/css">
    .myimage{
      width: 200px !important;
      height : 200px !important;
    }
  </style>
@stop
@section('title')
    <title>{{ config('app.name', 'OPA')}}-Vehicle Category Tables</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Vehicle Category Tables </a>
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="red">
                        <h4 class="title">Vehicle Category List</h4>
                        <a href="{{route('admin.table.vcategory.add')}}" class="btn btn-sm btn-primary pull-right">New Category</a>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table" id="vcategoryTable">
                            <thead class="text-danger">                                
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($vcategories as $vcategory)
                                    <tr>                                        
                                        <td>{{$vcategory->name}}</td>
                                        @if($vcategory->status == 1)
                                            <td>active</td>
                                        @else
                                            <td>inactive</td>
                                        @endif
                                        <td>
                                            <a class="btn btn-success btn-sm" href="{{route('admin.table.vcategory.edit',$vcategory->id)}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                            <a class="btn btn-danger btn-sm"><i class="fa fa-remove"></i>Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tfoot>                     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
  <script src="{{ asset('bootstrap/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bootstrap/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script>
    $(function(){
      $('#vcategoryTable').DataTable()
    })
  </script>
@stop


