@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="title">Complaints List</h4>
          </div>
          <div class="card-content table-responsive">
            <table class="table" id="complaintTable">
              <thead class="text-danger">
                <th>Name</th>
                <th>Code</th>
                <th>Plate Number</th>
                <th>Complaint</th>
                <th>Status</th>
                <th>Date</th>
              </thead>
              <tbody>
                @foreach($complaints as $complaint)
                <tr>
                  @if($complaint->name)
                  <td>{{$complaint->name}}</td>
                  @else
                  <td>Not Registered</td>
									@endif
                  <td>{{$complaint->code}}</td>
                  <td>{{$complaint->plate_number}}</td>
                  <td>{{$complaint->complaint}}</td>
                  <td>{{$complaint->status}}</td>
                  <td>{{$complaint->updated_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#complaintTable').DataTable()
    })
  </script>
@stop
