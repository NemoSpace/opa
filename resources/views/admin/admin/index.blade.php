@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Admin
              @if($permission > 0)
                <a href="{{route('admin.admin.add')}}" class="btn btn-sm btn-primary btn-round pull-right">New Admin</a>
              @endif
            </strong>
          </div>
          <div class="card-body">
            <table id="adminTable" class="table table-bordered">
              <thead class="text-primary">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($admins as $adm)
                  <tr>
                    <td>{{$adm->id }}</td>
                    <td>{{$adm->name }}</td>
                    <td>{{$adm->email }}</td>
                    <td>{{$adm->address }}</td>
                    @if($adm->status == 1)
                      <td>Acive</td>
                    @else
                      <td>Inactive</td>
                    @endif
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-success btn-sm" href="{{route('admin.admin.edit',$adm->id)}}">Edit</a>
                        <a class="btn btn-danger btn-sm">Delete</a>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#adminTable').DataTable()
    })
  </script>
@stop
