@extends('admin.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 500px !important;
      height : 500px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="title">Admin</h4>
            <p class="category">Update Admin Info</p>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('admin.admin.update',$admins->id)}}"  enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <input type="file" name="file" id="file" onchange="AlertFilesize(this)">
              <div class="form-group">
                @if($admins->image == 'EMPTY')
                <img class="avatar border-gray" id="myimage" src="#"/>
                @else
                <img class="avatar border-gray" id="myimage" src="{{asset('/public/'.$admins->image)}}"  />
                @endif
              </div>
              <div class="form-group">
                <input name="name" type="text" class="form-control" placeholder="Full name" value="{{$admins->name}}">
              </div>
              <div class="form-group">
                <input name="email" type="email" class="form-control" placeholder="Email" value="{{$admins->email}}">
              </div>
              <div class="form-group">
                <input name="address" type="text" class="form-control" placeholder="Address" value="{{$admins->address}}">
              </div>
              <div class="form-group">
                <select name="permission" class="form-control" id="permission" class="form-control">
                  <option value="" selected="selected">-- Permission --</option>
                  <option value="superadmin">Super Admin</option>
                  <option value="admin">Admin</option>
                </select>
              </div>
              <div class="form-group">
                <select name="status" class="form-control" id="status" class="form-control">
                  <option value="" selected="selected">-- Status --</option>
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
                </select>
              </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript">
    @if($admin->permission == 'superadmin')
      $(document).ready(function () {
        $('#permission').val('{{$admins->permission}}');
        $('#status').val('{{$admins->status}}');
      });
    @endif
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
</script>
@stop
