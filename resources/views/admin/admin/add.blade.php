@extends('admin.layouts.app')
@section('css')
    <style type="text/css">
      #myimage{
        width: 500px !important;
        height : 500px !important;
      }
    </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="title">Admin</h4>
            <p class="category">Fill in Admin Info</p>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('admin.admin.save')}}"  enctype="multipart/form-data" onsubmit="document.getElementById('submit').disabled=true; document.getElementById('submit').value='Submitting, please wait...';">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="file" name="file" id="file" onchange="AlertFilesize(this)">
              <div class="form-group">
                <img class="img" id="myimage" src="#"/>
              </div>
              <div class="form-group">
                <input name="name" type="text" class="form-control" placeholder="Full name" required minlength="7">
              </div>
              <div class="form-group">
                <input name="email" type="email" class="form-control" placeholder="Email" required>
              </div>
              <div class="form-group">
                <input name="address" type="text" class="form-control" placeholder="Address" required minlength="10">
              </div>
              <div class="form-group">
                  <select name="permission" class="form-control" id="permission" class="form-control">
                    <option value="" selected="selected">-- Permission --</option>
                    <option value="superadmin">Super Admin</option>
                    <option value="admin">Admin</option>
                  </select>
              </div>
              <div class="form-group">
                <select name="status" class="form-control" id="status" class="form-control">
                  <option value="" selected="selected">-- Status --</option>
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
                </select>
              </div>
              <div class="form-group">
                <button type="submit" id="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript">
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
  </script>
@stop
