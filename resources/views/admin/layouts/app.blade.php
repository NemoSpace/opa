<!doctype html>
<html class="no-js" lang="">
  <head>
    @yield('css')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'OPA') }}</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{url('/public/sufee/assets/scss/style.css')}}">
    <link href="{{url('/public/sufee/assets/css/lib/vector-map/jqvmap.min.css')}}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    @yield('css')
  </head>
  <body>
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{url('/owner')}}">OPA</a>
            <a class="navbar-brand hidden" href="{{url('/owner')}}"><img src="{{url('/public/sufee/images/logo2.png')}}" alt="Logo"></a>
          </div>
          <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="{{ url('/admin')}}"><i class="menu-icon fa fa-dashboard"></i>Dashboard </a></li>
              <li><a href="{{ url('/admin/profile')}}"><i class="menu-icon ti-user"></i>Profile </a></li>
              <li><a href="{{route('admin.admin.list')}}"><i class="menu-icon fa fa-users"></i>Admin Table</a></li>
              <li><a href="{{ route('admin.motorist.list')}}"> <i class="menu-icon fa fa-user"></i>Motorist Table</a></li>
              <li><a href="{{ route('admin.owner.list')}}"><i class="menu-icon ti-user"></i>Owner Table</a></li>
              <li><a href="{{ route('admin.table')}}"> <i class=" menu-icon fa fa-table"></i>Table List</a></li>
              <li><a href="{{route('admin.backup.backups')}}"> <i class=" menu-icon fa fa-table"></i>Restore and Backup</a></li>
            </ul>
          </div>
        </nav>
      </aside>
      <div id="right-panel" class="right-panel">
        <header id="header" class="header">
          <div class="header-menu">
            <div class="col-sm-7">
              <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
            </div>
            <div class="header-right">
              <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img class="user-avatar rounded-circle" src="{{url('/public/sufee/images/admin.png')}}" alt="User Avatar">
                </a>
                <div class="user-menu dropdown-menu">
                  <a class="nav-link" href="{{ url('/admin/profile')}}"><i class="fa fa- user"></i>My Profile</a>
                  <a class="nav-link" href="{{ route('admin.logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power -off"></i>Logout</a>
                  <form id="logout-form" action="{{ route('admin.logout')}}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="content mt-3">
          @yield('content')
        </div>
      </div>
      <script src="{{url('/public/sufee/assets/js/vendor/jquery-2.1.4.min.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
      <script src="{{url('/public/sufee/assets/js/plugins.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/main.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/lib/chart-js/Chart.bundle.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/dashboard.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/widgets.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/lib/vector-map/jquery.vmap.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/lib/vector-map/jquery.vmap.min.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/lib/vector-map/country/jquery.vmap.world.js')}}"></script>
      <script src="{{url('/public/sufee/assets/js/jquery.min.js')}}"></script>
      @yield('script')
  </body>
</html>
