@extends('admin.layouts.app')
@section('css')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Motorist</strong>
          </div>
          <div class="card-body">
            <table id="motoristTable" class="table table-bordered">
              <thead class="text-primary">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Sex</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($motorists as $motorist)
                <tr>
                  <td>{{$motorist->id }}</td>
                  <td>{{$motorist->name }}</td>
                  <td>{{$motorist->email }}</td>
                  <td>{{$motorist->address }}</td>
                  <td>{{$motorist->number }}</td>
                  <td>{{$motorist->sex }}</td>
                  @if($motorist->status == 1)
                  <td>pending</td>
                  @elseif($motorist->status == 2)
                  <td>active</td>
                  @else
                  <td>inactive</td>
                  @endif
                  <td>
                    <div class="btn-group">
                      <a class="btn btn-success btn-sm btn-round" href="{{route('admin.motorist.edit',$motorist->id)}}">Edit</a>
                      <a class="btn btn-danger btn-sm btn-round">Delete</a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      $('#motoristTable').DataTable()
    })
  </script>
@stop
