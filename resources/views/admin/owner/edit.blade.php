@extends('admin.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 500px !important;
      height : 500px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="title">Owner</h4>
            <p class="category">Update Owner Info</p>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('admin.owner.update',$owners->id)}}"  enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <div class="form-group">
                <img class="avatar border-gray" id="myimage" src="{{asset('/public/'.$owners->image)}}"  />
              </div>
              <div class="form-group">
                <input name="name" type="text" class="form-control" placeholder="Full name" value="{{$owners->name}}" readonly>
              </div>
              <div class="form-group">
                <input name="email" type="email" class="form-control" placeholder="Email" value="{{$owners->email}}" readonly>
              </div>
              <div class="form-group">
                <input name="sex" type="text" class="form-control" placeholder="Sex" value="{{$owners->sex}}" readonly>
              </div>
              <div class="form-group">
                <input name="number" type="text" class="form-control" placeholder="Phone" value="{{$owners->number}}" readonly>
              </div>
              <div class="form-group">
                <input name="address" type="text" class="form-control" placeholder="Address" value="{{$owners->address}}" readonly>
              </div>
              <div class="form-group">
                <select name="status" class="form-control" id="status" class="form-control">
                  <option value="" selected="selected">-- Status --</option>
                  <option value="1">pending</option>
                  <option value="2">active</option>
                  <option value="3">inactive</option>
                </select>
              </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success btn-round">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@stop
@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#status').val('{{$owners->status}}');
    });
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
  </script>
@stop
