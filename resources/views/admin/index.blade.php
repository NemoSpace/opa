@extends('admin.layouts.app')
@section('title')
  <title>{{ config('app.name', 'OPA')}}-Dashboard</title>
@stop
@section('content-title')
  <a class="navbar-brand" href="#"> Dashboard </a>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">     
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card text-center">
           <div class="card-header card-header-warning">
            <i class="fa fa-users"></i>
            <b>Active Admin</b>
          </div>
          <div class="card-body">
            <h3 class="card-title" >{{$admins}}</h3>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card text-center">
           <div class="card-header card-header-warning">
             <i class="menu-icon fa fa-user"></i>
            <b>Active Motorist</b>
          </div>
          <div class="card-body">
            <h3 class="card-title" >{{$motorists}}</h3>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card text-center">
           <div class="card-header card-header-warning">
            <i class="menu-icon ti-user"></i>
            <b>Active Owner</b>
          </div>
          <div class="card-body">
            <h3 class="card-title" >{{$owners}}</h3>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card text-center">
           <div class="card-header card-header-warning">
            <i class="menu-icon ti-car"></i>
            <b>Vechicle Category</b>
          </div>
          <div class="card-body">
            <h3 class="card-title" >{{$vcats}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div> 
@stop