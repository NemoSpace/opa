@extends('admin.layouts.app')
@section('css')
  <style type="text/css">
    #myimage{
      width: 500px !important;
      height : 500px !important;
    }
  </style>
@stop
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Profile</h5>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('admin.profile.save')}}"  enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <input type="file" name="file" id="file" onchange="AlertFilesize(this)">
              <div class="form-group">
                @if($admin->image == 'EMPTY')
                <img class="align-self-center rounded-circle mr-3" id="myimage" src="#">
                @else
                <img class="align-self-center rounded-circle mr-3" id="myimage" src="{{asset('/public/'.$admin->image)}}">
                @endif
              </div>
              <div class="form-group">
                <input name="email" type="email" class="form-control" placeholder="email" value="{{$admin->email}}">
              </div>
              <div class="form-group">
                <input name="name" type="text" class="form-control" placeholder="Name" value="{{$admin->name}}">
              </div>
              <div class="form-group">
                <input name="address" type="text" class="form-control" placeholder="address"  value="{{$admin->address}}">
              </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success btn-round">Update Profile</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
@section('script')
  <script type="text/javascript">
    function AlertFilesize(input){
      var reader = new FileReader();
      reader.onload = function (e) {$('#myimage').attr('src', e.target.result);}
      reader.readAsDataURL(input.files[0]);
    }
  </script>
@stop
