@extends('admin.layouts.app')
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 offset-md-4 text-sm-center">
        <aside class="profile-nav alt">
          <section class="card">
            <div class="card-header user-header alt bg-dark">
              <div class="media">
                <a href=""><img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('/public/'.$admin->image)}}"></a>
                <div class="media-body">
                  <h6 class="text-light display-6">{{$admin->name}}</h6>
                  <p>{{$admin->permission}}</p>
                </div>
              </div>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><a> <i class="fa fa-envelope-o"></i> {{$admin->email}}</a></li>
              <li class="list-group-item"><a> <i class="fa fa-map-marker"></i> {{$admin->address}}</a></li>
              <li class="list-group-item">
                <a href="{{url('admin/profile/edit')}}" class="btn btn-success btn-round">Edit</a>
                <a href="{{route('admin.profile.changepass')}}" class="btn btn-info btn-round">Change Password</a>
              </li>
            </ul>
          </section>
        </aside>
      </div>
    </div>
  </div>
@endsection
