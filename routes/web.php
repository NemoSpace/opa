<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//welcome pages
Route::get('/', function () {return view('welcome');});
Route::get('/admin/login', 'RegisterController@adminLogin')->name('admin.login');
Route::get('/motorist/login', 'RegisterController@motoristLogin')->name('motorist.login');
Route::get('/owner/login', 'RegisterController@ownerLogin')->name('owner.login');
Route::post('/admin/logout', 'RegisterController@logout')->name('admin.logout');
Route::post('/motorist/logout', 'RegisterController@logout')->name('motorist.logout');
Route::post('/owner/logout', 'RegisterController@logout')->name('owner.logout');
Route::get('/admin/register','RegisterController@adminRegister')->name('admin.register');
Route::get('/motorist/register','RegisterController@motoristRegister')->name('motorist.register');
Route::get('/owner/register','RegisterController@ownerRegister')->name('owner.register');
Route::post('/login','RegisterController@login')->name('login.submit');
Route::post('/register','RegisterController@save')->name('register.submit');
Route::get('/terms', 'RegisterController@terms')->name('terms');

// Email Verification and socialite
Route::get('/social/login/facebook','SocialiteController@redirectToProvider');
Route::get('/social/login/facebook/callback', 'SocialiteController@handleProviderCallback');
Route::get('/verifyMotorist/{email}/{verifyToken}','RegisterController@sendEmailVerificationMotorist')->name('sendEmailVerificationMotorist');
Route::get('/verifyOwner/{email}/{verifyToken}','RegisterController@sendEmailVerificationOwner')->name('sendEmailVerificationOwner');


//Motorist Algorithm
Route::prefix('motorist')->group(function(){
	Route::middleware(['auth:motorists'])->group(function () {
		Route::get('/', 'DashboardController@MotoristDashboard')->name('motorist.dashboard');
		Route::get('/map', 'DashboardController@MotoristMap')->name('motorist.map');
		Route::get('/payment', 'PaymentController@motoristPayment')->name('motorist.payment.table');

		Route::get('/chart/lot', 'DashboardController@MotoristTopLot')->name('motorist.chart.lot');
		Route::get('/chart/slot', 'DashboardController@MotoristTopSlot')->name('motorist.chart.slot');
		Route::get('/chart/cancel', 'DashboardController@MotoristTopCancel')->name('motorist.chart.cancel');
		Route::get('/chart/payment', 'DashboardController@MotoristMonthlyPayment')->name('motorist.payment');

		Route::get('/profile', 'MotoristController@index')->name('motorist.profile');
		Route::get('/profile/edit', 'MotoristController@edit')->name('motorist.profile.edit');
		Route::match(['put', 'patch'],'/profile/', 'MotoristController@save')->name('motorist.profile.save');
		Route::get('/profile/changepass', 'MotoristController@changepass')->name('motorist.profile.changepass');
		Route::delete('/profile/{vehicles}','VehicleController@destroy')->name('motorist.vehicles.destroy');
		Route::match(['put', 'patch'],'/profile/changepass/{motorists}', 'MotoristController@savepass')->name('motorist.profile.savepass');

		Route::get('/vehicles','VehicleController@index')->name('motorist.vehicles');
		Route::get('/vehicles/add', 'VehicleController@create')->name('motorist.vehicles.create');
	 	Route::post('/vehicles','VehicleController@insert')->name('motorist.vehicles.insert');
	 	Route::get('/vehicles/edit/{vehicles}','VehicleController@update')->name('motorist.vehicles.update');
	 	Route::match(['put', 'patch'],'/vehicles/edit/{vehicles}','VehicleController@edit')->name('motorist.vehicles.edit');

		Route::get('/notification','NotificationController@motoristNotification')->name('motorist.notification');
		Route::post('/notification/update','NotificationController@update')->name('motorist.notification.update');

	 	Route::get('/reserve','ReserveController@index')->name('motorist.reserve');
	 	Route::get('/reserve/search','ReserveController@search')->name('motorist.reserve.search');
	 	Route::post('/reserve/cost', 'ReserveController@computeCost')->name('motorist.cost');
		Route::post('/reserve/cost/confirm', 'ReserveController@confirm')->name('motorist.reserve.confirm');
		Route::post('/reserve/cancel/{reserve}', 'ReserveController@cancel')->name('motorist.reserve.cancel');
		Route::get('/reserve/extends/{reserve}', 'ReserveController@extends')->name('motorist.reserve.extends');
		Route::get('/reserve/pay/{reserve}', 'ReserveController@pay')->name('motorist.reserve.pay');
		Route::post('/reserve/rate', 'ReserveController@rates')->name('motorist.reserve.rate');
	 	Route::post('/reserve/expire', 'ReserveController@expire')->name('motorist.reserve.expire');

	 	Route::get('/complain', 'ComplaintController@index')->name('motorist.complain');
	 	Route::post('/complain/complaint', 'ComplaintController@complaint')->name('motorist.complain.complaint');
	});
});

//Owner Algorithm
Route::prefix('owner')->group(function(){
	Route::middleware(['auth:owners'])->group(function () {
		Route::get('/', 'DashboardController@OwnerDashboard')->name('owner.dashboard');

		Route::get('/chart/complaint', 'DashboardController@OwnerMonthlyComplaint')->name('owner.chart.complaint');
		Route::get('/chart/payment', 'DashboardController@OwnerMonthlyPayment')->name('owner.chart.payment');
		Route::get('/chart/cancel', 'DashboardController@OwnerMonthlyCancel')->name('owner.chart.cancel');
		Route::get('/chart/ratings', 'DashboardController@OwnerRatings')->name('owner.chart.ratings');
		Route::get('/chart/reservation', 'DashboardController@OwnerMonthlyReservation')->name('owner.chart.reservation');
		Route::get('/chart/slot/occupied', 'DashboardController@OwnerTopSlot')->name('owner.chart.slot.occupied');
		Route::get('/chart/slot/canceled', 'DashboardController@OwnerTopSlotCancel')->name('owner.chart.slot.canceled');
		Route::get('/chart/slot/motorist', 'DashboardController@OwnerTopMotorist')->name('owner.chart.slot.motorist');



		Route::get('/chart/violation', 'DashboardController@OwnerMonthlyViolation')->name('owner.chart.violation');

		Route::get('/rfid', 'RfidController@index')->name('owner.rfid');
		Route::post('/rfid/add', 'RfidController@insert')->name('owner.rfid.add');
		Route::post('/rfid/edit', 'RfidController@update')->name('owner.rfid.edit');
		Route::post('/rfid/delete', 'RfidController@destroy')->name('owner.rfid.delete');

		Route::get('/profile', 'OwnerController@index')->name('owner.profile');
		Route::get('/profile/edit', 'OwnerController@edit')->name('owner.profile.edit');
		Route::match(['put', 'patch'],'/profile/edit', 'OwnerController@save')->name('owner.profile.save');
		Route::get('/profile/changepass', 'OwnerController@changepass')->name('owner.profile.changepass');
		Route::match(['put', 'patch'],'/profile/changepass', 'OwnerController@savepass')->name('owner.profile.savepass');

		Route::get('/lot', 'LotController@search')->name('owner.lot.search');
		Route::get('/lot/add', 'LotController@create')->name('owner.lot.add');
		Route::post('/lot/add', 'LotController@insert')->name('owner.lot.save');
		Route::get('/lot/edit/{lots}', 'LotController@edit')->name('owner.lot.edit');
		Route::match(['put', 'patch'],'/lot/edit/{lots}', 'LotController@update')->name('owner.lot.update');
		Route::delete('/lot/delete/{lots}', 'LotController@destroy')->name('owner.lot.destroy');


		Route::get('/lot/{lots}', 'SlotController@index')->name('owner.slot.view');
		Route::get('/lot/slotcategory/add/{lots}', 'SlotController@addSlotCategory')->name('owner.slotcategory.add');
		Route::post('/lot/slotcategory/save/{lots}', 'SlotController@saveSlotCategory')->name('owner.slotcategory.save');
		Route::get('/lot/slotcategory/edit/{lots}', 'SlotController@editSlotCategory')->name('owner.slotcategory.edit');
		Route::match(['put', 'patch'],'/lot/slotcategory/save/{lots}', 'SlotController@updateSlotCategory')->name('owner.slotcategory.update');
		Route::delete('/lot/{scategory}', 'SlotController@removeSlotCategory')->name('owner.scategory.destroy');

		Route::delete('/lot/{slots}','SlotController@removeParkingSlot')->name('owner.slot.destroy');
		Route::get('/lot/slot/add/{lots}', 'SlotController@addParkingSlot')->name('owner.slot.add');
		Route::post('/lot/slot/save/{lots}', 'SlotController@saveParkingSlot')->name('owner.slot.save');
		Route::get('/lot/slot/edit/{lots}', 'SlotController@editParkingSlot')->name('owner.slot.edit');
		Route::match(['put', 'patch'],'/lot/slot/update/{slots}', 'SlotController@updateParkingSlot')->name('owner.slot.update');
		Route::delete('/lot/slot/delete/{slots}','SlotController@removeParkingSlot')->name('owner.slot.delete');
		Route::get('/slot/status', 'SlotController@slotStatus')->name('owner.slot.status');

		Route::get('/notification','NotificationController@ownerNotification')->name('owner.notification');
		Route::post('/notification/update','NotificationController@update')->name('owner.notification.update');

		Route::get('/payment', 'PaymentController@index')->name('owner.payment');
		Route::get('/payment/view/{slot}', 'PaymentController@view')->name('owner.payment.view');
		Route::post('/payment/recieve', 'PaymentController@recieve')->name('owner.payment.recieve');
		Route::post('/payment/filterdate', 'PaymentController@filterToDate')->name('owner.payment.date');
		Route::post('/payment/totalpayments', 'PaymentController@totalPayments')->name('owner.payment.total');
		Route::get('/payment/pdf', 'PaymentController@convertToPDF')->name('owner.payment.pdf');
		Route::get('/payment/excel', 'PaymentController@exportFile')->name('owner.payment.excel');

		Route::post('/arrive/nonuser', 'ReserveController@arriveNonUser')->name('owner.arrive.nonuser');
		Route::post('/arrive/user', 'ReserveController@arriveUser')->name('owner.arrive.user');
		Route::get('/complaint', 'ComplaintController@search')->name('owner.complaint');
		Route::post('/complaint/update', 'ComplaintController@update')->name('owner.complaint.update');

		Route::get('/cancel', 'ReserveController@cancelTable')->name('owner.cancel');
		Route::post('/cancel/filterdate', 'ReserveController@filterToDate')->name('owner.cancel.date');
		Route::post('/cancel/totalcancel', 'ReserveController@totalCancel')->name('owner.payment.total');
		Route::get('/cancel/pdf', 'ReserveController@convertToPDF')->name('owner.cancel.pdf');
		Route::get('/cancel/excel', 'ReserveController@exportFile')->name('owner.cancel.excel');

		Route::get('/violator', 'ViolationController@index')->name('owner.violator');
		Route::post('/violator/filterdate', 'ViolationController@filterToDate')->name('owner.violator.date');
		Route::get('/violator/pdf', 'ViolationController@convertToPDF')->name('owner.violator.pdf');
		Route::get('/violator/excel', 'ViolationController@exportFile')->name('owner.violator.excel');

		Route::get('/discount','DiscountController@index')->name('owner.discount');
		Route::post('/discount/filterdate', 'DiscountController@filterToDates')->name('owner.discount.date');
		Route::get('/discount/pdf', 'DiscountController@convertPDF')->name('owner.discount.pdf');
		Route::get('/discount/excel', 'DiscountController@exportToFile')->name('owner.discount.excel');
	});
});

//Admin
Route::prefix('admin')->group(function(){
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/profile', 'AdminController@profile')->name('admin.profile');
	Route::get('/profile/edit', 'AdminController@edit')->name('admin.profile.edit');
	Route::match(['put', 'patch'],'/profile/edit', 'AdminController@save')->name('admin.profile.save');
	Route::get('/profile/changepass', 'AdminController@changePassword')->name('admin.profile.changepass');
	Route::match(['put', 'patch'],'/profile/changepass/{id}', 'AdminController@savePassword')->name('admin.profile.savepass');

	Route::get('admin/list','AdminController@viewAdmin')->name('admin.admin.list');
	Route::get('admin/list/add','AdminController@addAdmin')->name('admin.admin.add');
	Route::post('admin/list/save','AdminController@saveAdmin')->name('admin.admin.save');
	Route::get('admin/list/edit/{id}','AdminController@editAdmin')->name('admin.admin.edit');
	Route::match(['put', 'patch'],'admin/list/edit/{id}', 'AdminController@updateAdmin')->name('admin.admin.update');

	Route::get('/motorist/list', 'AdminController@viewMotorist')->name('admin.motorist.list');
	Route::get('motorist/list/edit/{id}','AdminController@editMotorist')->name('admin.motorist.edit');
	Route::match(['put', 'patch'],'motorist/list/edit/{id}', 'AdminController@updateMotorist')->name('admin.motorist.update');

	Route::get('/owner/list', 'AdminController@viewOwner')->name('admin.owner.list');
	Route::get('owner/list/edit/{id}','AdminController@editOwner')->name('admin.owner.edit');
	Route::match(['put', 'patch'],'owner/list/edit/{id}', 'AdminController@updateOwner')->name('admin.owner.update');

	Route::get('/tables', 'AdminController@tables')->name('admin.table');
	Route::get('/tables/{table}', 'AdminController@viewTable')->name('admin.table.view');
	Route::get('/tables/vcategories/add', 'AdminController@addVehicleCategory')->name('admin.table.vcategory.add');
	Route::post('/tables/vcategories/save','AdminController@saveVehicleCategory')->name('admin.table.vcategory.save');
	Route::get('/tables/vcategories/edit/{id}', 'AdminController@editVehicleCategory')->name('admin.table.vcategory.edit');
	Route::match(['put', 'patch'],'/tables/vcategories/update/{id}', 'AdminController@updateVehicleCategory')->name('admin.table.vcategory.update');

	Route::get('/vehicles','AdminController@vehicles')->name('admin.vehicles');
	Route::get('/vehicles/edit/{vehicles}', 'AdminController@vehiclesedit')->name('admin.table.vehiclesedit');
	Route::match(['put', 'patch'],'/vehicles/edit/{vehicles}','AdminController@vehiclesave')->name('admin.table.vehiclesave');

	Route::get('/slots','AdminController@slots')->name('admin.slot');
	Route::get('/slots/edit/{slots}', 'AdminController@slotedit')->name('admin.table.slotedit');
	Route::match(['put', 'patch'],'/slots/edit/{slots}','AdminController@slotsave')->name('admin.table.slotsave');

	Route::get('/lots','AdminController@lots')->name('admin.lots');
	Route::get('/lots/edit/{id}', 'AdminController@lotsedit')->name('admin.table.lotsedit');
	Route::match(['put', 'patch'],'/lots/edit/{lots}','AdminController@lotssave')->name('admin.table.lotssave');
	//Backup and Restore
	Route::get('backup', 'BackupController@index')->name('admin.backup.backups');
	Route::get('backup/create', 'BackupController@create');
	Route::get('backup/createdb', 'BackupController@createdb');
	Route::get('backup/download/{file_name}', 'BackupController@download');
	Route::get('backup/restoredb/{file_name}', 'BackupController@restoredb');
	Route::get('backup/delete/{file_name}', 'BackupController@delete');

	// Route::get('/add','AdminController@addadmin')->name('admin.add');
	Route::post('/add', 'AdminController@adminstore')->name('admin.addstore');
	Route::get('/admins','AdminController@admins')->name('admin.admins');
	Route::get('/edit/{admins}','AdminController@adminsedit')->name('admin.table.adminsedit');
	Route::match(['put', 'patch'],'/edit/{admins}','AdminController@adminssave')->name('admin.table.adminssave');
});
