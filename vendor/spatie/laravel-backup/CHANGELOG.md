# Changelog

All notable changes to `laravel-backup` are documented [here](https://docs.spatie.be/laravel-backup/v3/changelog)
