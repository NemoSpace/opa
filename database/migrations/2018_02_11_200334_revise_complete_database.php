<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReviseCompleteDatabase extends Migration
{
    public function up()
    {
        if(!(Schema::hasTable('admins'))) {
            Schema::create('admins', function (Blueprint $table) {
                $table->increments('id');
                $table->string('image');
                $table->string('name');
                $table->string('email')->unique();
                $table->string('address');
                $table->string('permission');
                $table->string('status');
                $table->string('password');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('complaints'))) {
            Schema::create('complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('complainant_id');
                $table->string('complainant_role');
                $table->integer('complainee_id');
                $table->string('complainee_role');
                $table->integer('slot_id');
                $table->string('plate_number');
                $table->text('complaint');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('discounts'))) {
            Schema::create('discounts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('reservation_id');
                $table->decimal('percent',10,2);
                $table->decimal('price',10,2);
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('lots'))) {
            Schema::create('lots', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('owner_id');
                $table->string('image');
                $table->string('map');
                $table->string('location');
                $table->string('latitude');
                $table->string('longitude');
                $table->string('address');
                $table->string('addrLot')->nullable();
                $table->string('addrStreet')->nullable();
                $table->string('addrSubd')->nullable();
                $table->string('addrBrgy')->nullable();
                $table->string('addrCity')->nullable();
                $table->string('addrProv')->nullable();
                $table->string('addrPostal')->nullable();
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('motorists'))) {
            Schema::create('motorists', function (Blueprint $table) {
                $table->increments('id');
                $table->string('image');
                $table->string('name');
                $table->string('sex');
                $table->string('address');
                $table->string('number');
                $table->string('email')->unique();
                $table->string('status');
                $table->string('password');
                $table->string('verifyToken');
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if(!(Schema::hasTable('notifications'))) {
            Schema::create('notifications', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('user_name');
                $table->string('user_type');
                $table->string('message');
                $table->string('status');
                $table->rememberToken();
                $table->timestamps();
            });
        }
        if (!(Schema::hasTable('owners'))) {
            Schema::create('owners', function (Blueprint $table) {
                $table->increments('id');
                $table->string('image');
                $table->string('name');
                $table->string('sex');
                $table->string('address');
                $table->string('number');
                $table->string('email')->unique();
                $table->string('status');
                $table->string('password');
                $table->string('verifyToken');
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if(!(Schema::hasTable('payments'))) {
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('owner_id');
                $table->integer('motorist_id');
                $table->integer('reservation_id');
                $table->decimal('price',10,2);
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('penalties'))) {
            Schema::create('penalties', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('reservation_id');
                $table->decimal('percent',10,2);
                $table->decimal('price',10,2);
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('ratings'))) {
            Schema::create('ratings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('owner_id');
                $table->decimal('rate',10,2);
                $table->dateTime('date');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('reservations'))) {
            Schema::create('reservations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('rfid_id');
                $table->integer('slot_id');
                $table->string('plate_number');
                $table->decimal('price',10,2);
                $table->dateTime('reserved');
                $table->dateTime('arrival');
                $table->dateTime('departure');
                $table->dateTime('dates');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('rfids'))) {
            Schema::create('rfids', function (Blueprint $table) {
                $table->increments('id');
                $table->string('rfid');
                $table->integer('owner_id');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('scategories'))) {
            Schema::create('scategories', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lot_id');
                $table->integer('vcategory_id');
                $table->decimal('price',10,2);
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('slots'))) {
            Schema::create('slots', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('scategory_id');
                $table->string('code');
                $table->string('image');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('users'))) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('vcategories'))) {
            Schema::create('vcategories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('vehicles'))) {
            Schema::create('vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('plate_number');
                $table->string('model');
                $table->integer('vcategory_id');
                $table->integer('motorist_id');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if(!(Schema::hasTable('violations'))) {
            Schema::create('violations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lot_id');
                $table->string('plate_number');
                $table->integer('strike');
                $table->string('status');
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }

        if(!(Schema::hasTable('rfidtables'))) {
            Schema::create('rfidtables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('rfid_id');
                $table->timestamps();
            });
        }

    }
    public function down()
    {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('complaints');
        Schema::dropIfExists('discounts');
        Schema::dropIfExists('lots');
        Schema::dropIfExists('motorists');
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('owners');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('penalties');
        Schema::dropIfExists('ratings');
        Schema::dropIfExists('reservations');
        Schema::dropIfExists('rfids');
        Schema::dropIfExists('scategories');
        Schema::dropIfExists('slots');
        Schema::dropIfExists('users');
        Schema::dropIfExists('vcategories');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('violations');
        Schema::dropIfExists('rfidtables');
    }
}
