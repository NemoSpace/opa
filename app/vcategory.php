<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class vcategory extends Model
{
	use SoftDeletes;
  protected $table = 'vcategories';
}
