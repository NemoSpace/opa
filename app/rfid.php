<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class rfid extends Model
{
	use SoftDeletes;
    protected $table = 'rfids';
}
