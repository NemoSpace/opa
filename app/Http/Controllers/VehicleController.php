<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\motorist;
use App\notification;
use App\vcategory;
use App\vehicle;

use Auth;
use DB;
use Redirect;
use Session;
use Validator;

class VehicleController extends Controller{

    public function index(){
    	$motorists = Motorist::where('ID',Auth::id())->first();
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
    	$vehicles = Vehicle::where('motorist_id',Auth::id())->whereNull('deleted_at')->get();
        return view('motorist.vehicles.index', compact('vehicles','motorists','notifications'));
    }

    public function create(){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $motorists = Motorist::where('ID',Auth::id())->first();
      $vcategories = vcategory::where('status','=',1)->get();
      return view('motorist.vehicles.add', compact('motorists','vcategories','notifications'));
    }

    public function insert(Request $request){
      $validator = Validator::make($request->all(), [
          'plate_number'    => 'required|min:6|max:7',
          'model'           => 'required',
          'type'            => 'required',
      ]);

      $vehicles = Vehicle::where('plate_number', $request->plate_number)->get()->count();
      if($vehicles > 0){
          $validator->getMessageBag()->add('plate_number', 'plate number already exist');
          return Redirect::back()->withErrors($validator);
      }

      if($validator->fails()) {
          return Redirect::back()->withErrors($validator);
      }

      $vehicles = new Vehicle;
      $vehicles->plate_number = $request->plate_number;
      $vehicles->model = $request->model;
      $vehicles->vcategory_id = $request->type;
      $vehicles->motorist_id = Auth::id();
      $vehicles->status = 1;
      $vehicles->save();
      return redirect()->route('motorist.profile');
    }

    public function update($id){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $motorists = Motorist::where('ID', Auth::id())->first();
      $vcategories = vcategory::where('status','=',1)->get();
      $vehicles = Vehicle::findOrFail($id);
      return view('motorist.vehicles.edit', compact('vehicles','motorists','vcategories','notifications'));

    }

    public function edit(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'plate_number'    => 'required|min:6|max:7',
            'model'           => 'required',
            'type'            => 'required',
        ]);

        $vehicles = Vehicle::where('id','<>',$id)->where('plate_number', $request->plate_number)->get()->count();
        if($vehicles > 0){
            $validator->getMessageBag()->add('plate_number', 'plate number already exist');
            return Redirect::back()->withErrors($validator);
        }

        if($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $vehicles = Vehicle::findOrFail($id);
        $vehicles->plate_number = $request->plate_number;
        $vehicles->model = $request->model;
        $vehicles->vcategory_id = $request->type;
        $vehicles->motorist_id = Auth::id();
        $vehicles->save();
        return redirect()->route('motorist.profile');
    }

    public function destroy($id){
        vehicle::destroy($id);
        return redirect()->route('motorist.profile');
      }
}
