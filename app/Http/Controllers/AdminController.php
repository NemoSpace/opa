<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Charts;
use Auth;
use DB;
use Redirect;
use Session;
use Validator;


use App\admin;
use App\discount;
use App\motorist;
use App\owner;
use App\lot;
use App\slot;
use App\vcategory;
use App\vehicle;
use App\notification;

class AdminController extends Controller
{
  public function __construct(){
    $this->middleware('auth:admins');
  }
  public function index(){
    $admin = Admin::where('ID',Auth::id())->first();
    $admins = Admin::where('status','=','1')->get()->count();
    $motorists = motorist::where('status','=','2')->get()->count();
    $owners = owner::where('status','=','2')->get()->count();
    $vcats = vcategory::where('status','=','1')->get()->count();
    $motorist = motorist::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
            ->get();

    $chart = Charts::database($motorist, 'bar', 'highcharts')
            ->colors(['#0000ff'])
            ->title("Monthly new Register Motorist")
            ->elementLabel("Total Motorist")
            ->dimensions(500, 500)
            ->responsive(false)
            ->groupByMonth(date('Y'), true);

    $owner = owner::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
            ->get();
    $chartowner = Charts::database($owner, 'bar', 'highcharts')
            ->title("Monthly new Register Owners")
            ->elementLabel("Total Owners")
            ->dimensions(500, 500)
            ->responsive(false)
            ->groupByMonth(date('Y'), true);

     $violation =  DB::table('violations')
                  ->select('violations.id')
                  ->count();
     $complaint = DB::table('complaints')
                  ->select('complaints.id')
                  ->count();
     $lot = DB::table('lots')
                  ->select('lots.id')
                  ->count();
     $vehicle = DB::table('vehicles')
                  ->select('vehicles.id')
                  ->count();
     $vcategories = DB::table('vcategories')
                  ->select('vcategories.id')
                  ->count();
    $scategories = DB::table('scategories')
                  ->select('scategories.id')
                  ->count();
   $reservation = DB::table('reservations')
                    ->leftJoin('slots','slots.id','=','reservations.slot_id')
                    ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                    ->leftJoin('lots','lots.id','=','scategories.lot_id')
                    ->leftJoin('owners','owners.id','=','lots.owner_id')
                    ->select('reservations.id','scategories.lot_id','lots.location')
                    ->get();
    $chartcount = Charts::create('bar', 'highcharts')
              ->title('Total Count')
              ->colors(['#C5CAE9', '#283593','#ff0000', '#00ff00', '#0000ff','#31B0D5'])
              ->elementLabel('Total')
              ->labels(['violation', 'Complaint', 'Lot','Vehicle','Vehicle Category','Slot Category'])
              ->values([$violation, $complaint,$lot,$vehicle, $vcategories,$scategories])
              ->dimensions(1000,500)
              ->responsive(true);

   $mostreservelot = DB::table('reservations')
                    ->leftJoin('slots','slots.id','=','reservations.slot_id')
                    ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                    ->leftJoin('lots','lots.id','=','scategories.lot_id')
                    ->leftJoin('owners','owners.id','=','lots.owner_id')
                    ->select(DB::raw('count(lots.id) as lote, lots.id, lots.location'))
                    ->where('reservations.status','=', 'DEPARTED')
                    ->groupBy('lots.id')
                    ->groupBy('lots.location')
                    ->orderBy('lote','desc')
                    ->limit(5)
                    ->where('lots.owner_id','=',Auth::id())
                    ->get();


      if($mostreservelot->count() < 0){
          $chartlot = Charts::database($mostreservelot,'bar','highcharts')
              ->title('Most Reserved Parking Lot')
              ->elementLabel('Total')
              ->responsive(false);
     }
     if($mostreservelot->count() == 1){
          $chartlot = Charts::create('bar', 'highcharts')
                    ->colors(['#286090'])
                    ->labels([$mostreservelot[0]->location])
                    ->title('Most Reserved Parking Lot')
                    ->values([$mostreservelot[0]->lote])
                    ->responsive(false);}
     if($mostreservelot->count() == 2){
          $chartlot = Charts::create('bar', 'highcharts')
                    ->colors(['#286090', '#31B0D5'])
                    ->labels([$mostreservelot[0]->location,$mostreservelot[1]->location])
                    ->title('Most Reserved Parking Lot')
                    ->values([$mostreservelot[0]->lote,$mostreservelot[1]->lote])
                    ->responsive(true);}
     if($mostreservelot->count() == 3){
          $chartlot = Charts::create('bar', 'highcharts')
                    ->colors(['#286090', '#31B0D5', '#449D44'])
                    ->labels([$mostreservelot[0]->location,$mostreservelot[1]->location,$mostreservelot[2]->location])
                    ->title('Most Reserved Parking Lot')
                    ->values([$mostreservelot[0]->lote,$mostreservelot[1]->lote,$mostreservelot[2]->lote])
                    ->responsive(true);}
     if($mostreservelot->count() == 4){
          $chartlot = Charts::create('bar', 'highcharts')
                    ->colors(['#286090', '#31B0D5', '#449D44','#EC971F'])
                    ->labels([$mostreservelot[0]->location,$mostreservelot[1]->location,$mostreservelot[2]->location,$mostreservelot[3]->location])
                    ->title('Most Reserved Parking Lot')
                    ->values([$mostreservelot[0]->lote,$mostreservelot[1]->lote,$mostreservelot[2]->lote,$mostreservelot[3]->lote])
                    ->responsive(true);}
     if($mostreservelot->count() == 5){
          $chartlot = Charts::create('bar', 'highcharts')
                    ->colors(['#286090', '#31B0D5', '#449D44','#EC971F','#C9302C'])
                    ->labels([$mostreservelot[0]->location,$mostreservelot[1]->location,$mostreservelot[2]->location,$mostreservelot[3]->location,$mostreservelot[4]->location])
                    ->title('Most Reserved Parking Lot')
                    ->values([$mostreservelot[0]->lote,$mostreservelot[1]->lote,$mostreservelot[2]->lote,$mostreservelot[3]->lote,$mostreservelot[4]->lote])
                    ->responsive(true);}
    return view('admin.index',compact('admin','motorists','admins','owners','vcats','chart','chartowner','chartcount','chartlot','mostreservelot'));
  }
  public function profile(){
    $admin = Admin::findOrFail(Auth::id());
    return view('admin.profile.index',compact('admin'));
  }
  public function edit(Request $request){
    $admin = Admin::findOrFail(Auth::id());
    return view('admin.profile.edit',compact('admin'));
  }
  public function save(request $request){
    $this->validate($request,[
      'email' => 'required|email',
      'name' => 'required',
      'address' => 'required',
    ]);

    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $file = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$file->getClientOriginalName();
      $file->move('public/img/admin/', $image);
      $image = 'img/admin/'.$image;
    }

    $admin = Admin::findOrFail(Auth::id());
    if ($admin->image == 'EMPTY'){
      $admin->image = $image;
    }
    if ($admin->image <> $image && $image <> 'EMPTY'){
      $admin->image = $image;
    }
    $admin->name = $request->name;
    $admin->email = $request->email;
    $admin->address = $request->address;
    $admin->save();
    return redirect()->route('admin.profile')->with('message', 'Item updated successfully.');
  }
  public function changePassword(){
    $admin = Admin::where('ID',Auth::id())->first();
    return view('admin.profile.changepass',compact('admin'));
  }
  public function savePassword(Request $request,$id){
    $validator = Validator::make($request->all(), [
      'oldpass'     => 'required',
      'password'    => 'required|min:4',
      'confirm'     => 'required|same:password'
    ]);

    $admins = Admin::findOrFail(Auth::id());
    if($request->oldpass ==$request->password){
      $validator->getMessageBag()->add('password', 'Old Password and New Password should not be the same');
      return Redirect::back()->withErrors($validator);
    }
    if (Hash::check($request->oldpass, $admins->password)) {
      if($validator->fails()) {
        return Redirect::back()->withErrors($validator);
      }
      $admins->password = $request->password;
      $admins->save();
      return redirect()->route('admin.profile');
    }else{
      $validator->getMessageBag()->add('oldpass', 'Old Password does not match');
      return Redirect::back()->withErrors($validator);
    }
  }

  //Admin List Functions
  public function viewAdmin(){
    $permission = Admin::Where('ID',Auth::id())->Where('permission','superadmin')->get()->count();
    $admins = Admin::Where('permission','!=','superadmin')->get();
    $admin = Admin::Where('id',Auth::id())->first();
    return view('admin.admin.index',compact('admin','admins','permission'));
  }
  public function addAdmin(){
    $admin = Admin::where('id',Auth::id())->first();
    return view('admin.admin.add',compact('admin'));
  }
  public function saveAdmin(Request $request){
    $validator = Validator::make($request->all(), [
      'email'       => 'required|email',
      'name'        => 'required',
      'address'     => 'required',
      'permission'  => 'required',
      'status'      => 'required',
    ]);

    $admins = Admin::where('email',$request->email)->get()->count();
    if($admins > 0){
      $validator->getMessageBag()->add('email', 'The email already in use');
      return Redirect::back()->withErrors($validator);
    }
    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $file = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$file->getClientOriginalName();
      $file->move('public/img/admin/', $image);
      $image = 'img/admin/'.$image;
    }

    $admins = new Admin;
    $admins->image = $image;
    $admins->name = $request->name;
    $admins->email = $request->email;
    $admins->address = $request->address;
    $admins->permission = $request->permission;
    $admins->status = $request->status;
    $admins->password = 'password';
    $admins->save();
    return redirect()->route('admin.admin.list')->with('message', 'Item updated successfully.');
  }
  public function editAdmin($id){
    $admin = Admin::where('ID',Auth::id())->first();
    $admins = Admin::findOrFail($id);
    return view('admin.admin.edit',compact('admin','admins'));
  }
  public function updateAdmin(Request $request, $id){
    $validator = Validator::make($request->all(), [
      'email'       => 'required|email',
      'name'        => 'required',
      'address'     => 'required',
      'permission'  => 'required',
      'status'      => 'required',
    ]);

    $admins = Admin::where('email',$request->email)->where('id','!=',$id)->get()->count();
    if($admins > 0){
      $validator->getMessageBag()->add('email', 'The email already in use');
      return Redirect::back()->withErrors($validator);

    }
    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $file = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$file->getClientOriginalName();
      $file->move('public/img/admin/', $image);
      $image = 'img/admin/'.$image;
    }

    $admins = Admin::findOrFail($id);
    if ($admins->image == 'EMPTY'){
      $admins->image = $image;
    }
    if ($admins->image <> $image && $image <> 'EMPTY'){
      $admins->image = $image;
    }

    if($admins->address != $request->address){
      $admins->address = $request->address;
    }
    $admins->name = $request->name;
    $admins->email = $request->email;
    $admins->permission = $request->permission;
    $admins->status = $request->status;
    $admins->save();
    return redirect()->route('admin.admin.list')->with('message', 'Item updated successfully.');
  }

  //Motorist List Functions
  public function viewMotorist(){
    $admin = Admin::Where('id',Auth::id())->first();
    $motorists = Motorist::Get();
    return view('admin.motorist.index',compact('admin','motorists'));
  }
  public function editMotorist($id){
    $admin = Admin::Where('id',Auth::id())->first();
    $motorists = Motorist::findOrFail($id);
    return view('admin.motorist.edit',compact('admin','motorists'));
  }
  public function updateMotorist(Request $request, $id){
    $validator = Validator::make($request->all(), [
      'status'      => 'required',
    ]);
    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $motorists = Motorist::findOrFail($id);
    $motorists->status = $request->status;
    $motorists->save();
    return redirect()->route('admin.motorist.list')->with('message', 'Item updated successfully.');
  }

  //Owner List Functions
  public function viewOwner(){
    $admin = Admin::Where('id',Auth::id())->first();
    $owners = Owner::Get();
    return view('admin.owner.index',compact('admin','owners'));
  }
  public function editOwner($id){
    $admin = Admin::Where('id',Auth::id())->first();
    $owners = Owner::findOrFail($id);
    return view('admin.owner.edit',compact('admin','owners'));
  }
  public function updateOwner(Request $request, $id){
    $validator = Validator::make($request->all(), [
      'status'      => 'required',
    ]);
    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $owners = Owner::findOrFail($id);
    $owners->status = $request->status;
    $owners->save();
    return redirect()->route('admin.owner.list')->with('message', 'Item updated successfully.');
  }

  public function tables(){
    $admin = Admin::where('id',Auth::id())->first();
    $tables = DB::select('SHOW TABLES');
    return view('admin.table.index',compact('admin','tables'));
  }
  public function viewTable($tbl){
    $admin = Admin::Where('id',Auth::id())->first();
    switch ($tbl){
      case 'complaints':
      $complaints = DB::table('complaints')
                    ->leftJoin('motorists','motorists.id','=','complaints.complainant_id')
                    ->leftJoin('slots','slots.id','=','complaints.slot_id')
                    ->select('motorists.name','slots.code','complaints.plate_number','complaints.complaint','complaints.status','complaints.updated_at')
                    ->get();
      return view('admin.table.complaints.index', compact('admin','complaints'));
        break;
      case 'discounts':
        $discounts = DB::table('discounts')
                    ->leftJoin('reservations','reservations.id','=','discounts.reservation_id')
                    ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                    ->leftJoin('motorists','motorists.id','=','vehicles.motorist_id')
                    ->select('discounts.*','motorists.name')
                    ->get();
        return view('admin.table.discounts.index', compact('admin','discounts'));
        break;
      case 'lots':
        $lots = DB::table('lots')
                    ->leftJoin('owners','owners.id','=','lots.owner_id')
                    ->select('lots.*','owners.name')
                    ->get();
        return view('admin.table.lots.index', compact('admin','lots'));
        break;
      case 'notifications':
        $notifications = Notification::get();
        return view('admin.table.notifications.index', compact('admin','notifications'));
        break;
      case 'payments':
        $payments = DB::table('payments')
                    ->leftJoin('owners','owners.id','=','payments.owner_id')
                    ->leftJoin('motorists','motorists.id','=','payments.motorist_id')
                    ->leftJoin('reservations','reservations.id','=','payments.reservation_id')
                    ->leftJoin('slots','slots.id','=','reservations.slot_id')
                    ->select(DB::raw('payments.price,payments.status,payments.updated_at,owners.name as owner,motorists.name as motorist,slots.code'))
                    ->get();
        return view('admin.table.payments.index', compact('admin','payments'));
        break;
        case 'penalties':
          $penalties = DB::table('penalties')
                      ->leftJoin('reservations','reservations.id','=','penalties.reservation_id')
                      ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                      ->leftJoin('motorists','motorists.id','=','vehicles.motorist_id')
                      ->select('penalties.*','motorists.name')
                      ->get();
          return view('admin.table.penalties.index', compact('admin','penalties'));
          break;
      case 'ratings':
        $ratings = DB::table('ratings')
                  ->leftJoin('owners','owners.id','=','ratings.owner_id')
                  ->select('owners.name','ratings.rate','ratings.date')
                  ->get();
        return view('admin.table.ratings.index', compact('admin','ratings'));
      case 'reservations':
        $reservations = DB::table('reservations')
                    ->leftJoin('slots','slots.id','=','reservations.slot_id')
                    ->select('reservations.*','slots.code')
                    ->get();
        return view('admin.table.reservations.index', compact('admin','reservations'));
        break;
      case 'rfids':
        $rfids = DB::table('rfids')
                    ->leftJoin('owners','owners.id','=','rfids.owner_id')
                    ->select('rfids.*','owners.name')
                    ->get();
        return view('admin.table.rfids.index', compact('admin','rfids'));
        break;
        case 'rfidtables':
          $rfidtables = DB::table('rfidtables')
                      ->select('rfidtables.*')
                      ->get();
          return view('admin.table.rfidtables.index', compact('admin','rfidtables'));
          break;
      case 'scategories':
        $scategories = DB::table('scategories')
                    ->leftJoin('vcategories','vcategories.id','=','scategories.vcategory_id')
                    ->leftJoin('lots','lots.id','=','scategories.lot_id')
                    ->select('scategories.*','vcategories.name','lots.location','lots.address')
                    ->get();
        return view('admin.table.scategories.index', compact('admin','scategories'));
        break;
      case 'slots':
        $slots = DB::table('slots')
                ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                ->leftJoin('vcategories','vcategories.id','=','scategories.vcategory_id')
                ->leftJoin('lots','lots.id','=','scategories.lot_id')
                ->select('slots.*','vcategories.name','scategories.price','lots.location','lots.address')
                ->get();
        return view('admin.table.slots.index', compact('admin','slots'));
        break;
      case 'vcategories':
         $vcategories = Vcategory::get();
        return view('admin.table.vcategories.index', compact('admin','vcategories'));
        break;
      case 'vehicles':
        $vehicles = DB::table('vehicles')
                    ->leftJoin('motorists', 'motorists.id','=','vehicles.motorist_id')
                    ->leftJoin('vcategories', 'vcategories.id','=','vehicles.vcategory_id')
                    ->select(DB::raw('vehicles.*, motorists.name, vcategories.name as type'))
                    ->get();
        return view('admin.table.vehicles.index', compact('admin','vehicles'));
        break;
      case 'violations':
      $violations = DB::table('violations')
                  ->leftJoin('lots', 'lots.id','=','violations.lot_id')
                  ->leftJoin('owners', 'owners.id','=','lots.owner_id')
                  ->select('violations.plate_number','violations.strike','violations.updated_at','lots.location','owners.name')
                  ->get();
      return view('admin.table.violations.index', compact('admin','violations'));
        break;
    }
  }
  public function addVehicleCategory(){
    $admin = Admin::where('id', Auth::id())->first();
    return view('admin.table.vcategories.add', compact('admin'));
  }
  public function saveVehicleCategory(Request $request){
    $this->validate($request,[
      'name' => 'required',
      'status' => 'required',
    ]);

    $vcategories = New Vcategory;
    $vcategories->name = $request->name;
    $vcategories->status = $request->status;
    $vcategories->save();
    return redirect()->route('admin.table.view','vcategories')->with('message', 'Item updated successfully.');
  }
  public function editVehicleCategory($id){
    $admin = Admin::where('id', Auth::id())->first();
    $vcategories = Vcategory::findOrFail($id);
    return view('admin.table.vcategories.edit', compact('admin','vcategories'));
  }
  public function updateVehicleCategory(Request $request, $id){
    $this->validate($request,[
      'name' => 'required',
      'status' => 'required',
    ]);

    $vcategories = Vcategory::findOrFail($id);
    $vcategories->name = $request->name;
    $vcategories->status = $request->status;
    $vcategories->save();
    return redirect()->route('admin.table.view','vcategories')->with('message', 'Item updated successfully.');
  }
}
