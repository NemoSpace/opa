<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\VerifyEmailMotorist;
use App\Mail\VerifyEmailOwner;
use App\admin;
use App\motorist;
use App\owner;

use Auth;
use Socialite;
use Session;
use Validator;
use Redirect;

class RegisterController extends Controller{
	public function __construct(){
  		$this->middleware('guest',['except'=>['logout']]);
  	}

	public function adminRegister(){
  		Session::flush();
    	Session::put('user','admin');
        return view('authentication.register');
    }

	public function motoristRegister(){
        Session::flush();
        Session::put('user','motorist');
    	return view('authentication.register');
    }

  	public function ownerRegister(){
    	Session::flush();
    	Session::put('user','owner');
  		return view('authentication.register');
  	}

  	public function adminLogin(){
        Session::flush();
        Session::put('user','admin');
    	return view('authentication.login');
    }

    public function motoristLogin(){
        Session::flush();
        Session::put('user','motorist');
    	return view('authentication.login');
    }

    public function ownerLogin(){
        Session::flush();
        Session::put('user','owner');
    	return view('authentication.login');
    }

  	public function save(Request $request){
  		if (Session('user') == 'admin'){
  			$this->validate($request,[
            	'email' 	=> 'required|email|unique:admins',
            	'name' 		=> 'required|max:120',
            	'password' 	=> 'required|min:6',
            	'confirm' 	=> 'required|min:6',
            	'address' 	=> 'required',
        	]);

        	$admin 				= new Admin;
        	$admin->image 		= "EMPTY";
        	$admin->name 		= $request->name;
        	$admin->email 		= $request->email;
        	$admin->address 	= $request->address;
        	$admin->status 		= '1';
        	$admin->permission	= 'admin';
        	$admin->password 	= $request->password;
        	$admin->save();
        	return ridirect('/');

  		}else{
  			if (Session('user') == 'motorist'){
		        $this->validate($request,[
		            'email'     => 'required|email|unique:motorists',
		            'name'      => 'required|max:120',
		            'password'  => 'required|min:6',
		            'confirm'   => 'required|same:password',
		            'address'   => 'required',
		            'sex'       => 'required',
		            'number'    => 'required|min:11|max:11',
		        ]);

	        	$motorist = Motorist::create([
		            'image'         => 'EMPTY',
		            'name'          => $request->name,
		            'email'         => $request->email,
		            'sex'           => $request->sex,
		            'address'       => $request->address,
		            'number'        => $request->number,
		            'password'      => $request->password,
		            'status'        => "1",
		            'verifyToken'   => Str::random(40)
		        ]);
		        //event(new NewwMotorist($update));
		        $users = Motorist::findOrFail($motorist->id);

	        }elseif (Session('user') == 'owner'){
	        	$this->validate($request,[
		            'email'     => 'required|email|unique:owners',
		            'name'      => 'required|max:120',
		            'password'  => 'required|min:6',
		            'confirm'   => 'required|same:password',
		            'address'   => 'required',
		            'sex'       => 'required',
		            'number'    => 'required|min:11|max:11',
		        ]);

				$owner = Owner::create([
			      'image'       => 'EMPTY',
			      'name'        => $request->name,
			      'email'       => $request->email,
			      'sex'         => $request->sex,
			      'address'     => $request->address,
			      'number'      => $request->number,
			      'password'    => $request->password,
			      'status'      => "1",
			      'verifyToken' => Str::random(40)
			    ]);
			    $users = Owner::findOrFail($owner->id);
	        }
	        $this->sendEmail($users);
       		Session::flash('message', 'Thank You, Please check your email to activate your account.');
			return view('authentication.thankyou');
       	}
    }

    public function sendEmail($user){
    	if (Session('user') == 'motorist'){
    		\Mail::to($user['email'])->send(new verifyEmailMotorist($user));
    	}else{
    		\Mail::to($user['email'])->send(new verifyEmailOwner($user));
    	}

        Session::flash('message', 'Email Sent');
        return view('authentication.thankyou');
    }

    public function sendEmailVerificationMotorist(Request $request, $email, $verifyToken){
       Motorist::where('email','=',$email)->where('verifyToken','=',$verifyToken)->update(['status'=>2]);
       Session::flash('message', 'Email Verified, You can now Login');
       return redirect()->route('motorist.login');
    }

    public function sendEmailVerificationOwner(Request $request, $email, $verifyToken){
       Owner::where('email','=',$email)->where('verifyToken','=',$verifyToken)->update(['status'=>2]);
       Session::flash('message', 'Email Verified, You can now Login');
       return redirect()->route('owner.login');
    }

    public function login(Request $request){
		$this->validate($request,[
			'email'    => 'required|email',
			'password' => 'required|min:6'
		]);
		if (Session('user') == 'admin'){
			$admin = Admin::where('email', $request->email)->where('status',2)->whereNull('deleted_at')->first();
	        if($admin){
	            Auth::guard('admins')->login($admin);
	            return redirect()->route('admin.dashboard');
	        }
    	}
        if (Session('user') == 'motorist'){
			$motorist = Motorist::where('email', $request->email)->where('status', 2)->whereNull('deleted_at')->first();
	        if($motorist){
	            if(Auth::guard('motorists')->attempt([ 'email' => $request->email, 'password' => $request->password],$request->remember)){
	        	   return redirect()->intended(route('motorist.dashboard'));
	            }
			}
		}
		if (Session('user') == 'owner'){
			$owner = Owner::where('email', $request->email)->where('status', 2)->whereNull('deleted_at')->first();
	    	if($owner){
	      		if(Auth::guard('owners')->attempt([ 'email' => $request->email, 'password' => $request->password],$request->remember)){
	        		return redirect()->intended(route('owner.dashboard'));
	      		}
			}
		}
		return redirect()->back()->withInput()->withErrors(['error'=> 'This Account does not exist or might be deactivated']);
	}

	public function logout(){
		if (Session('user') == 'admin'){
        	Auth::guard('admins')->logout();
    	}
        if (Session('user') == 'motorist'){
        	Auth::guard('motorists')->logout();
        }
		if (Session('user') == 'owner'){
			Auth::guard('owners')->logout();
		}
		return redirect('/');
	}

 	public function terms(){
       return view('terms.termsandcondition');
    }
}
