<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\redirect;
use App\complaint;
use App\discount;
use App\lot;
use App\notification;
use App\owner;
use App\payment;
use App\penalty;
use App\rating;
use App\reservation;
use App\scategory;
use App\slot;
use App\vcategory;
use App\motorist;
use App\violation;
use App\Mail\receipt;
use App\rfidtables;

use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use PDF;
use Excel;
use Session;
use Validator;
class PaymentController extends Controller{
	public function index(){
		$dateToday = Carbon::now('Asia/Manila');
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
		$owners = Owner::where('ID', Auth::id())->first();
		$payments = $this->getPayments($dateToday,$dateToday);
		return view('owner.payment.index',compact('owners','payments','notifications'));
	}
	public function motoristPayment(){
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
		$payments = DB::table('payments')
								->leftJoin('reservations','payments.reservation_id','=','reservations.id')
								->leftJoin('slots','reservations.slot_id','=','slots.id')
								->leftJoin('scategories','slots.scategory_id','=','scategories.id')
								->leftJoin('lots','scategories.lot_id','=','lots.id')
								->select('lots.location','payments.price','payments.status')
								->where('motorist_id',Auth::id())
								->get();
		return view('motorist.payment.table',compact('payments','notifications'));
	}
	public function view($id){
		$dateToday = Carbon::now('Asia/Manila');
		$reservations = DB::table('reservations')
										->leftJoin('vehicles', 'vehicles.plate_number', '=', 'reservations.plate_number')
										->leftJoin('motorists', 'motorists.id', '=', 'vehicles.motorist_id')
										->leftJoin('rfids', 'rfids.id', '=', 'reservations.rfid_id')
										->select('reservations.*','motorists.name','motorists.email','vehicles.motorist_id','rfids.rfid')
										->where('reservations.reserved','<=',$dateToday)
										->where('reservations.dates','>=',$dateToday)
										->where('reservations.slot_id',$id)
										->first();

		if($reservations->rfid_id == 0){
			$rfid = NULL;
		}else{
			$rfidtables = DB::table('rfidtables')
	 	 									->leftjoin('rfids','rfids.rfid','=','rfidtables.rfid_id')
	 										->select('rfids.rfid')
	 										->where('rfids.id','=', $reservations->rfid_id)
	 										->first();
			$rfid= $rfidtables->rfid;
		}
		$price = $reservations->price;
		$departure = Carbon::parse($reservations->departure, 'Asia/Manila')->addMinutes(5);
		if ($dateToday->gt($departure)){
			$minutes = $dateToday->diffInMinutes($departure);
			$slot = DB::table('slots')
							->leftJoin('scategories','scategories.id','=','slots.scategory_id')
							->select('scategories.price')
							->where('slots.id', $id)
							->first();
			$hours = $minutes/60;
			if($minutes > 0){
				if($hours > 1){
					$price = $price + ($slot->price * $hours);
				}else{
					$price = $price + ($slot->price * 1);
				}
			}
		}

		$penalties = Penalty::where('reservation_id',$reservations->id)->first();
		$discounts = Discount::where('reservation_id',$reservations->id)->first();

		if($penalties){$price = $price + $penalties->price;}
		if($discounts){$price = $price - $discounts->price;}

		$payments = Payment::where('motorist_id',$reservations->motorist_id)->where('status','UNPAID')->sum('price');
		$info = array(  'id' => $reservations->id,
										'rfid' => $rfid,
										'motorist_id' => $reservations->motorist_id,
										'price' => $price,
										'reserved' => $reservations->reserved,
										'status' => $reservations->status,
										'arrival' => $reservations->arrival,
										'departure' => $reservations->departure,
										'plate_number' => $reservations->plate_number,
										'balance' => $payments );
		return json_encode($info);
	}
	public function filterToDate(Request $request){
		$payments = $this->getPayments($request->start, $request->end);
		return response()->json($payments);
	}
	public function convertToPDF(Request $request){
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
		$payments = $this->getPayments($request->start, $request->end);
		view()->share('payments',$payments);
		if($request->has('download')){
			PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
			$pdf = PDF::loadView('owner.payment.pdf');
			return $pdf->download(uniqid().'_'.time(). '- payment.pdf');
		}
		return view('owner.payment.pdf',compact('payments','notifications'));
	}
	public function recieve(Request $request){
		$dateToday = Carbon::now('Asia/Manila');
		$reservations = reservation::findOrFail($request->reservation_id);
		if($reservations->status == 'RESERVED'){
			$reservations->rfid_id = $request->rfid;
			$reservations->arrival = $dateToday;
			$reservations->status = "OCCUPIED";
			$reservations->save();
			return redirect()->route('owner.slot.status');
		}
		$reservations->departure = $dateToday;
		$reservations->dates = $dateToday;
		$reservations->status = 'DEPARTED';
		$reservations->save();
		$motorist_id = 0;

		if($request->motorist_id){$motorist_id = $request->motorist_id;}

		$payments = new payment;
		$payments->motorist_id = $motorist_id;
		$payments->owner_id = Auth::id();
		$payments->reservation_id = $request->reservation_id;
		$payments->price = $request->price;
		$payments->status = "PAID";
		$payments->save();

		$payment = Payment::where('motorist_id',$request->motorist_id)->where('status','UNPAID')->update(['status' => 'PAID']);
		$payments = Payment::where('motorist_id',$request->motorist_id)->orderBy('id','desc')->first();
		if($motorist_id > 0){
			$motorist =  Motorist::findOrFail($motorist_id);
			$this->sendEmail($motorist, $payments);
		}
		return redirect()->route('owner.slot.status');
	}
	public function sendEmail($motorist, $payments){
		$dateToday = Carbon::now('Asia/Manila');
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
		$reservation = reservation::where('id',$payments->reservation_id)->first();
		\Mail::to($motorist['email'])->send(new receipt($motorist,$reservation));
		Session::flash('message', 'Email Sent');
		return redirect()->route('owner.dashboard',compact('reservation','notifications'));
	}
	public function totalPayments(Request $request){
		$payments = DB::table('payments')
								->select(DB::raw('IFNULL(SUM(payments.price),0)as total, IFNULL(payments.owner_id, 0)as id'))
								->where('payments.owner_id', Auth::id())
								->whereBetween('payments.updated_at',[$request->start, $request->end])
								->groupBy('owner_id')
								->first();
		$total = 0;
		if($payments){
			$total = $payments->total;
		}
		$info = array('total'=>$total);
		return response()->json($info);
	}
	public function getPayments($start, $end){
		if($start == $end){
			$end = str_replace('00:00:00', '23:59:59', $start);
		}
		$payments = DB::table('payments')
								->leftJoin('motorists', 'motorists.id', '=', 'payments.motorist_id')
								->leftJoin('reservations', 'reservations.id','=','payments.reservation_id')
								->select(DB::raw('payments.id, payments.price, DATE_FORMAT(payments.updated_at, \'%m/%d/%Y %h:%i:%s %p\') AS updates, payments.status, payments.reservation_id, reservations.plate_number, IFNULL(motorists.name,\'Not Registered\')as name'))
								->where('payments.owner_id', Auth::id())
								->whereBetween('payments.updated_at',[$start, $end])
								->orderBy('payments.updated_at','DESC')
								->get();
		return $payments;
	}
	public function exportFile(Request $request){
  	$payment = $this->getPayments($request->start, $request->end);
    $payment = collect($payment)->map(function($x){ return (array) $x; })->toArray();
    return \Excel::create('PaymentList', function($excel) use ($payment) {
    	$excel->sheet('sheet name', function($sheet) use ($payment){
      	$sheet->fromArray($payment);
      });
    })->download($request->type);
  }
}
