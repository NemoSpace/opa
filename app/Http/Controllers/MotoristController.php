<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\motorist;
use App\notification;
use App\reservation;
use App\vehicle;
use App\payment;

use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use Redirect;
use Session;
use Validator;
class MotoristController extends Controller
{
  public function index(){
    $motorists = motorist::findOrFail(Auth::id());
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
    $vehicles = DB::table('vehicles')
                ->leftJoin('vcategories', 'vcategories.id','=','vehicles.vcategory_id')
                ->select(DB::raw('vehicles.*, vcategories.name as type'))
                ->where('vehicles.motorist_id',Auth::id())->whereNull('vehicles.deleted_at')
                ->get();
    return view('motorist.profile.index',compact('motorists','vehicles','notifications'))->with('message', 'Slot Deleted!');
  }
  public function edit(Request $request){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
    $motorists = motorist::findOrFail(Auth::id());
    return view('motorist.profile.edit',compact('motorists','notifications'));
  }

  public function save(Request $request){
    $validator = Validator::make($request->all(), [
      'email'   => 'required|email',
      'name'    => 'required',
      'sex'     => 'required',
      'address' => 'required',
      'number'  => 'required|min:11',
    ]);

    $motorists = Motorist::where('id','<>', Auth::id())->where('email', $request->email)->get()->count();
    if($motorists > 0){
        $validator->getMessageBag()->add('email', 'email already exist');
        return Redirect::back()->withErrors($validator);
    }

    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $file = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$file->getClientOriginalName();
      $file->move('public/img/motorist/', $image);
      $image = 'img/motorist/'.$image;
    }

    $motorists = motorist::findOrFail(Auth::id());
    if ($motorists->image == 'EMPTY'){
      $motorists->image = $image;
    }
    if ($motorists->image <> $image && $image <> 'EMPTY'){
      $motorists->image = $image;
    }

    $motorists->name = $request->name;
    $motorists->email = $request->email;
    $motorists->sex = $request->sex;
    $motorists->address = $request->address;
    $motorists->number = $request->number;
    $motorists->save();
    return redirect()->route('motorist.profile');
  }

  public function changepass(){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
    $motorists = motorist::findOrFail(Auth::id());
    return view('motorist.profile.changepass',compact('motorists','notifications'));
  }
  public function savepass(Request $request){
    $validator = Validator::make($request->all(), [
      'oldpass'     => 'required',
      'password'    => 'required|min:4',
      'confirm'     => 'required|same:password'
    ]);
    $motorists = motorist::findOrFail(Auth::id());
    if($request->oldpass ==$request->password){
      $validator->getMessageBag()->add('password', 'Old Password and New Password should not be the same');
      return Redirect::back()->withErrors($validator);
    }
    if (Hash::check($request->oldpass, $motorists->password)) {
      if($validator->fails()) {
        return Redirect::back()->withErrors($validator);
      }
      $motorists->password = $request->password;
      $motorists->save();
      return redirect()->route('motorist.profile');
    }else{
      $validator->getMessageBag()->add('oldpass', 'Old Password does not match');
      return Redirect::back()->withErrors($validator);
    }
  }
}
