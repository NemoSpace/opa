<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MotoristReserve;
use App\motorist;
use App\owner;
use App\lot;
use App\notification;
use App\payment;
use App\rating;
use App\reservation;
use App\rfid;
use App\slot;
use App\vcategory;
use App\vehicle;

use Carbon\Carbon;
use Auth;
use Session;
use DB;
use PDF;
use Excel;
class ReserveController extends Controller{
    public function index(){
      $dateToday = Carbon::now('Asia/Manila');
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $motorists = Motorist::where('ID',Auth::id())->first();
      $reservations = DB::Table('reservations')
                    ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                    ->select('reservations.id')
                    ->where('vehicles.motorist_id', Auth::id())
                    ->where('reservations.reserved','<=', $dateToday)
                    ->where('reservations.departure','>=',$dateToday)
                    ->get()
                    ->count();

      $lots = $this->getLot('',$dateToday);
      $vehicles = DB::Table('vehicles')
                      ->leftJoin('motorists','motorists.id','=','vehicles.motorist_id')
                      ->leftJoin('vcategories','vcategories.id','=','vehicles.vcategory_id')
                      ->select('vehicles.*','vcategories.name')
                      ->distinct()
                      ->where('motorists.id', Auth::id())
                      ->whereNull('vehicles.deleted_at')
                      ->get();

      $reserves = DB::Table('reservations')
                    ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                    ->select('reservations.id')
                    ->where('reservations.reserved','<=', $dateToday)
                    ->where('reservations.departure','>=',$dateToday)
                    ->where('vehicles.motorist_id',Auth::id())
                    ->get()
                    ->count();
      return view('motorist.reserve.index',compact('motorists','vehicles','vcategories','reserves','reservations','lots','notifications'));
    }
    public function search(Request $request){
        $dateToday = Carbon::now('Asia/Manila');
        $lots = $this->getLot($request->search, $dateToday);
        return response()->json($lots);
    }
    public function computeCost(Request $request){
        $carbon = Carbon::now('Asia/Manila');
        $dateToday = $carbon->format('Y-m-d H:i:s');
        $vehicles = vehicle::where('id', $request->vehicle)->first();
        $slots = DB::table('slots')
                    ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                    ->leftJoin('lots','lots.id','=','scategories.lot_id')
                    ->select('slots.id','slots.code','scategories.price')
                    ->distinct()
                    ->where('lots.id', $request->id)
                    ->where('scategories.vcategory_id', $vehicles->vcategory_id)
                    ->whereNull('slots.deleted_at')
                    ->orderBy('slots.id', 'DESC')
                    ->get();
        foreach ($slots as $slot){
            $reservations = Reservation::where('slot_id', $slot->id)->where('reserved','<=', $dateToday)->where('departure','>=',$dateToday)->get();
            if($reservations->count() < 1){
                $slot_id    = $slot->id;
                $slot_code  = $slot->code;
                $slot_cost  = $slot->price * $request->hour;
            }
        }
        $info = array("id" => $slot_id , "code" =>$slot_code ,"cost" => $slot_cost);

        return json_encode($info);
    }
    public function confirm(Request $request){
        $reserve = Carbon::now('Asia/Manila');
        $arrival =  Carbon::now('Asia/Manila')->addMinutes(30);
        $departure =  Carbon::now('Asia/Manila')->addHours($request->slotHour);
        $dates = $departure->toDateString();
        $dates = $dates.' 23:59:59';
        $reservations = new Reservation;
        $reservations->rfid_id = 0;
        $reservations->plate_number = $request->slotVehicle;
        $reservations->slot_id = $request->slotID;
        $reservations->price = $request->slotPrice;
        $reservations->reserved = $reserve;
        $reservations->arrival = $arrival;
        $reservations->departure = $departure;
        $reservations->dates = $dates;
        $reservations->status = 'RESERVED';
        $reservations->save();
        $reservations = Reservation::where('plate_number',$request->slotVehicle)->orderBy('id','desc')->first();
        $motorists = Motorist::where('id',Auth::id())->first();
        $owners = $this->getOwner($request->slotID);
        $this->sendNotification($motorists->id,$motorists->name,'motorist','Your reservation was successful');
        $this->sendNotification($owners->id,$owners->name,'owner',$request->slotVehicle.' reserved a space on your parking lot.');
        return redirect()->route('motorist.dashboard');
    }
    public function cancel($id){
        $dateToday = Carbon::now('Asia/Manila');
        $reservations = Reservation::findOrFail($id);
        $slots = $reservations->slot_id;
        $plate = $reservations->plate_number;
        $reservations->departure = $dateToday;
        $reservations->dates = $dateToday;
        $reservations->status = "CANCELED";
        $reservations->save();

        $owners = $this->getOwner($slots);
        $this->sendNotification($owners->id,$owners->name,'owner',$plate.' cancel his/her reservation on your parking lot.');
        return redirect()->route('motorist.dashboard');
    }
    public function arriveUser(Request $request){
        $dateToday = Carbon::now('Asia/Manila');
        $rfidss = 0;
        if($request->rfid){
            $rfids = Rfid::where('rfid', $request->rfid)->first();
            $rfidss= $rfids->id;
        }
        $reservations = Reservation::findOrFail($request->reservation_id);
        $reservations->rfid_id = $rfidss;
        $reservations->arrival = $dateToday;
        $reservations->status = "OCCUPIED";
        $reservations->save();
        return redirect()->route('motorist.dashboard');
    }
    public function arriveNonUser(Request $request){
      $dateToday = Carbon::now('Asia/Manila');
      $departure = Carbon::now('Asia/Manila')->addHours($request->hours);
      $dates = $departure->toDateString();
      $dates = $dates.' 23:59:59';
      $rfidss = 0;
      if($request->rfid){
          $rfids = Rfid::where('rfid', $request->rfid)->first();
          $rfidss= $rfids->id;
      }
      $reservations = new Reservation;
      $reservations->rfid_id = $rfidss;
      $reservations->plate_number = $request->plate_number;
      $reservations->slot_id = $request->slot_id;
      $reservations->price = $request->price;
      $reservations->reserved = $dateToday;
      $reservations->arrival = $dateToday;
      $reservations->departure = $departure;
      $reservations->dates = $dates;
      $reservations->status = 'OCCUPIED';
      $reservations->save();
      return redirect()->route('owner.slot.status');
    }
    public function extends($id){
        $reservations = Reservation::findOrFail($id);
        $slots = DB::table('slots')
                    ->leftJoin('scategories', 'scategories.id', '=', 'slots.scategory_id')
                    ->select('scategories.price')
                    ->distinct()
                    ->where('slots.id', $reservations->slot_id)
                    ->first();
        $departure = Carbon::parse($reservations->departure)->addHour();
        $reservations->price = $reservations->price + $slots->price;
        $reservations->departure = $departure;
        $reservations->save();
        return redirect()->route('motorist.dashboard');
    }
    public function pay($id){
        $notifications = Notification::where('user_id', Auth::id())->where('user_type', 'motorist')->get()->count();
        $carbon = Carbon::now('Asia/Manila');
        $dateToday = $carbon->format('Y-m-d H:i:s');
        $reservations = DB::table('reservations')
                            ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                            ->leftJoin('motorists','motorists.id','=','vehicles.motorist_id')
                            ->select('motorists.name','reservations.*','vehicles.plate_number')
                            ->distinct()
                            ->where('reservations.id',$id)
                            ->first();
        $payments = Payment::where('motorist_id',Auth::id())->where('status','UNPAID')->sum('price');
        return view('motorist.payment.index', compact('reservations','dateToday','payments','notifications'));
    }
    public function expire(Request $request){
      $dateToday = Carbon::now('Asia/Manila');
      $reservations = Reservation::findOrFail($request->id);
      $price = $reservations->price;
      $reservations->departure = $dateToday;
      $reservations->dates = $dateToday;
      $reservations->status = "FAILED";
      $reservations->save();

      $owners = DB::table('owners')
                  ->leftJoin('lots','owners.id','=','lots.owner_id')
                  ->leftJoin('scategories','lots.id','=','scategories.lot_id')
                  ->leftJoin('slots','scategories.id','=','slots.scategory_id')
                  ->leftJoin('reservations','slots.id','=','reservations.slot_id')
                  ->select('owners.id')
                  ->where('reservations.id',$request->id)
                  ->first();

      $payments = new Payment;
      $payments->motorist_id = Auth::id();
      $payments->owner_id = $owners->id;
      $payments->reservation_id = $request->id;
      $payments->price = $price;
      $payments->status = "UNPAID";
      $payments->save();
      return redirect()->route('motorist.dashboard');
    }
    public function rates(Request $request){
        $dateToday = Carbon::now('Asia/Manila');
        $owners = DB::table('owners')
                    ->leftJoin('lots','owners.id','=','lots.owner_id')
                    ->leftJoin('scategories','lots.id','=','scategories.lot_id')
                    ->leftJoin('slots','scategories.id','=','slots.scategory_id')
                    ->select('owners.id')
                    ->where('slots.id',$request->slot)
                    ->first();

        $ratings = new Rating;
        $ratings->owner_id = $owners->id;
        $ratings->rate = $request->rate;
        $ratings->date = $dateToday;
        $ratings->status = '2';
        $ratings->save();
        return redirect()->route('motorist.dashboard');

    }
    public function getLot($search, $dateToday){
        $lots = DB::table('lots')
            ->leftJoin('scategories','lots.id','=','scategories.lot_id')
            ->leftJoin('slots','scategories.id','=','slots.scategory_id')
            ->leftJoin( DB::raw('(select reservations.slot_id from reservations where reservations.reserved <= \''.$dateToday.'\' and reservations.departure >= \''.$dateToday.'\') reservation'), function($join){
                    $join->on('reservation.slot_id','=','slots.id');
            })
            ->select(DB::raw('lots.id,lots.image,lots.location,lots.address, (count(slots.id)) as slot, (count(slots.id)-(IFNULL(count(reservation.slot_id),0))) as available'))
            ->distinct()
            ->whereNull('lots.deleted_at')
            ->where('lots.location','like','%'.$search.'%')
            ->groupBy('lots.id')
            ->groupBy('lots.image')
            ->groupBy('lots.location')
            ->groupBy('lots.address')
            ->get();
        return $lots;
    }
    public function sendNotification($id,$name,$type,$message){
      $notifications = new Notification;
      $notifications->user_id = $id;
      $notifications->user_name = $name;
      $notifications->user_type = $type;
      $notifications->message = $message;
      $notifications->status = 'unread';
      $notifications->save();
    }
    public function getOwner($slot){
      $owners = DB::table('owners')
                ->leftJoin('lots','lots.owner_id','=','owners.id')
                ->leftJoin('scategories','scategories.lot_id','=','lots.id')
                ->leftJoin('slots','slots.scategory_id','=','scategories.id')
                ->select('owners.*')
                ->where('slots.id',$slot)
                ->first();
        return $owners;
    }
    public function cancelTable(){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $reservations = DB::Table('reservations')
                        ->leftJoin('slots','slots.id','=','reservations.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->select(DB::raw('reservations.id, lots.location,slots.code,reservations.price, DATE_FORMAT(reservations.updated_at, \'%m/%d/%Y %h:%i:%s %p\') AS updates'))
                        ->where('lots.owner_id', Auth::id())
                        ->orderBy('reservations.updated_at','DESC')
                        ->get();
        return view('owner.cancel.index',compact('reservations','notifications'));
    }

    public function filterToDate(Request $request){
      $reservations = $this->getCancel($request->start, $request->end);
      return response()->json($reservations);
    }

    public function getCancel($start, $end){
      if($start == $end){
  			$end = str_replace('00:00:00', '23:59:59', $start);
  		}
      $reservations = DB::Table('reservations')
                        ->leftJoin('slots','slots.id','=','reservations.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->select(DB::raw('reservations.id, lots.location,slots.code,reservations.price, DATE_FORMAT(reservations.updated_at, \'%m/%d/%Y %h:%i:%s %p\') AS updates'))
                        ->where('lots.owner_id', Auth::id())
                        ->whereBetween('reservations.updated_at',[$start, $end])
                        ->orderBy('reservations.updated_at','DESC')
                        ->get();
  		return $reservations;
  	}

    public function totalCancel(Request $request){
      $reservations = DB::table('reservations')
                      ->leftJoin('slots','slots.id','=','reservations.slot_id')
                      ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                      ->leftJoin('lots','lots.id','=','scategories.lot_id')
  			              ->select(DB::raw('IFNULL(SUM(reservations.price),0)as total, IFNULL(lots.owner_id, 0)as id'))
                      ->where('lots.owner_id', Auth::id())
  				            ->whereBetween('reservations.updated_at',[$request->start, $request->end])
  				            ->groupBy('owner_id')
  				            ->first();
      $total = 0;
  		if($reservations){
  			$total = $reservations->total;
  		}
  		$info = array('total'=>$total);
  		return response()->json($info);
  	}

    public function convertToPDF(Request $request){
  		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
  		$reservations = $this->getCancel($request->start, $request->end);
  		view()->share('reservations',$reservations);
  		if($request->has('download')){
  			PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
  			$pdf = PDF::loadView('owner.cancel.pdf');
  			return $pdf->download(uniqid().'_'.time(). '- cancel.pdf');
  		}
  		return view('owner.cancel.pdf',compact('reservations','notifications'));
  	}

    public function exportFile(Request $request){
    	$reservations = $this->getCancel($request->start, $request->end);
      $reservations = collect($reservations)->map(function($x){ return (array) $x; })->toArray();
      return \Excel::create('Cancel Reservation List', function($excel) use ($reservations) {
      	$excel->sheet('sheet name', function($sheet) use ($reservations){
        	$sheet->fromArray($reservations);
        });
      })->download($request->type);
    }
}
