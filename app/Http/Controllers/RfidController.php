<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\notification;
use App\rfid;

use Auth;
class RfidController extends Controller
{
    public function index(){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
      $rfids = Rfid::where('owner_id',Auth::id())->whereNull('deleted_at')->get();
      $unreg = Rfid::where('owner_id', 0)->orderBy('id','DESC')->first();
      return view('owner.rfid.index',compact('rfids', 'notifications','unreg'));
    }
    public function insert(Request $request){
      $unreg = Rfid::where('rfid', $request->rfid)->orderBy('id','DESC')->first();
      $rfids = Rfid::findOrFail($unreg->id);
      $rfids->owner_id = Auth::id();
      $rfids->status = 1;
      $rfids->save();
      return redirect()->route('owner.dashboard');
    }
    public function update(Request $request){
      $rfids = Rfid::findOrFail($request->id);
      $rfids->rfid = $request->rfid;
      $rfids->save();
      return redirect()->route('owner.dashboard');
    }

    public function destroy(Request $request){
  		  rfid::destroy($request->id);
     		return redirect()->back()->with('message', 'RFID Deleted!');
  	}
}
