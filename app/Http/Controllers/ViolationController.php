<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\violation;
use App\notification;

use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use PDF;
use Session;
use Validator;

class ViolationController extends Controller
{
    public function index(){
      $dateToday = Carbon::now('Asia/Manila');
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
      $violations = $this->getViolation($dateToday,$dateToday);
      return view('owner.violation.index', compact('violations','notifications'));
    }

    public function getViolation($start, $end){
      if($start == $end){
        $end = str_replace('00:00:00', '23:59:59', $start);
      }
      $violations = DB::table('violations')
                      ->leftJoin('lots','lots.id','=','violations.lot_id')
                      ->leftJoin('vehicles','vehicles.plate_number','=','violations.plate_number')
                      ->leftJoin('complaints','violations.plate_number','=','complaints.plate_number')
                      ->select(DB::raw('violations.*,complaints.complaint,lots.location,lots.address,DATE_FORMAT(violations.updated_at, \'%m/%d/%Y %h:%i:%s %p\') AS updates'))
                      ->where('lots.owner_id', Auth::id())
                      ->whereBetween('violations.updated_at',[$start, $end])
                      ->orderBy('violations.updated_at', 'DESC')
                      ->get();
      return $violations;
    }

    public function filterToDate(Request $request){
      $violations = $this->getViolation($request->start, $request->end);
      return response()->json($violations);
    }

    public function convertToPDF(Request $request){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
  		$violations = $this->getViolation($request->start, $request->end);
  		view()->share('violations',$violations);
  		if($request->has('download')){
  			PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
  			$pdf = PDF::loadView('owner.violation.pdf');
  			return $pdf->download(uniqid().'_'.time(). '- violations.pdf');
  		}
  		return view('owner.violation.pdf',compact('violations','notifications'));
  	}

    public function exportFile(Request $request){
          $violations = $this->getViolation($request->start, $request->end);
          $violations = collect($violations)->map(function($x){ return (array) $x; })->toArray();
          return \Excel::create(uniqid().'_'.time(). '- violations', function($excel) use ($violations) {
              $excel->sheet('sheet name', function($sheet) use ($violations){
                  $sheet->fromArray($violations);
              });
          })->download($request->type);
      }
}
