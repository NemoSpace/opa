<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\notification;
use Auth;

class NotificationController extends Controller
{
    public function motoristNotification(){
        $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
        $notification = Notification::where('user_id', Auth::id())->where('user_type','motorist')->get();
        return view('motorist.notification.index', compact('notification', 'notifications'));
    }

    public function update(Request $request){
      $notifications = Notification::findOrFail($request->id);
      $type = $notifications->user_type;
      $notifications->status = $request->status;
      $notifications->save();

      if($type == 'motorist'){
          return redirect()->route('motorist.notification');
      }else{
        return redirect()->route('owner.notification');
      }
    }

    public function ownerNotification(){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $notification = Notification::where('user_id', Auth::id())->where('user_type','owner')->get();
      return view('owner.notification.index', compact('notification', 'notifications'));
    }
}
