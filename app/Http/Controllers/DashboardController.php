<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\redirect;
use App\Mail\receipt;
use App\complaint;
use App\discount;
use App\lot;
use App\motorist;
use App\notification;
use App\owner;
use App\payment;
use App\penalty;
use App\rating;
use App\reservation;
use App\scategory;
use App\slot;
use App\vcategory;
use App\vehicle;
use App\violation;
use App\rfid;
use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use Session;
use Validator;

class DashboardController extends Controller
{
    public function MotoristDashboard(){
        $dateToday = Carbon::now('Asia/Manila');
        $motorists = Motorist::where('ID',Auth::id())->first();
        $vehicle = Vehicle::where('motorist_id',Auth::id())->get()->count();
        $paid = Payment::where('motorist_id',Auth::id())->where('status','PAID')->get()->count();
        $unpaid = Payment::where('motorist_id',Auth::id())->where('status','UNPAID')->get()->count();
        $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
        $reservations = DB::table('reservations')
                    ->leftJoin('vehicles', 'reservations.plate_number','=', 'vehicles.plate_number')
                    ->leftJoin('slots', 'reservations.slot_id','=', 'slots.id')
                    ->leftJoin('scategories', 'slots.scategory_id','=', 'scategories.id')
                    ->leftJoin('lots', 'scategories.lot_id','=', 'lots.id')
                    ->select('reservations.id','reservations.arrival','reservations.departure','reservations.dates','lots.location','lots.latitude','lots.longitude','reservations.slot_id','reservations.status','slots.image')
                    ->where('vehicles.motorist_id', Auth::id())
                    ->where('reservations.reserved','<=',$dateToday)
                    ->where('reservations.dates','>=', $dateToday)
                    ->first();
        return view('motorist.index',compact('motorists','vehicle','reservations','notifications','paid','unpaid'));
    }
    public function MotoristMap(){
      $dateToday = Carbon::now('Asia/Manila');
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','motorist')->where('status','unread')->get()->count();
      $reservations = DB::table('reservations')
                  ->leftJoin('vehicles', 'reservations.plate_number','=', 'vehicles.plate_number')
                  ->leftJoin('slots', 'reservations.slot_id','=', 'slots.id')
                  ->leftJoin('scategories', 'slots.scategory_id','=', 'scategories.id')
                  ->leftJoin('lots', 'scategories.lot_id','=', 'lots.id')
                  ->select('reservations.id','reservations.arrival','reservations.departure','reservations.dates','lots.location','lots.latitude','lots.longitude','reservations.slot_id','reservations.status','slots.image')
                  ->where('vehicles.motorist_id', Auth::id())
                  ->where('reservations.reserved','<=',$dateToday)
                  ->where('reservations.dates','>=', $dateToday)
                  ->first();
      return view('motorist.map.index',compact('reservations','notifications'));
    }
    public function OwnerDashboard(){
        $dateToday = Carbon::now('Asia/Manila');
        $owners = Owner::where('ID', Auth::id())->first();
        $payments = Payment::where('owner_id',Auth::id())->get()->count();
        $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
        $rfids = Rfid::where('owner_id',Auth::id())->whereNull('deleted_at')->get()->count();
        $slots = DB::table('slots')
                    ->leftJoin('scategories','slots.scategory_id','=','scategories.id')
                    ->leftJoin('lots','lots.id','=','scategories.lot_id')
                    ->Select('slots.id')
                    ->where('lots.owner_id', Auth::id())
                    ->whereNull('slots.deleted_at')
                    ->get()
                    ->count();
        return view('owner.index',compact('owners','slots', 'notifications','payments','rfids','rfidtables'));
    }
    public function OwnerRatings(){
        $ratings = DB::table('ratings')->select(DB::raw('rate, count(rate) as rates'))->where('owner_id',Auth::id())->groupBy('rate')->get();
        return response()->json($ratings);
    }
    public function OwnerTopSlot(){
      $slots = DB::table('reservations')
                      ->leftJoin('slots','slots.id','=','reservations.slot_id')
                      ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                      ->leftJoin('lots','lots.id','=','scategories.lot_id')
                      ->leftJoin('owners','owners.id','=','lots.owner_id')
                      ->select(DB::raw('count(reservations.slot_id) as lote, slots.code, slots.id'))
                      ->where('reservations.status','=', 'DEPARTED')
                      ->whereNull('slots.deleted_at')
                      ->groupBy('slots.id')
                      ->groupBy('slots.code')
                      ->orderBy('lote','desc')
                      ->limit(5)
                      ->where('lots.owner_id','=',Auth::id())
                      ->get();
      return response()->json($slots);
    }
    public function OwnerTopSlotCancel(){
      $slots = DB::table('reservations')
                      ->leftJoin('slots','slots.id','=','reservations.slot_id')
                      ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                      ->leftJoin('lots','lots.id','=','scategories.lot_id')
                      ->leftJoin('owners','owners.id','=','lots.owner_id')
                      ->select(DB::raw('count(reservations.slot_id) as lote, slots.code, slots.id'))
                      ->where('reservations.status','=', 'CANCELED')
                      ->whereNull('slots.deleted_at')
                      ->groupBy('slots.id')
                      ->groupBy('slots.code')
                      ->orderBy('lote','desc')
                      ->limit(5)
                      ->where('lots.owner_id','=',Auth::id())
                      ->get();
      return response()->json($slots);
    }
    public function OwnerTopMotorist(){
      $slots = DB::table('reservations')
                      ->leftJoin('slots','slots.id','=','reservations.slot_id')
                      ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                      ->leftJoin('lots','lots.id','=','scategories.lot_id')
                      ->leftJoin('owners','owners.id','=','lots.owner_id')
                      ->select(DB::raw('count(reservations.plate_number) as plates, reservations.plate_number'))
                      ->where('reservations.status','=', 'DEPARTED')
                      ->groupBy('reservations.plate_number')
                      ->orderBy('plates','desc')
                      ->limit(5)
                      ->where('lots.owner_id','=',Auth::id())
                      ->get();
      return response()->json($slots);
    }
    public function OwnerMonthlyPayment(){
      $months = $this->MonthArray();
      $payments = DB::table('payments')
                  ->select(DB::raw('sum(price) as pays, DATE_FORMAT(updated_at, \'%m\') as month, DATE_FORMAT(updated_at, \'%Y\') as year'))
                  ->where('owner_id',Auth::id())
                  ->groupBy('month')
                  ->groupBy('year')
                  ->orderBy('month', 'asc')
                  ->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $payments->count()-1 ; $j++){
            if($i == $payments[$j]->month){
              $pay = $payments[$j]->pays;
              $j = $payments->count();
            }else{
              $pay = 0;
            }
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i],"pay" => $pay);
      }

      return response()->json($info);
    }
    public function OwnerMonthlyCancel(){
      $months = $this->MonthArray();
      $reservations = DB::Table('reservations')
                        ->leftJoin('slots','slots.id','=','reservations.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->select(DB::raw('sum(reservations.price) as pays, DATE_FORMAT(reservations.updated_at, \'%m\') as month, DATE_FORMAT(reservations.updated_at, \'%Y\') as year'))
                        ->where('lots.owner_id', Auth::id())
                        ->groupBy('month')
                        ->groupBy('year')
                        ->orderBy('month', 'asc')
                        ->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $reservations->count()-1 ; $j++){
            if($i == $reservations[$j]->month){
              $pay = $reservations[$j]->pays;
              $j = $reservations->count();
            }else{
              $pay = 0;
            }
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i],"pay" => $pay);
      }

      return response()->json($info);
    }
    public function OwnerMonthlyReservation(){
      $months = $this->MonthArray();
      $lots = Lot::where('owner_id', Auth::id())->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $lots->count()-1 ; $j++){
          $reservations = DB::table('reservations')
                        ->select(DB::raw('COUNT(reservations.id) reserves'))
                        ->leftJoin('slots','slots.id','=','reservations.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->where('lots.id', $lots[$j]->id)
                        ->whereMonth('reservations.departure', $i)
                        ->first();
            $location[$j] = array("id"=>$lots[$j]->id, "location"=>$lots[$j]->location,"reserves" => $reservations->reserves);
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i], "location"=>$location);
      }
      return $this->loopPerLot('location','reserves',$lots,$info);
    }
    public function OwnerMonthlyComplaint(){
      $months = $this->MonthArray();
      $lots = Lot::where('owner_id', Auth::id())->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $lots->count()-1 ; $j++){
          $complaints = DB::table('complaints')
                        ->select(DB::raw('COUNT(complaints.id) complaint'))
                        ->leftJoin('slots','slots.id','=','complaints.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->where('lots.id', $lots[$j]->id)
                        ->where('lots.owner_id',Auth::id())
                        ->whereMonth('complaints.updated_at', $i)
                        ->first();
          $complaint[$j] = array("id"=>$lots[$j]->id, "location"=>$lots[$j]->location,"complaints" => $complaints->complaint);
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i], "complaint"=>$complaint);
      }
      return $this->loopPerLot('complaint','complaints',$lots,$info);

    }
    public function OwnerMonthlyViolation(){
      $months = $this->MonthArray();
      $lots = Lot::where('owner_id', Auth::id())->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $lots->count()-1 ; $j++){
          $violations = Violation::where('lot_id',$lots[$j]->id)->whereMonth('updated_at', $i)->get()->count();
          $violation[$j] = array("id"=>$lots[$j]->id, "location"=>$lots[$j]->location,"violations" => $violations);
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i], "violation"=>$violation);
      }
      return $this->loopPerLot('violation','violations',$lots,$info);
    }
    public function loopPerLot($arrayValue,$arrayValues,$lots,$info){
      for($i = 0; $i <= $lots->count() - 1 ; $i++){
        $data = [];
        for($j = 1; $j <= 12; $j++){
           array_push($data, $info[$j][$arrayValue][$i][$arrayValues]);
        }
        $rgbx = 'rgba('.rand(0,255).', '.rand(0,255).', '.rand(0,255).', 0.9)';
        $rgbz = str_replace('0.9', '0.5', $rgbx);
        $bar[$i] = array("label"=>$lots[$i]->location, "data"=>$data, "backgroundColor" => $rgbx, "borderColor"=> $rgbz,"borderWidth" => "0" );
      }
      return response()->json($bar);
    }
    public function MonthArray(){
      $months[1] = ["name" => "January"];
      $months[2] = ["name" => "February"];
      $months[3] = ["name" => "March"];
      $months[4] = ["name" => "April"];
      $months[5] = ["name" => "May"];
      $months[6] = ["name" => "June"];
      $months[7] = ["name" => "July"];
      $months[8] = ["name" => "August"];
      $months[9] = ["name" => "September"];
      $months[10] = ["name" => "October"];
      $months[11] = ["name" => "November"];
      $months[12] = ["name" => "December"];
      return $months;
    }
    public function MotoristTopLot(){
      $lots = DB::table('reservations')
              ->select(DB::raw('count(lots.id) lote,lots.location'))
              ->leftJoin('slots','slots.id','=','reservations.slot_id')
              ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
              ->leftJoin('lots','lots.id','=','scategories.lot_id')
              ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
              ->where('reservations.status','DEPARTED')
              ->where('vehicles.motorist_id', Auth::id())
              ->groupBy('location')
              ->orderBy('lote','desc')
              ->limit(5)
              ->get();
      return response()->json($lots);
    }
    public function MotoristTopSlot(){
      $slots = DB::table('reservations')
              ->select(DB::raw('count(slots.id) slote,slots.code'))
              ->leftJoin('slots','slots.id','=','reservations.slot_id')
              ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
              ->where('reservations.status','DEPARTED')
              ->where('vehicles.motorist_id', Auth::id())
              ->groupBy('slots.code')
              ->orderBy('slote','desc')
              ->limit(5)
              ->get();
      return response()->json($slots);
    }
    public function MotoristTopCancel(){
      $cslots = DB::table('reservations')
                      ->leftJoin('slots','slots.id','=','reservations.slot_id')
                      ->leftJoin('vehicles','vehicles.plate_number','=','reservations.plate_number')
                      ->select(DB::raw('count(reservations.slot_id) as clote, slots.code, slots.id'))
                      ->where('reservations.status','=', 'CANCELED')
                      ->whereNull('slots.deleted_at')
                      ->where('vehicles.motorist_id','=',Auth::id())
                      ->groupBy('slots.id')
                      ->groupBy('slots.code')
                      ->orderBy('clote','desc')
                      ->limit(5)
                      ->get();
      return response()->json($cslots);
    }
    public function MotoristMonthlyPayment(){
      $months = $this->MonthArray();
      $payments = DB::table('payments')
                  ->select(DB::raw('sum(price) as pays, DATE_FORMAT(updated_at, \'%m\') as month, DATE_FORMAT(updated_at, \'%Y\') as year'))
                  ->where('motorist_id',Auth::id())
                  ->groupBy('month')
                  ->groupBy('year')
                  ->orderBy('month', 'asc')
                  ->get();
      for($i = 1; $i <= 12; $i++){
        for($j = 0; $j <= $payments->count()-1 ; $j++){
            if($i == $payments[$j]->month){
              $pay = $payments[$j]->pays;
              $j = $payments->count();
            }else{
              $pay = 0;
            }
        }
        $info[$i] =  array("month"=> $i, "monthName"=>$months[$i],"pay" => $pay);
      }
      return response()->json($info);
    }
}
