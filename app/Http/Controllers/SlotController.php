<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\redirect;
use Illuminate\Http\Request;

use App\lot;
use App\notification;
use App\owner;
use App\scategory;
use App\slot;
use App\vcategory;

use Carbon\Carbon;
use Auth;
use DB;
use Session;
use Validator;
class SlotController extends Controller
{

  public function index($id){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $lots = Lot::where('ID', $id)->first();
    $scategories = $this->getScategories($id);
    $slots = DB::table('slots')
                ->leftJoin('scategories','slots.scategory_id','=','scategories.id')
                ->leftJoin('vcategories','scategories.vcategory_id','=','vcategories.id')
                ->Select('slots.*','vcategories.name')
                ->where('scategories.lot_id',$id)
                ->whereNull('slots.deleted_at')
                ->get();
    return view('owner.slot.index',compact('owners','scategories','slots','lots','notifications'));
  }

  public function addSlotCategory($id){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $lots = Lot::where('ID', $id)->first();
    $vcategories = Vcategory::get();
    return view('owner.slot.category.add',compact('owners','vcategories','lots','notifications'));
  }
  public function saveSlotCategory(Request $request,$id){
    $this->validate($request,[
      'type' => 'required',
      'price' => 'required',
    ]);
    $scategories = new Scategory;
    $scategories->lot_id = $id;
    $scategories->vcategory_id = $request->type;
    $scategories->price = $request->price;
    $scategories->status = '1';
    $scategories->save();
    return redirect()->route('owner.slot.view',$id);
  }
  public function editSlotCategory($id){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $scategories = Scategory::where('ID',$id)->first();
    $vcategories = Vcategory::get();
    return view('owner.slot.category.edit',compact('owners','vcategories','scategories','notifications'));
  }
  public function updateSlotCategory(Request $request, $id){

    $this->validate($request,[
      'type' => 'required',
      'price' => 'required',
    ]);

    $scategories = Scategory::findOrFail($id);
    $lot = $scategories->lot_id;
    $scategories->vcategory_id = $request->type;
    $scategories->price = $request->price;
    $scategories->save();
    return redirect()->route('owner.slot.view',$lot);
  }
  public function removeSlotCategory($id){
   scategory::destroy($id);
    return redirect()->back()->with('message', 'Slot Category Deleted!');
  }

  public function addParkingSlot($id){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $lots = Lot::where('ID',$id)->first();
    $scategories = $this->getScategories($id);
    return view('owner.slot.add',compact('owners','lots','scategories','notifications'));
  }
  public function saveParkingSlot(Request $request, $id){
    $this->validate($request,[
      'code' => 'required',
      'type' => 'required',
    ]);
    $image = $this->slotImage();
    $slots = new Slot;
    $slots->image = $image;
    $slots->code = $request->code;
    $slots->scategory_id = $request->type;
    $slots->status = '1';
    $slots->save();
    return redirect()->route('owner.slot.view',$id);
  }

  public function editParkingSlot($id){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $slots = Slot::where('ID', $id)->first();
    $lots = $this->getScategoriesByID($slots->scategory_id);
    $scategories = $this->getScategories($lots->lot_id);
    return view('owner.slot.edit',compact('owners','slots','scategories','notifications'));
  }
  public function updateParkingSlot(Request $request, $id){
    $this->validate($request,[
      'code' => 'required',
      'type' => 'required',
    ]);

    $image = $this->slotImage();
    $slots = Slot::findOrFail($id);
    $lots = $this->getScategoriesByID($slots->scategory_id);
    if ($slots->image == 'EMPTY'){$slots->image = $image;  }
    if ($slots->image <> $image && $image <> 'EMPTY'){$slots->image = $image;  }
    $slots->code = $request->code;
    $slots->scategory_id = $request->type;
    $slots->save();
    return redirect()->route('owner.slot.view',$lots->lot_id);
  }

  public function removeParkingSlot($id){
    slot::destroy($id);
    return redirect()->back()->with('message', 'Slot Deleted!');
  }

  public function getScategories($id){
    $scategories = DB::table('scategories')
                      ->leftJoin('vcategories','scategories.vcategory_id','=','vcategories.id')
                      ->select('scategories.*','vcategories.name')
                      ->distinct()
                      ->where('scategories.lot_id',$id)
                      ->whereNull('scategories.deleted_at')
                      ->get();
    return $scategories;
  }
  public function getScategoriesByID($id){
      $scategories = DB::table('scategories')
                ->leftJoin('vcategories','scategories.vcategory_id','=','vcategories.id')
                ->select('scategories.lot_id')
                ->distinct()
                ->where('scategories.id',$id)
                ->whereNull('scategories.deleted_at')
                ->first();
      return $scategories;
  }

  public function slotImage(){
    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $fileLot = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$fileLot->getClientOriginalName();
      $fileLot->move('public/img/slot/', $image);
      $image = 'img/slot/'.$image;
    }
    return $image;
  }

  public function slotStatus(){
    $dateToday = Carbon::now('Asia/Manila');
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $slotsSlt = DB::table('slots')
                  ->leftJoin('scategories','slots.scategory_id','=','scategories.id')
                  ->leftJoin('lots','lots.id','=','scategories.lot_id')
                  ->leftJoin('vcategories','vcategories.id','=','scategories.vcategory_id')
                  ->Select(DB::raw('slots.id,lots.location,lots.address,slots.code,vcategories.name,scategories.price,slots.status,\'No Vehicle\' as plate_number'))
                  ->distinct()
                  ->whereNull('slots.deleted_at')
                  ->where('lots.owner_id', Auth::id())
                  ->whereNotIn('slots.id',function($query){
                    $dateToday = Carbon::now('Asia/Manila');
                    $query->select('slot_id')->from('reservations')->where('reserved','<=',$dateToday)->where('dates','>=',$dateToday);
                  })
                  ->get();
    $slotsRes = DB::table('slots')
                ->leftJoin('reservations','slots.id','=','reservations.slot_id')
                ->leftJoin('scategories','slots.scategory_id','=','scategories.id')
                ->leftJoin('lots','lots.id','=','scategories.lot_id')
                ->leftJoin('vcategories','vcategories.id','=','scategories.vcategory_id')
                ->Select(DB::raw('slots.id,lots.location,lots.address,slots.code,vcategories.name,scategories.price,IFNULL(reservations.status,\'VACANT\')as status,reservations.plate_number'))
                ->distinct()
                ->where('lots.owner_id', Auth::id())
                ->whereNull('slots.deleted_at')
                ->where('reservations.reserved','<=',$dateToday)
                ->where('reservations.dates','>=',$dateToday)
                ->get();

  $rfidtables = DB::table('rfidtables')
                  ->leftjoin('rfids','rfids.rfid','=','rfidtables.rfid_id')
                  ->select('rfidtables.id','rfids.rfid')
                  ->where('rfids.owner_id','=', Auth::id())
                  ->whereDate('rfidtables.created_at','=',$dateToday->toDateString())
                  ->orderBy('rfidtables.id','DESC')
                  ->first();

    if($slotsRes->count() > 0 && $slotsSlt  ->count() > 0){
      $slots = $slotsRes->merge($slotsSlt);
    }elseif($slotsRes->count() < 1 && $slotsSlt->count() > 0){
      $slots = $slotsSlt;
    }else{
      $slots = $slotsRes;
    }
    return view('owner.slot.status',compact('slots', 'notifications','rfidtables'));
  }
}
