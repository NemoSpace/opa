<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\complaint;
use App\discount;
use App\lot;
use App\motorist;
use App\notification;
use App\owner;
use App\payment;
use App\penalty;
use App\rating;
use App\reservation;
use App\scategory;
use App\slot;
use App\vcategory;
use App\vehicle;
use App\violation;

use App\Mail\receipt;
use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use PDF;
use Session;
use Validator;

class ComplaintController extends Controller
{
    public function index(){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type', 'motorist')->get()->count();
    	$complaints = Complaint::where('complainant_id',Auth::id())->get();
    	return view('motorist.complain.index', compact('complaints','notifications'));
    }

    public function complaint(Request $request){
      $this->validate($request,[
	      'plate_number' => 'required',
	      'complaint' => 'required'
	    ]);
	    $dateToday = Carbon::now('Asia/Manila');
	    $vehicles = Vehicle::where('plate_number',$request->plate_number)->first();
	    $reservations = DB::table('reservations')
	    					->leftJoin('vehicles','reservations.plate_number','=','vehicles.plate_number')
	    					->select('reservations.slot_id')
	    					->where('vehicles.motorist_id', Auth::id())
                ->where('reservations.reserved','<=',$dateToday)
                ->where('reservations.departure','>=', $dateToday)
                ->first();
      $complainee = 0;
	    if($vehicles){
        $complainee = $vehicles->motorist_id;
	    }
    	$complaints = new Complaint;
    	$complaints->complainant_id = Auth::id();
    	$complaints->complainant_role = 'motorists';
    	$complaints->complainee_id = $complainee;
    	$complaints->complainee_role = 'motorists';
    	$complaints->slot_id = $reservations->slot_id;
    	$complaints->plate_number = $request->plate_number;
    	$complaints->complaint = $request->complaint;
    	$complaints->status = 'PENDING';
    	$complaints->save();
    	return redirect()->route('motorist.complain');
    }

    public function search(Request $request){
      $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
      $search = \Request::get('search');
      $complaints = DB::table('complaints')
                        ->leftJoin('slots','slots.id','=','complaints.slot_id')
                        ->leftJoin('scategories','scategories.id','=','slots.scategory_id')
                        ->leftJoin('lots','lots.id','=','scategories.lot_id')
                        ->leftJoin('owners','owners.id','=','lots.owner_id')
                        ->select('complaints.*','slots.code','lots.id as lots','lots.location','lots.address')
                        ->where('owners.id', Auth::id())
                        ->orWhere('lots.location','like','%'.$search.'%')
                        ->orWhere('lots.address','like','%'.$search.'%')
                        ->orderBy('complaints.status','DESC')
                        ->orderBy('complaints.updated_at','DESC')
                        ->get();
      return view('owner.complaint.index', compact('complaints','notifications'));
    }


    public function update(Request $request){
      $dateToday = Carbon::now('Asia/Manila');
      $complaints = complaint::findOrFail($request->cid);
      $motorist_id = $complaints->complainant_id;

      $reservationDiscount = DB::table('vehicles')
                              ->leftJoin('reservations','reservations.plate_number','=','vehicles.plate_number')
                              ->select('reservations.*')
                              ->distinct()
                              ->where('reservations.reserved','<=', $dateToday)
                              ->where('reservations.departure','>=', $dateToday)
                              ->where('motorist_id',$motorist_id)
                              ->first();

        $reservationPenalty = Reservation::where('plate_number',$request->platenumber)
                              ->where('reserved','<=', $dateToday)
                              ->where('departure','>=', $dateToday)
                              ->first();

        $discount = ($request->percent/100) * $reservationDiscount->price;
        $discounts = new Discount;
        $discounts->percent = $request->percent;
        $discounts->price = $discount;
        $discounts->reservation_id = $reservationDiscount->id;
        $discounts->status = '1';
        $discounts->save();

        $penalties = new Penalty;
        $penalties->price = $discount;
        $penalties->reservation_id = $reservationPenalty->id;
        $penalties->status = '1';
        $penalties->save();

        $reservations = reservation::findOrFail($reservationDiscount->id);
        $reservations->slot_id = $reservationPenalty->slot_id;
        $reservations->save();

        $reservations = reservation::findOrFail($reservationPenalty->id);
        $reservations->slot_id = $reservationDiscount->slot_id;
        $reservations->save();



        $complaints->status = $request->fstatus;
        $complaints->save();

        if($request->fstatus == 'APPROVED'){
          $violations = Violation::where('plate_number',$request->platenumber)->get()->count();
          $strike = 1;
          if($violations == 1){
            $strike = 2;
          }elseif($violations == 2){
            $strike = 3;
          }
          $violations = new Violation;
          $violations->lot_id = $request->lots;
          $violations->plate_number = $request->platenumber;
          $violations->strike = $strike;
          $violations->status = '1';
          $violations->save();

      }
      return redirect()->route('owner.complaint');
    }
}
