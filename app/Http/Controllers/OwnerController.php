<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\redirect;
use App\complaint;
use App\discount;
use App\lot;
use App\notification;
use App\owner;
use App\payment;
use App\penalty;
use App\rating;
use App\reservation;
use App\scategory;
use App\slot;
use App\vcategory;
use App\motorist;
use App\violation;
use App\Mail\receipt;

use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use PDF;
use Session;
use Validator;
class OwnerController extends Controller{

  public function index(){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    $lots =  DB::table('lots')
            ->leftJoin('scategories','lots.id','=','scategories.lot_id')
            ->leftJoin('slots','scategories.id','=','slots.scategory_id')
            ->select(DB::raw('lots.id,lots.image,lots.location,lots.address,count(slots.id) as slot'))
            ->distinct()
            ->where('lots.owner_id', Auth::id())
            ->whereNull('lots.deleted_at')
            ->groupBy('lots.id')
            ->groupBy('lots.image')
            ->groupBy('lots.location')
            ->groupBy('lots.address')
            ->get();
    return view('owner.profile.index',compact('owners','lots','notifications'));
  }
  public function edit(){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::where('ID', Auth::id())->first();
    return view('owner.profile.edit',compact('owners','notifications'));
  }
  public function save(Request $request){
    $validator = Validator::make($request->all(), [
      'email'   => 'required|email',
      'name'    => 'required',
      'sex'     => 'required',
      'address' => 'required',
      'number'  => 'required|min:11',
    ]);

    $owners = Owner::where('id','<>', Auth::id())->where('email', $request->email)->get()->count();
    if($owners > 0){
        $validator->getMessageBag()->add('email', 'email already exist');
        return Redirect::back()->withErrors($validator);
    }

    if($validator->fails()) {
      return Redirect::back()->withErrors($validator);
    }

    $image = 'EMPTY';
    if(Input::hasFile('file')){
      $file = Input::file('file');
      $image = uniqid().'_'.time(). '-' .$file->getClientOriginalName();
      $file->move('public/img/owner/', $image);
      $image = 'img/owner/'.$image;
    }

    $owners = owner::findOrFail(Auth::id());
    if ($owners->image == 'EMPTY'){
      $owners->image = $image;
    }
    if ($owners->image <> $image && $image <> 'EMPTY'){
      $owners->image = $image;
    }
    $owners->name = $request->name;
    $owners->email = $request->email;
    $owners->sex = $request->sex;
    $owners->address = $request->address;
    $owners->number = $request->number;
    $owners->save();
    $owners = Owner::where('ID', Auth::id())->first();
    return redirect()->route('owner.profile');
  }

  public function changepass(){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $owners = Owner::findOrFail(Auth::id());
    return view('owner.profile.changepass',compact('owners','notifications'));
  }
  public function savepass(Request $request){
    $validator = Validator::make($request->all(), [
      'oldpass'     => 'required',
      'password'    => 'required|min:6',
      'confirm'     => 'required|same:password'
    ]);

    $owners = owner::findOrFail(Auth::id());
    if($request->oldpass ==$request->password){
      $validator->getMessageBag()->add('password', 'Old Password and New Password should not be the same');
      return Redirect::back()->withErrors($validator);
    }
    if (Hash::check($request->oldpass, $owners->password)) {
      if($validator->fails()) {
        return Redirect::back()->withErrors($validator);
      }
      $owners->password = $request->password;
      $owners->save();
      return redirect()->route('owner.profile');
    }else{
      $validator->getMessageBag()->add('oldpass', 'Old Password does not match');
      return Redirect::back()->withErrors($validator);
    }
  }
}
