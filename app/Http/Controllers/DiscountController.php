<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\discount;
use App\notification;

use Carbon\Carbon;
use Charts;
use Auth;
use DB;
use PDF;
use Session;
use Validator;

class DiscountController extends Controller
{
  public function index(){
    $dateToday = Carbon::now('Asia/Manila');
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $discounts = $this->getDiscount($dateToday,$dateToday);
    return view('owner.discount.index', compact('discounts','notifications'));
  }

  public function getDiscount($start, $end){
    if($start == $end){
      $end = str_replace('00:00:00', '23:59:59', $start);
    }
    $discounts = DB::table('discounts')
                ->leftJoin('reservations','discounts.reservation_id','=','reservations.id')
                ->leftJoin('slots','slots.id','=','reservations.slot_id')
                ->leftJoin('scategories','slots.scategory_id','=','scategories.id')
                ->leftJoin('lots','lots.id','=','scategories.lot_id')
                ->leftJoin(DB::raw('(select penalties.id,reservations.plate_number, penalties.price from penalties left join reservations on penalties.reservation_id = reservations.id ) penalty'), function($join){
                            $join->on('penalty.id','=','discounts.id');
                  })
                ->select(DB::raw('reservations.plate_number as dnumber,discounts.percent,DATE_FORMAT(discounts.updated_at, \'%m/%d/%Y %h:%i:%s %p\') AS updates,discounts.price as dprice,penalty.plate_number as pnumber,penalty.price as pprice'))
                ->distinct()
                ->where('lots.owner_id', Auth::id())
                ->get();
    return $discounts;
  }

  public function filterToDates(Request $request){
    $discounts = $this->getDiscount($request->start, $request->end);
    return response()->json($discounts);
  }

  public function convertPDF(Request $request){
    $notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
    $discounts = $this->getViolation($request->start, $request->end);
    view()->share('discounts',$discounts);
    if($request->has('download')){
      PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
      $pdf = PDF::loadView('owner.discount.pdf');
      return $pdf->download(uniqid().'_'.time(). '- discounts.pdf');
    }
    return view('owner.discount.pdf',compact('discounts','notifications'));
  }

  public function exportToFile(Request $request){
        $discounts = $this->getDiscount($request->start, $request->end);
        $discounts = collect($discounts)->map(function($x){ return (array) $x; })->toArray();
        return \Excel::create(uniqid().'_'.time(). '- DiscountList', function($excel) use ($discounts) {
            $excel->sheet('sheet name', function($sheet) use ($discounts){
                $sheet->fromArray($discounts);
            });
        })->download($request->type);
    }
}
