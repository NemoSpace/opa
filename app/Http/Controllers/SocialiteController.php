<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\motorist;
use App\owner;

use Auth;
use Session;
use Socialite;
use Validator,Redirect;

class SocialiteController extends Controller
{
    public function redirectToProvider(){

        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback(){

    	if (Session('user') == 'motorist'){

			$userSocial = Socialite::driver('facebook')->user();
        	$motorist = Motorist::where('email',$userSocial->email)->first();

        	if($motorist){

            	Auth::guard('motorists')->login($motorist);
            	return redirect()->route('motorist.dashboard');
    		}
        	else{

            	$motorist = new Motorist;
            	$motorist->image = "EMPTY";
            	$motorist->name=$userSocial->name;
            	$motorist->email=$userSocial->email;
            	$motorist->sex='male';
            	$motorist->address='Manila';
            	$motorist->number='123456890123';
            	$motorist->password = 'password';
                $motorist->rfid = "EMPTY";
            	$motorist->status = '2';
            	$motorist->save();
            	Auth::guard('motorists')->login($motorist);
            	return redirect()->route('motorist.dashboard');
            }
    	}

    	if (Session('user') == 'owner'){

		 	$userSocial = Socialite::driver('facebook')->user();
        	$owner = Owner::where('email',$userSocial ->email)->first();

        	if($owner){

            	auth::guard('owners')->login($owner);
            	return redirect()->route('owner.dashboard');
        	}
        	else{

	            $owner = new Owner;
	            $owner->image = "EMPTY";
	            $owner->name=$userSocial->name;
	            $owner->email=$userSocial->email;
	            $owner->sex='Male';
	            $owner->address='Manila';
	            $owner->number='123456890123';
	            $owner->password = 'password';
	            $owner->status = '2';
	            $owner->save();
	            Auth::guard('owners')->login($owner);
	            return redirect()->route('owner.dashboard');
        	}
    	}
    }
}
