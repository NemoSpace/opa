<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\redirect;
use Illuminate\Http\Request;

use App\lot;
use App\owner;
use App\notification;

use Carbon\Carbon;
use Auth;
use DB;
use Geocoder;
use Session;
use Validator;
class LotController extends Controller
{
	public function create(){
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
		$owners = Owner::where('ID', Auth::id())->first();
  	return view('owner.lot.add',compact('owners','notifications'));
	}

	public function insert(Request $request){
	    $validator = Validator::make($request->all(), [
				'location' 		=> 'required',
      	'addrBrgy' 		=> 'required',
      	'addrCity' 		=> 'required',
      	'addrProv' 		=> 'required',
      	'addrPostal' 	=> 'required'
	    ]);
	    $geocoder = Geocoder::getCoordinatesForAddress($request->location,'AIzaSyDT96tvvCBOBAB75nD8wYoVbUx_PkiqtLc');
	    if($geocoder['lng'] == 0 || $geocoder['lat'] == 0){
	    	$validator->getMessageBag()->add('location', 'location error');
	    	return Redirect::back()->withErrors($validator);
    	}
    	if($validator->fails()) {
      		return Redirect::back()->withErrors($validator);
    	}

    	$address = $this->LotAddress($request);
    	$image = $this->LotImage($request);
    	$map = $this->LotMap($request);


    	$lots = new lot;
			$lots->image = $image;
    	$lots->map = $map;
	    $lots->owner_id = Auth::id();
	    $lots->location = $request->location;
	    $lots->latitude = $geocoder['lat'];
	    $lots->longitude = $geocoder['lng'];
	    $lots->addrLot = $request->addrLot;
	    $lots->addrStreet = $request->addrStreet;
	    $lots->addrSubd = $request->addrSubd;
	    $lots->addrBrgy = $request->addrBrgy;
	    $lots->addrCity = $request->addrCity;
	    $lots->addrProv = $request->addrProv;
	    $lots->addrPostal = $request->addrPostal;
	    $lots->address = $address;
	    $lots->save();
	    return redirect()->route('owner.profile');
	}

	public function edit($id){
		$notifications = Notification::where('user_id', Auth::id())->where('user_type','owner')->where('status','unread')->get()->count();
		$owners = Owner::where('ID', Auth::id())->first();
  	$lots = Lot::findOrFail($id);
  	return view('owner.lot.edit',compact('owners','lots','notifications'));
	}

	public function update(Request $request, $id){
		$validator = Validator::make($request->all(), [
			'location' 		=> 'required',
    	'addrBrgy' 		=> 'required',
    	'addrCity' 		=> 'required',
    	'addrProv' 		=> 'required',
    	'addrPostal' 	=> 'required'
	    ]);
	    $geocoder = Geocoder::getCoordinatesForAddress($request->location,'AIzaSyDT96tvvCBOBAB75nD8wYoVbUx_PkiqtLc');
	    if($geocoder['lng'] == 0 || $geocoder['lat'] == 0){
	    	$validator->getMessageBag()->add('location', 'location error');
	    	return Redirect::back()->withErrors($validator);
    	}
    	if($validator->fails()) {
      		return Redirect::back()->withErrors($validator);
    	}

		$address = $this->LotAddress($request);
  	$image = $this->LotImage($request);
  	$map = $this->LotMap($request);

  	$lots = Lot::findOrFail($id);
  	if ($lots->image == 'EMPTY'){$lots->image = $image;}
  	if ($lots->image <> $image && $image <> 'EMPTY'){$lots->image = $image;}
  	if ($lots->map == 'EMPTY'){$lots->map = $map;}
  	if ($lots->map <> $map && $map <> 'EMPTY'){$lots->map = $map;}

  	$lots->location = $request->location;
  	$lots->latitude = $geocoder['lat'];
    $lots->longitude = $geocoder['lng'];
		$lots->addrLot = $request->addrLot;
  	$lots->addrStreet = $request->addrStreet;
  	$lots->addrSubd = $request->addrSubd;
  	$lots->addrBrgy = $request->addrBrgy;
  	$lots->addrCity = $request->addrCity;
  	$lots->addrProv = $request->addrProv;
  	$lots->addrPostal = $request->addrPostal;
  	$lots->address = $address;
  	$lots->save();
  	return redirect()->route('owner.profile');
	}

	public function destroy($id){
		lot::destroy($id);
   	return redirect()->back()->with('message', 'Lot Deleted!');
	}

	public function LotAddress($request){
    	$address = '';
    	if($request->addrLot){$address  = $address.$request->addrLot.', ';}
    	if($request->addrStreet){$address  = $address.$request->addrStreet.', ';}
    	if($request->addrSubd){$address = $address.$request->addrSubd.', ';}
    	if($request->addrBrgy){$address  = $address.$request->addrBrgy.', ';}
    	if($request->addrCity){$address  = $address.$request->addrCity.', '; }
    	if($request->addrProv){$address = $address.$request->addrProv.'';}
    	return $address;
	}

	public function LotImage(){
		$image = 'EMPTY';
  	if(Input::hasFile('file')){
    		$fileLot = Input::file('file');
    		$image = uniqid().'_'.time(). '-' .$fileLot->getClientOriginalName();
    		$fileLot->move('public/img/lot/', $image);
    		$image = 'img/lot/'.$image;
  	}
  	return $image;
  }

  public function LotMap(){
		$map = 'EMPTY';
		if(Input::hasFile('image')){
    		$fileMap = Input::file('image');
    		$map = uniqid().'_'.time(). '-' .$fileMap->getClientOriginalName();
    		$fileMap->move('public/img/map/', $map);
    		$map = 'img/map/'.$map;
  	}
  	return $map;
	}

	public function search(Request $request){
		$lots =  DB::table('lots')
		        ->leftJoin('scategories','lots.id','=','scategories.lot_id')
		        ->leftJoin('slots','scategories.id','=','slots.scategory_id')
		        ->select(DB::raw('lots.id,lots.image,lots.location,lots.address,count(slots.id) as slot'))
		        ->distinct()
		        ->where('lots.owner_id', Auth::id())
		        ->whereNull('lots.deleted_at')
		        ->where('lots.location','like','%'.$request->search.'%')
		        ->groupBy('lots.id')
		        ->groupBy('lots.image')
		        ->groupBy('lots.location')
		        ->groupBy('lots.address')
		        ->get();
		return response()->json($lots);
	}
}
