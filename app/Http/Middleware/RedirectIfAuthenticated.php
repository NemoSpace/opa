<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {

        switch ($guard) {
            case 'motorists':
                if(Auth::guard($guard)->check()){
                    return redirect()->route('motorist.dashboard');
                }
                break;
            case 'owners':
                if(Auth::guard($guard)->check()){
                    return redirect()->route('owner.dashboard');
                }
                break;
            case 'admins':
                if(Auth::guard($guard)->check()){
                    return redirect()->route('admin.dashboard');
                }
                break;
            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/home');
                }
                break;
        }

        return $next($request);
    }
}
