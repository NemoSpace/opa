<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    protected $except = [
        '/admin',
        '/motorist',
        '/owner',
        '/register',
        '/login',
    ];
}
