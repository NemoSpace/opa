<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class motorist extends Authenticatable{
  use Notifiable;
	protected $fillable = [
        'name','image', 'email', 'sex','address','number','password','status','verifyToken','rfid'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function vehicle(){
      return $this->hasMany('App\vehicle','motorist_id');
    }

     public function admin(){
        return $this->belongsTo('App\admin','admin_id');
    }
}
