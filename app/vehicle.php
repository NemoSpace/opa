<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class vehicle extends Model
{

	use SoftDeletes;
    protected $table = 'vehicles';
    protected $fillable = [
        'plate_number', 'model', 'type', 'motorist_id'
    ];

    public function motorist(){
    	return $this->belongsTo('App\motorist','motorist_id');
    }
}
