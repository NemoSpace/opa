<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MotoristReserve implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $reservation;

    public function __construct($reservation){
        $this->reservation = $reservation;
    }
    public function broadcastOn(){
        return new Channel('reservationTable');
    }

     public function broadcastWith(){
        return [
            'slot_id'   => $this->reservation->slot_id,
            'reserved'  => $this->reservation->reserved,
            'arrival'   => $this->reservation->arrival,
            'departure' => $this->reservation->departure,
        ];
    }


    // *
    //  * Get the channels the event should broadcast on.
    //  *
    //  * @return Channel|array
     
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }
}
