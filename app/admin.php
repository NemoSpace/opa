<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class admin extends Authenticatable
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'email', 'image', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

     public function motorist(){
        return $this->hasMany('App\motorirst','motorist_id');
    }

     public function owner(){
        return $this->hasMany('App\owner','owner_id');
    }
}
