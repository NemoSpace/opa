<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class penalty extends Model
{
    use SoftDeletes;
    protected $table = 'penalties';
}
