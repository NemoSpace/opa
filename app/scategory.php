<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class scategory extends Model
{
	use SoftDeletes;
    protected $table = 'scategories';
}
