<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class owner extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $fillable = [
        'name', 'image','email', 'sex','address','number','username','password','status','verifyToken'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value){
        $this->attributes['password'] =Hash::make($value);
    }

    public function slot(){
    	return $this->hasMany('App\slot','owner_id');
    }

     public function admin(){
        return $this->belongsTo('App\admin','admin_id');
    }
}
